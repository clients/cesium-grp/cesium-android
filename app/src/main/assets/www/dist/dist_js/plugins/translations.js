angular.module("cesium.plugins.translations", []).config(["$translateProvider", function($translateProvider) {
$translateProvider.translations("ca", {
  "NETWORK": {
    "VIEW": {
      "BTN_GRAPH": "Estadística"
    }
  },
  "GRAPH": {
    "COMMON": {
      "LINEAR_SCALE" : "Escala lineal",
      "LOGARITHMIC_SCALE" : "Escala logarítmica",
      "BTN_SHOW_STATS": "Ver estadísticas",
      "BTN_SHOW_DETAILED_STATS": "Estadísticas detalladas",
      "RANGE_DURATION_DIVIDER": "Unidad de tiempo:",
      "RANGE_DURATION": {
        "HOUR": "Agrupar por <b>horas</b>",
        "DAY": "Agrupar por <b>días</b>",
        "MONTH": "Agrupar por <b>meses</b>"
      }
    },
    "ACCOUNT": {
      "TITLE": "Estadísticas",
      "BTN_SHOW_STATS": "Ver estadísticas de la cuenta",
      "BALANCE_DIVIDER": "Balance de la cuenta",
      "BALANCE_TITLE": "Evolución de la cuenta {{pubkey|formatPubkey}}",
      "TX_RECEIVED_LABEL": "Recibido",
      "TX_SENT_LABEL": "Gastado",
      "UD_LABEL": "DU",
      "BALANCE_LABEL": "Saldo",
      "INPUT_CHART_TITLE": "Resumen de lo recibido, por emisor",
      "OUTPUT_CHART_TITLE": "Resumen de lo gastado, por destinatario"
    },
    "BLOCKCHAIN": {
      "TITLE": "Estadística",
      "BLOCKS_ISSUERS_DIVIDER": "Bloques escritos por cada miembro",
      "BLOCKS_ISSUERS_HELP": "<b>{{issuerCount|formatInteger}} miembros</b> han calculado <b>{{blockCount|formatInteger}} bloques</b>",
      "BLOCKS_ISSUERS_TITLE": "Número de bloques por miembro",
      "BLOCKS_ISSUERS_LABEL": "Número de bloques",
      "TX_DIVIDER": "Análisis de transacciones",
      "TX_AMOUNT_TITLE": "El volumen de transacciones",
      "TX_AMOUNT_PUBKEY_TITLE": "El volumen de transacciones calculado por {{issuer | formatPubkey}}",
      "TX_AMOUNT_LABEL": "Volumen intercambiado",
      "TX_COUNT_TITLE": "Número de transacciones escritas",
      "TX_COUNT_LABEL": "Número de transacciones",
      "TX_AVG_BY_BLOCK": "Promedio por bloque"
    },
    "CURRENCY": {
      "MONETARY_MASS_TITLE": "Evolución de la masa monetaria",
      "MONETARY_MASS_LABEL": "Masa monetaria",
      "MONETARY_MASS_SHARE_LABEL": "Promedio miembro",
      "UD_TITLE": "Evolución del dividendo universales",
      "MEMBERS_COUNT_TITLE": "Evolución del número de miembros",
      "MEMBERS_COUNT_LABEL": "Número de miembros"
    },
    "PEER": {
      "VIEW": {
        "BLOCK_COUNT_LABEL": "Número de bloques calculados",
        "BLOCK_COUNT": "{{count}} bloques",
        "NO_BLOCK": "Ningún bloque"
      }
    }
  }
}
);

$translateProvider.translations("de-DE", {
  "NETWORK": {
    "VIEW": {
      "BTN_GRAPH": "Statistiken"
    }
  },
  "GRAPH": {
    "COMMON": {
      "LINEAR_SCALE": "Lineare Skalierung",
      "LOGARITHMIC_SCALE": "Logarithmische Skalierung",
      "BTN_SHOW_STATS": "Statistiken ansehen",
      "BTN_SHOW_DETAILED_STATS": "Detaillierte Statistiken",
      "RANGE_DURATION_DIVIDER": "Schrittweite:",
      "RANGE_DURATION": {
        "HOUR": "Stunde",
        "DAY": "Tag",
        "MONTH": "Monat"
      },
      "MAX_AGE": {
        "DAY": "Seit 24 Stunden",
        "WEEK": "Seit einer Woche",
        "MONTH": "Seit einem Monat",
        "QUARTER": "Seit 3 Monaten",
        "SEMESTER": "Seit 6 Monaten",
        "YEAR": "Seit einem Jahr",
        "FOREVER": "Seit immer"
      }
    },
    "ACCOUNT": {
      "TITLE": "Statistiken",
      "BTN_SHOW_STATS": "Kontostatistiken anzeigen",
      "BALANCE_DIVIDER": "Kontostatus",
      "BALANCE_TITLE": "Kontoentwicklung {{pubkey|formatPubkey}}",
      "TX_RECEIVED_LABEL": "Einnahmen",
      "TX_SENT_LABEL": "Ausgaben",
      "TX_ACCUMULATION_LABEL": "Transaktionsbericht",
      "UD_LABEL": "UD",
      "UD_ACCUMULATION_LABEL": "Bilanz der DU",
      "BALANCE_LABEL": "Kontostand",
      "WOT_DIVIDER": "Netz des Vertrauens",
      "CERTIFICATION_TITLE": "Anzahl der Zertifizierungen - {{pubkey|formatPubkey}}",
      "RECEIVED_CERT_LABEL": "Insgesamt erhalten",
      "RECEIVED_CERT_DELTA_LABEL": "Schwankung der Erhaltenen",
      "GIVEN_CERT_LABEL": "Insgesamt vergeben",
      "GIVEN_CERT_DELTA_LABEL": "Schwankung der Vergebenen",
      "INPUT_CHART_TITLE": "Summe der Einnahmen, nach Emittent:",
      "OUTPUT_CHART_TITLE": "Summe der Ausgaben, nach Empfänger:"
    },
    "BLOCKCHAIN": {
      "TITLE": "Statistiken",
      "BLOCKS_ISSUERS_DIVIDER": "Blockberechnungsanalyse",
      "BLOCKS_ISSUERS_HELP": "<b>{{issuerCount|formatInteger}} Mitglieder</b> berechneten <b>{{blockCount|formatInteger}} Blöcke</b>",
      "BLOCKS_ISSUERS_TITLE": "Anzahl berechneter Blöcke nach Mitgliedern",
      "BLOCKS_ISSUERS_LABEL": "Anzahl der Blöcke",
      "TX_DIVIDER": "Transaktionsanalyse",
      "TX_AMOUNT_TITLE": "Transaktionsvolumen {{currency|abbreviate}}",
      "TX_AMOUNT_PUBKEY_TITLE": "Transaktionsvolumen berechnet von {{issuer | formatPubkey}}",
      "TX_AMOUNT_LABEL": "Gehandeltes Volumen",
      "TX_COUNT_TITLE": "Anzahl der geschriebenen Transaktionen",
      "TX_COUNT_LABEL": "Anzahl der Transaktionen",
      "TX_AVG_BY_BLOCK": "Durchschnittliche Anzahl von Transaktionen pro Block"
    },
    "CURRENCY": {
      "MONETARY_MASS_TITLE": "Entwicklung der Geldmenge",
      "MONETARY_MASS_LABEL": "Geldmenge",
      "MONETARY_MASS_SHARE_LABEL": "Durchschnitt pro Mitglied",
      "UD_TITLE": "Entwicklung der universellen Dividende",
      "MEMBERS_COUNT_TITLE": "Entwicklung der Mitgliederzahl",
      "MEMBERS_COUNT_LABEL": "Anzahl der Mitglieder",
      "MEMBERS_DELTA_TITLE": "Schwankungen in der Mitgliederzahl",
      "IS_MEMBER_DELTA_LABEL": "Validierte Mitgliedschaften",
      "WAS_MEMBER_DELTA_LABEL": "Mitgliederverluste",
      "PENDING_DELTA_LABEL": "Anträge auf Mitgliedschaft"
    },
    "PEER": {
      "VIEW": {
        "BLOCK_COUNT_LABEL": "Anzahl der berechneten Blöcke",
        "BLOCK_COUNT": "{{count}} Blöcke",
        "NO_BLOCK": "Kein Block"
      }
    },
    "DOC_STATS": {
      "TITLE": "Speicherstatistik (Cesium+)",
      "USER": {
        "TITLE": "Anzahl der mit einem Konto verknüpften Dokumente",
        "USER_PROFILE": "Benutzerprofile",
        "USER_SETTINGS": "Gespeicherte Einstellungen"
      },
      "USER_DELTA": {
        "TITLE": "Variation in der Anzahl der mit einem Konto verknüpften Dokumente",
        "USER_PROFILE": "Variation der Benutzerprofile",
        "USER_SETTINGS": "Variation der gespeicherten Einstellungen"
      },
      "MESSAGE": {
        "TITLE": "Anzahl der Dokumente im Zusammenhang mit der Kommunikation",
        "MESSAGE_INBOX": "Im Posteingang gespeicherte Nachrichten",
        "MESSAGE_OUTBOX": "Im Postausgang gespeicherte Nachrichten",
        "INVITATION_CERTIFICATION": "Einladungen zur Zertifizierung"
      },
      "SOCIAL": {
        "TITLE": "Anzahl der Seiten oder Gruppen",
        "PAGE_COMMENT": "Kommentare",
        "PAGE_RECORD": "Seiten",
        "GROUP_RECORD": "Gruppen"
      },
      "OTHER": {
        "TITLE": "Sonstige Dokumente",
        "HISTORY_DELETE": "Löschen von Dokumenten"
      }
    },
    "SYNCHRO": {
      "TITLE": "Synchronisationsstatistik",
      "COUNT": {
        "TITLE": "Anzahl Synchronisierungen",
        "INSERTS": "Einfügungen",
        "UPDATES": "Aktualisierungen",
        "DELETES": "Löschungen"
      },
      "PEER": {
        "TITLE": "Angeforderte Knoten",
        "ES_USER_API": "Knoten mit Benutzerdaten",
        "ES_SUBSCRIPTION_API": "Knoten mit Online-Diensten"
      },
      "PERFORMANCE": {
        "TITLE": "Ausführungsleistung",
        "DURATION": "Ausführungszeit (ms)"
      }
    },
    "NETWORK": {
      "TITLE": "Netzwerkstatistiken",
      "ENDPOINT_COUNT_TITLE": "Anzahl der Zugangspunkte",
      "ENDPOINT_DELTA_TITLE": "Variation in der Anzahl der Zugangspunkte"
    }
  }
}
);

$translateProvider.translations("en-GB", {
  "NETWORK": {
    "VIEW": {
      "BTN_GRAPH": "Statistics"
    }
  },
  "GRAPH": {
    "COMMON": {
      "LINEAR_SCALE" : "Linear scale",
      "LOGARITHMIC_SCALE" : "Logarithmic scale",
      "BTN_SHOW_STATS": "See statistics",
      "BTN_SHOW_DETAILED_STATS": "Detailed statistics",
      "RANGE_DURATION_DIVIDER": "Step unit:",
      "RANGE_DURATION": {
        "HOUR": "Group by <b>hour</b>",
        "DAY": "Group by <b>day</b>",
        "MONTH": "Group by <b>month</b>"
      },
      "MAX_AGE": {
        "DAY": "For 24h",
        "WEEK": "For a week",
        "MONTH": "For a month",
        "QUARTER": "For 3 months",
        "SEMESTER": "For 6 months",
        "YEAR": "For a year",
        "FOREVER": "Forever"
      }
    },
    "ACCOUNT": {
      "TITLE": "Statistics",
      "BTN_SHOW_STATS": "View account Statistics",
      "BALANCE_DIVIDER": "Account status",
      "BALANCE_TITLE": "Evolution of the account {{pubkey|formatPubkey}}",
      "TX_RECEIVED_LABEL": "Receipts",
      "TX_SENT_LABEL": "Spending",
      "UD_LABEL": "UD",
      "BALANCE_LABEL": "Balance",
      "INPUT_CHART_TITLE": "Sum of incoming flows, per transmitter:",
      "OUTPUT_CHART_TITLE": "Sum of outgoing flows, per recipient:"
    },
    "BLOCKCHAIN": {
      "TITLE": "Statistics",
      "BLOCKS_ISSUERS_DIVIDER": "Written blocks by members",
      "BLOCKS_ISSUERS_HELP": "<b>{{issuerCount|formatInteger}} members</b> calculated <b>{{blockCount|formatInteger}} blocks</b>",
      "BLOCKS_ISSUERS_TITLE": "Number of blocks calculated per member",
      "BLOCKS_ISSUERS_LABEL": "Number of blocks",
      "TX_DIVIDER": "Analysis of transactions",
      "TX_AMOUNT_TITLE": "Transaction volume",
      "TX_AMOUNT_PUBKEY_TITLE": "Volume of transactions calculated by {{issuer | formatPubkey}}",
      "TX_AMOUNT_LABEL": "Exchange volume",
      "TX_COUNT_TITLE": "Number of written transactions",
      "TX_COUNT_LABEL": "Number of transactions",
      "TX_AVG_BY_BLOCK": "Average per block"
    },
    "CURRENCY": {
      "MONETARY_MASS_TITLE": "Evolution of the monetary mass",
      "MONETARY_MASS_LABEL": "Monetary mass",
      "MONETARY_MASS_SHARE_LABEL": "Average per member",
      "UD_TITLE": "Evolution of the universal dividend",
      "MEMBERS_COUNT_TITLE": "Evolution of the number of members",
      "MEMBERS_COUNT_LABEL": "Number of members",
      "MEMBERS_DELTA_TITLE": "variation in the number of members",
      "IS_MEMBER_DELTA_LABEL": "Validated memberships",
      "WAS_MEMBER_DELTA_LABEL": "Membership losses",
      "PENDING_DELTA_LABEL": "Membership requests"
    },
    "PEER": {
      "VIEW": {
        "BLOCK_COUNT_LABEL": "Computed blocks count",
        "BLOCK_COUNT": "{{count}} blocks",
        "NO_BLOCK": "No block"
      }
    },
    "DOC_STATS": {
      "TITLE": "Data storage statistics (Cesium+)",
      "USER": {
        "TITLE": "Number of documents linked to an account",
        "USER_PROFILE": "User profiles",
        "USER_SETTINGS": "Saved settings"
      },
      "USER_DELTA": {
        "TITLE": "Delta / Number of documents linked to an account",
        "USER_PROFILE": "User profiles",
        "USER_SETTINGS": "Saved settings"
      },
      "MESSAGE": {
        "TITLE": "Number of documents related to the communication",
        "MESSAGE_INBOX": "Messages in inbox",
        "MESSAGE_OUTBOX": "Messages in outbox",
        "INVITATION_CERTIFICATION": "Invitations to certify"
      },
      "SOCIAL": {
        "TITLE": "Number of page or group",
        "PAGE_COMMENT": "Comments",
        "PAGE_RECORD": "Pages",
        "GROUP_RECORD": "Groups"
      },
      "OTHER": {
        "TITLE": "Other documents",
        "HISTORY_DELETE": "Deletion of documents"
      }
    },
    "SYNCHRO": {
      "TITLE": "Synchronization statistics",
      "COUNT": {
        "TITLE": "Synchronized volume",
        "INSERTS": "Insertions",
        "UPDATES": "Updates",
        "DELETES": "Deletions"
      },
      "PEER": {
        "TITLE": "Requested peers",
        "ES_USER_API": "Peers with with user data",
        "ES_SUBSCRIPTION_API": "Peers with subscription"
      },
      "PERFORMANCE": {
        "TITLE": "Execution performance",
        "DURATION": "Execution time (ms)"
      }
    }
  }
}
);

$translateProvider.translations("en", {
  "NETWORK": {
    "VIEW": {
      "BTN_GRAPH": "Statistics"
    }
  },
  "GRAPH": {
    "COMMON": {
      "LINEAR_SCALE" : "Linear scale",
      "LOGARITHMIC_SCALE" : "Logarithmic scale",
      "BTN_SHOW_STATS": "See statistics",
      "BTN_SHOW_DETAILED_STATS": "Detailed statistics",
      "RANGE_DURATION_DIVIDER": "Step unit:",
      "RANGE_DURATION": {
        "HOUR": "Group by <b>hour</b>",
        "DAY": "Group by <b>day</b>",
        "MONTH": "Group by <b>month</b>"
      },
      "MAX_AGE": {
        "DAY": "For 24h",
        "WEEK": "For a week",
        "MONTH": "For a month",
        "QUARTER": "For 3 months",
        "SEMESTER": "For 6 months",
        "YEAR": "For a year",
        "FOREVER": "Forever"
      }
    },
    "ACCOUNT": {
      "TITLE": "Statistics",
      "BTN_SHOW_STATS": "View account Statistics",
      "BALANCE_DIVIDER": "Account status",
      "BALANCE_TITLE": "Evolution of the account {{pubkey|formatPubkey}}",
      "TX_RECEIVED_LABEL": "Receipts",
      "TX_SENT_LABEL": "Spending",
      "UD_LABEL": "UD",
      "BALANCE_LABEL": "Balance",
      "INPUT_CHART_TITLE": "Sum of incoming flows, per transmitter:",
      "OUTPUT_CHART_TITLE": "Sum of outgoing flows, per recipient:"
    },
    "BLOCKCHAIN": {
      "TITLE": "Statistics",
      "BLOCKS_ISSUERS_DIVIDER": "Written blocks by members",
      "BLOCKS_ISSUERS_HELP": "<b>{{issuerCount|formatInteger}} members</b> calculated <b>{{blockCount|formatInteger}} blocks</b>",
      "BLOCKS_ISSUERS_TITLE": "Number of blocks calculated per member",
      "BLOCKS_ISSUERS_LABEL": "Number of blocks",
      "TX_DIVIDER": "Analysis of transactions",
      "TX_AMOUNT_TITLE": "Transaction volume",
      "TX_AMOUNT_PUBKEY_TITLE": "Volume of transactions calculated by {{issuer | formatPubkey}}",
      "TX_AMOUNT_LABEL": "Exchange volume",
      "TX_COUNT_TITLE": "Number of written transactions",
      "TX_COUNT_LABEL": "Number of transactions",
      "TX_AVG_BY_BLOCK": "Average per block"
    },
    "CURRENCY": {
      "MONETARY_MASS_TITLE": "Evolution of the monetary mass",
      "MONETARY_MASS_LABEL": "Monetary mass",
      "MONETARY_MASS_SHARE_LABEL": "Average per member",
      "UD_TITLE": "Evolution of the universal dividend",
      "MEMBERS_COUNT_TITLE": "Evolution of the number of members",
      "MEMBERS_COUNT_LABEL": "Number of members",
      "MEMBERS_DELTA_TITLE": "variation in the number of members",
      "IS_MEMBER_DELTA_LABEL": "Validated memberships",
      "WAS_MEMBER_DELTA_LABEL": "Membership losses",
      "PENDING_DELTA_LABEL": "Membership requests"
    },
    "PEER": {
      "VIEW": {
        "BLOCK_COUNT_LABEL": "Computed blocks count",
        "BLOCK_COUNT": "{{count}} blocks",
        "NO_BLOCK": "No block"
      }
    },
    "DOC_STATS": {
      "TITLE": "Data storage statistics (Cesium+)",
      "USER": {
        "TITLE": "Number of documents linked to an account",
        "USER_PROFILE": "User profiles",
        "USER_SETTINGS": "Saved settings"
      },
      "USER_DELTA": {
        "TITLE": "Delta / Number of documents linked to an account",
        "USER_PROFILE": "User profiles",
        "USER_SETTINGS": "Saved settings"
      },
      "MESSAGE": {
        "TITLE": "Number of documents related to the communication",
        "MESSAGE_INBOX": "Messages in inbox",
        "MESSAGE_OUTBOX": "Messages in outbox",
        "INVITATION_CERTIFICATION": "Invitations to certify"
      },
      "SOCIAL": {
        "TITLE": "Number of page or group",
        "PAGE_COMMENT": "Comments",
        "PAGE_RECORD": "Pages",
        "GROUP_RECORD": "Groups"
      },
      "OTHER": {
        "TITLE": "Other documents",
        "HISTORY_DELETE": "Deletion of documents"
      }
    },
    "SYNCHRO": {
      "TITLE": "Synchronization statistics",
      "COUNT": {
        "TITLE": "Synchronized volume",
        "INSERTS": "Insertions",
        "UPDATES": "Updates",
        "DELETES": "Deletions"
      },
      "PEER": {
        "TITLE": "Requested peers",
        "ES_USER_API": "Peers with with user data",
        "ES_SUBSCRIPTION_API": "Peers with subscription"
      },
      "PERFORMANCE": {
        "TITLE": "Execution performance",
        "DURATION": "Execution time (ms)"
      }
    }
  }
}
);

$translateProvider.translations("eo-EO", {
  "NETWORK": {
    "VIEW": {
      "BTN_GRAPH": "Statistikoj"
    }
  },
  "GRAPH": {
    "COMMON": {
      "LINEAR_SCALE" : "Lineara skalo",
      "LOGARITHMIC_SCALE" : "Logaritma skalo",
      "BTN_SHOW_STATS": "Vidi la statistikojn",
      "BTN_SHOW_DETAILED_STATS": "Detalaj statistikoj",
      "RANGE_DURATION_DIVIDER": "Tempo-unuo:",
      "RANGE_DURATION": {
        "HOUR": "Horo",
        "DAY": "Tago",
        "MONTH": "Monato"
      },
      "MAX_AGE": {
        "DAY": "Depost 24h",
        "WEEK": "Depost unu semajno",
        "MONTH": "Depost unu monato",
        "QUARTER": "Depost 3 monatoj",
        "SEMESTER": "Depost 6 monatoj",
        "YEAR": "Depost unu jaro",
        "FOREVER": "Depost ĉiam"
      }
    },
    "ACCOUNT": {
      "TITLE": "Statistikoj",
      "BTN_SHOW_STATS": "Vidi la statistikojn de la konto",
      "BALANCE_DIVIDER": "Stato de la konto",
      "BALANCE_TITLE": "Evoluo de la konto {{pubkey|formatPubkey}}",
      "TX_RECEIVED_LABEL": "Enspezoj",
      "TX_SENT_LABEL": "Elspezoj",
      "TX_ACCUMULATION_LABEL": "Bilanco de la spezoj",
      "UD_LABEL": "UD",
      "UD_ACCUMULATION_LABEL": "Bilanco de la UD",
      "BALANCE_LABEL": "Saldo",
      "WOT_DIVIDER": "Reto de fido",
      "CERTIFICATION_TITLE": "Nombro de atestaĵoj - {{pubkey|formatPubkey}}",
      "RECEIVED_CERT_LABEL": "Tuto pri la ricevitaj",
      "RECEIVED_CERT_DELTA_LABEL": "Vario pri la ricevitaj",
      "GIVEN_CERT_LABEL": "Tuto pri la senditaj",
      "GIVEN_CERT_DELTA_LABEL": "Vario pri la senditaj",
      "INPUT_CHART_TITLE": "Sumo de la enirantaj fluoj, por ĉiu sendinto:",
      "OUTPUT_CHART_TITLE": "Sumo de la elirantaj fluoj, por ĉiu ricevinto:"
    },
    "BLOCKCHAIN": {
      "TITLE": "Statistikoj",
      "BLOCKS_ISSUERS_DIVIDER": "Analizo de la kalkul-distribuo",
      "BLOCKS_ISSUERS_HELP": "<b>{{issuerCount|formatInteger}} membroj</b> kalkulis <b>{{blockCount|formatInteger}} blokojn</b>",
      "BLOCKS_ISSUERS_TITLE": "Nombro de blokoj kalkulitaj por membro",
      "BLOCKS_ISSUERS_LABEL": "Nombro de blokoj",
      "TX_DIVIDER": "Analizo de la spezoj",
      "TX_AMOUNT_TITLE": "Kvanto de la spezoj",
      "TX_AMOUNT_PUBKEY_TITLE": "Kvanto de spezoj kalkulitaj de {{issuer | formatPubkey}}",
      "TX_AMOUNT_LABEL": "Kvanto interŝanĝita",
      "TX_COUNT_TITLE": "Nombro de spezoj skribitaj",
      "TX_COUNT_LABEL": "Nombro de spezoj",
      "TX_AVG_BY_BLOCK": "Meza nombro de spezoj / bloko"
    },
    "CURRENCY": {
      "MONETARY_MASS_TITLE": "Evoluo de la mona maso",
      "MONETARY_MASS_LABEL": "Mona maso",
      "MONETARY_MASS_SHARE_LABEL": "Mezumo por membro",
      "UD_TITLE": "Evoluo de la universala dividendo",
      "MEMBERS_COUNT_TITLE": "Evoluo de la nombro de membroj",
      "MEMBERS_COUNT_LABEL": "Nombro de membroj",
      "MEMBERS_DELTA_TITLE": "Variado de la nombro de membroj",
      "IS_MEMBER_DELTA_LABEL": "Novaj aliĝoj",
      "WAS_MEMBER_DELTA_LABEL": "Aliĝo-perdoj",
      "PENDING_DELTA_LABEL": "Aliĝo-petoj"
    },
    "PEER": {
      "VIEW": {
        "BLOCK_COUNT_LABEL": "Nombro de blokoj kalkulitaj",
        "BLOCK_COUNT": "{{count}} blokoj",
        "NO_BLOCK": "Neniu bloko"
      }
    },
    "DOC_STATS": {
      "TITLE": "Statistikoj pri stokado",
      "USER": {
        "TITLE": "Nombro de dokumentoj ligitaj al konto",
        "USER_PROFILE": "Uzanto-profiloj",
        "USER_SETTINGS": "Parametroj konservitaj"
      },
      "USER_DELTA": {
        "TITLE": "Variado de la nombro de dokumentoj ligitaj al konto",
        "USER_PROFILE": "Uzanto-profiloj",
        "USER_SETTINGS": "Parametroj konservitaj"
      },
      "MESSAGE": {
        "TITLE": "Nombro de dokumentoj ligitaj al komunikado",
        "MESSAGE_INBOX": "Mesaĝoj en ricevujo",
        "MESSAGE_OUTBOX": "Senditaj mesaĝoj konservitaj",
        "INVITATION_CERTIFICATION": "Invitoj atestotaj"
      },
      "SOCIAL": {
        "TITLE": "Nombro de paĝoj aŭ grupoj",
        "PAGE_COMMENT": "Komentoj",
        "PAGE_RECORD": "Paĝoj",
        "GROUP_RECORD": "Grupoj"
      },
      "OTHER": {
        "TITLE": "Aliaj dokumentoj",
        "HISTORY_DELETE": "Forigoj de dokumentoj"
      }
    },
    "SYNCHRO": {
      "TITLE": "Statistikoj pri sinkronigoj",
      "COUNT": {
        "TITLE": "Kvanto sinkronigita",
        "INSERTS": "Enmetoj",
        "UPDATES": "Ĝisdatigoj",
        "DELETES": "Forigoj"
      },
      "PEER": {
        "TITLE": "Nodoj informpetitaj",
        "ES_USER_API": "Nodoj kun datenoj de uzantoj",
        "ES_SUBSCRIPTION_API": "Nodoj kun retaj servoj"
      },
      "PERFORMANCE": {
        "TITLE": "Efikecoj pri efektiviĝo",
        "DURATION": "Tempo por efektiviĝo (ms)"
      }
    },
    "NETWORK": {
      "TITLE": "Statistikoj pri la reto",
      "ENDPOINT_COUNT_TITLE": "Nombro de alir-punktoj",
      "ENDPOINT_DELTA_TITLE": "Variado de la nombro de alir-punktoj"
    }
  }
}
);

$translateProvider.translations("es-ES", {
  "NETWORK": {
    "VIEW": {
      "BTN_GRAPH": "Estadística"
    }
  },
  "GRAPH": {
    "COMMON": {
      "LINEAR_SCALE" : "Escala lineal",
      "LOGARITHMIC_SCALE" : "Escala logarítmica",
      "BTN_SHOW_STATS": "Ver estadísticas",
      "BTN_SHOW_DETAILED_STATS": "Estadísticas detalladas",
      "RANGE_DURATION_DIVIDER": "Unidad de tiempo:",
      "RANGE_DURATION": {
        "HOUR": "Agrupar por <b>horas</b>",
        "DAY": "Agrupar por <b>días</b>",
        "MONTH": "Agrupar por <b>meses</b>"
      }
    },
    "ACCOUNT": {
      "TITLE": "Estadísticas",
      "BTN_SHOW_STATS": "Ver estadísticas de la cuenta",
      "BALANCE_DIVIDER": "Balance de la cuenta",
      "BALANCE_TITLE": "Evolución de la cuenta {{pubkey|formatPubkey}}",
      "TX_RECEIVED_LABEL": "Recibido",
      "TX_SENT_LABEL": "Gastado",
      "UD_LABEL": "DU",
      "BALANCE_LABEL": "Saldo",
      "INPUT_CHART_TITLE": "Resumen de lo recibido, por emisor",
      "OUTPUT_CHART_TITLE": "Resumen de lo gastado, por destinatario"
    },
    "BLOCKCHAIN": {
      "TITLE": "Estadística",
      "BLOCKS_ISSUERS_DIVIDER": "Bloques escritos por cada miembro",
      "BLOCKS_ISSUERS_HELP": "<b>{{issuerCount|formatInteger}} miembros</b> han calculado <b>{{blockCount|formatInteger}} bloques</b>",
      "BLOCKS_ISSUERS_TITLE": "Número de bloques por miembro",
      "BLOCKS_ISSUERS_LABEL": "Número de bloques",
      "TX_DIVIDER": "Análisis de transacciones",
      "TX_AMOUNT_TITLE": "El volumen de transacciones",
      "TX_AMOUNT_PUBKEY_TITLE": "El volumen de transacciones calculado por {{issuer | formatPubkey}}",
      "TX_AMOUNT_LABEL": "Volumen intercambiado",
      "TX_COUNT_TITLE": "Número de transacciones escritas",
      "TX_COUNT_LABEL": "Número de transacciones",
      "TX_AVG_BY_BLOCK": "Promedio por bloque"
    },
    "CURRENCY": {
      "MONETARY_MASS_TITLE": "Evolución de la masa monetaria",
      "MONETARY_MASS_LABEL": "Masa monetaria",
      "MONETARY_MASS_SHARE_LABEL": "Promedio miembro",
      "UD_TITLE": "Evolución del dividendo universales",
      "MEMBERS_COUNT_TITLE": "Evolución del número de miembros",
      "MEMBERS_COUNT_LABEL": "Número de miembros"
    },
    "PEER": {
      "VIEW": {
        "BLOCK_COUNT_LABEL": "Número de bloques calculados",
        "BLOCK_COUNT": "{{count}} bloques",
        "NO_BLOCK": "Ningún bloque"
      }
    }
  }
}
);

$translateProvider.translations("fr-FR", {
  "NETWORK": {
    "VIEW": {
      "BTN_GRAPH": "Statistiques"
    }
  },
  "GRAPH": {
    "COMMON": {
      "LINEAR_SCALE" : "Echelle linéaire",
      "LOGARITHMIC_SCALE" : "Echelle logarithmique",
      "BTN_SHOW_STATS": "Voir les statistiques",
      "BTN_SHOW_DETAILED_STATS": "Statistiques détaillées",
      "RANGE_DURATION_DIVIDER": "Unité de temps :",
      "RANGE_DURATION": {
        "HOUR": "Heure",
        "DAY": "Jour",
        "MONTH": "Mois"
      },
      "MAX_AGE": {
        "DAY": "Depuis 24h",
        "WEEK": "Depuis une semaine",
        "MONTH": "Depuis un mois",
        "QUARTER": "Depuis 3 mois",
        "SEMESTER": "Depuis 6 mois",
        "YEAR": "Depuis un an",
        "FOREVER": "Depuis toujours"
      }
    },
    "ACCOUNT": {
      "TITLE": "Statistiques",
      "BTN_SHOW_STATS": "Voir les statistiques du compte",
      "BALANCE_DIVIDER": "Situation du compte",
      "BALANCE_TITLE": "Evolution du compte {{pubkey|formatPubkey}}",
      "TX_RECEIVED_LABEL": "Recettes",
      "TX_SENT_LABEL": "Dépenses",
      "TX_ACCUMULATION_LABEL": "Bilan des transactions",
      "UD_LABEL": "DU",
      "UD_ACCUMULATION_LABEL": "Bilan des DU",
      "BALANCE_LABEL": "Solde",
      "WOT_DIVIDER": "Toile de confiance",
      "CERTIFICATION_TITLE": "Nombre de certifications - {{pubkey|formatPubkey}}",
      "RECEIVED_CERT_LABEL": "Total reçues",
      "RECEIVED_CERT_DELTA_LABEL": "Variation reçues",
      "GIVEN_CERT_LABEL": "Total envoyées",
      "GIVEN_CERT_DELTA_LABEL": "Variation envoyées",
      "INPUT_CHART_TITLE": "Somme des flux entrants, par émetteur :",
      "OUTPUT_CHART_TITLE": "Somme des flux sortants, par destinaire :"
    },
    "BLOCKCHAIN": {
      "TITLE": "Statistiques",
      "BLOCKS_ISSUERS_DIVIDER": "Analyse de la répartition du calcul",
      "BLOCKS_ISSUERS_HELP": "<b>{{issuerCount|formatInteger}} membres</b> ont calculé <b>{{blockCount|formatInteger}} blocs</b>",
      "BLOCKS_ISSUERS_TITLE": "Nombre de blocs calculés par membre",
      "BLOCKS_ISSUERS_LABEL": "Nombre de blocs",
      "TX_DIVIDER": "Analyse des transactions",
      "TX_AMOUNT_TITLE": "Volume des transactions {{currency|abbreviate}}",
      "TX_AMOUNT_PUBKEY_TITLE": "Volume des transactions calculées par {{issuer | formatPubkey}}",
      "TX_AMOUNT_LABEL": "Volume échangé",
      "TX_COUNT_TITLE": "Nombre de transactions écrites",
      "TX_COUNT_LABEL": "Nombre de transactions",
      "TX_AVG_BY_BLOCK": "Nombre moyen de transactions / bloc"
    },
    "CURRENCY": {
      "MONETARY_MASS_TITLE": "Evolution de la masse monétaire",
      "MONETARY_MASS_LABEL": "Masse monétaire",
      "MONETARY_MASS_SHARE_LABEL": "Moyenne par membre",
      "UD_TITLE": "Evolution du dividende universel",
      "MEMBERS_COUNT_TITLE": "Evolution du nombre de membres",
      "MEMBERS_COUNT_LABEL": "Nombre de membres",
      "MEMBERS_DELTA_TITLE": "Variation du nombre de membres",
      "IS_MEMBER_DELTA_LABEL": "Prises en compte d'adhésion",
      "WAS_MEMBER_DELTA_LABEL": "Pertes d'adhésion",
      "PENDING_DELTA_LABEL": "Demandes d'adhésion"
    },
    "PEER": {
      "VIEW": {
        "BLOCK_COUNT_LABEL": "Nombre de blocs calculés",
        "BLOCK_COUNT": "{{count}} blocs",
        "NO_BLOCK": "Aucun bloc"
      }
    },
    "DOC_STATS": {
      "TITLE": "Statistiques de stockage",
      "USER": {
        "TITLE": "Nombre de documents liés à un compte",
        "USER_PROFILE": "Profils utilisateur",
        "USER_SETTINGS": "Paramètres sauvegardés"
      },
      "USER_DELTA": {
        "TITLE": "Variation du nombre de documents liés à un compte",
        "USER_PROFILE": "Profils utilisateur",
        "USER_SETTINGS": "Paramètres sauvegardés"
      },
      "MESSAGE": {
        "TITLE": "Nombre de documents liés à la communication",
        "MESSAGE_INBOX": "Messages en boîte de réception",
        "MESSAGE_OUTBOX": "Messages envoyés sauvegardés",
        "INVITATION_CERTIFICATION": "Invitations à certifier"
      },
      "SOCIAL": {
        "TITLE": "Nombre de pages ou groupes",
        "PAGE_COMMENT": "Commentaires",
        "PAGE_RECORD": "Pages",
        "GROUP_RECORD": "Groupes"
      },
      "OTHER": {
        "TITLE": "Autres documents",
        "HISTORY_DELETE": "Suppressions de documents"
      }
    },
    "SYNCHRO": {
      "TITLE": "Statistiques de synchronisations",
      "COUNT": {
        "TITLE": "Volume synchronisé",
        "INSERTS": "Insertions",
        "UPDATES": "Mises à jour",
        "DELETES": "Suppressions"
      },
      "PEER": {
        "TITLE": "Noeuds requêtés",
        "ES_USER_API": "Noeuds avec données utilisateurs",
        "ES_SUBSCRIPTION_API": "Noeuds avec services en ligne"
      },
      "PERFORMANCE": {
        "TITLE": "Performances d'exécution",
        "DURATION": "Temps d'exécution (ms)"
      }
    },
    "NETWORK": {
      "TITLE": "Statistiques réseau",
      "ENDPOINT_COUNT_TITLE": "Nombre de points d'accès",
      "ENDPOINT_DELTA_TITLE": "Variation du nombre de points d'accès"
    }
  }
}
);

$translateProvider.translations("it-IT", {
   "NETWORK": {
     "VIEW": {
       "BTN_GRAPH": "Statistiche"
     }
   },
   "GRAPH": {
     "COMMON": {
       "LINEAR_SCALE" : "Scala lineare",
       "LOGARITHMIC_SCALE" : "Scala logaritmica",
       "BTN_SHOW_STATS": "Vedere le statistiche",
       "BTN_SHOW_DETAILED_STATS": "Statistiche dettagliate",
       "RANGE_DURATION_DIVIDER": "Unità di tempo :",
       "RANGE_DURATION": {
         "HOUR": "Ora",
         "DAY": "Giorno",
         "MONTH": "Mese"
       }
     },
     "ACCOUNT": {
       "TITLE": "Statistiche",
       "BTN_SHOW_STATS": "Visualizzare le statistiche del conto",
       "BALANCE_DIVIDER": "Situazione del conto",
       "BALANCE_TITLE": "Evoluzione del conto {{pubkey|formatPubkey}}",
       "TX_RECEIVED_LABEL": "Incassi",
       "TX_SENT_LABEL": "Spese",
       "TX_ACCUMULATION_LABEL": "Bilancio delle transazioni",
       "UD_LABEL": "DU",
       "UD_ACCUMULATION_LABEL": "Bilancio dei DU",
       "BALANCE_LABEL": "Saldo",
       "WOT_DIVIDER": "Tela di fiducia",
       "CERTIFICATION_TITLE": "Numero di certificazioni - {{pubkey|formatPubkey}}",
       "RECEIVED_CERT_LABEL": "Totale ricevute",
       "RECEIVED_CERT_DELTA_LABEL": "Variazione ricevute",
       "GIVEN_CERT_LABEL": "Totale inviate",
       "GIVEN_CERT_DELTA_LABEL": "Variazione inviate",
       "INPUT_CHART_TITLE": "Somma dei flussi in entrata, par emittente :",
       "OUTPUT_CHART_TITLE": "Somma dei flussi in uscita, per destinatario :"
     },
     "BLOCKCHAIN": {
       "TITLE": "Statistiche",
       "BLOCKS_ISSUERS_DIVIDER": "Analisi della ripartizione del calcolo",
       "BLOCKS_ISSUERS_HELP": "<b>{{issuerCount|formatInteger}} membri</b> hanno calcolato <b>{{blockCount|formatInteger}} blocchi</b>",
       "BLOCKS_ISSUERS_TITLE": "Numero di blocchi calcolati a membro",
       "BLOCKS_ISSUERS_LABEL": "Numero di blocchi",
       "TX_DIVIDER": "Analisi delle transazioni",
       "TX_AMOUNT_TITLE": "Volume delle transazioni",
       "TX_AMOUNT_PUBKEY_TITLE": "Volume delle transazioni calcolato da {{issuer | formatPubkey}}",
       "TX_AMOUNT_LABEL": "Volume scambiato",
       "TX_COUNT_TITLE": "Numero di transazioni scritte",
       "TX_COUNT_LABEL": "Numero di transazioni",
       "TX_AVG_BY_BLOCK": "Numero medio di transazioni / blocco"
     },
     "CURRENCY": {
       "MONETARY_MASS_TITLE": "Evoluzione della massa monetaria",
       "MONETARY_MASS_LABEL": "Massa monetaria",
       "MONETARY_MASS_SHARE_LABEL": "Media a membro",
       "UD_TITLE": "Evoluzione del Dividendo Universale",
       "MEMBERS_COUNT_TITLE": "Evoluzione del numero di membri",
       "MEMBERS_COUNT_LABEL": "Numero di membri"
     },
     "PEER": {
       "VIEW": {
         "BLOCK_COUNT_LABEL": "Numero di blocchi calcolati",
         "BLOCK_COUNT": "{{count}} blocchi",
         "NO_BLOCK": "Nessun blocco"
       }
     },
     "DOC_STATS": {
       "TITLE": "Statistiche di stoccaggio",
       "USER": {
         "TITLE": "Numero di documenti legati ad un conto",
         "USER_PROFILE": "Profili dell'utente",
         "USER_SETTINGS": "Impostazioni salvate"
       },
       "MESSAGE": {
         "TITLE": "Numero di documenti legati alla conversazione",
         "MESSAGE_INBOX": "Messaggi in arrivo",
         "MESSAGE_OUTBOX": "Messaggi inviati salvati",
         "INVITATION_CERTIFICATION": "Invitazioni da certificare"
       },
       "SOCIAL": {
         "TITLE": "Numero di pagine o gruppi",
         "PAGE_COMMENT": "Commenti",
         "PAGE_RECORD": "Pagine",
         "GROUP_RECORD": "Gruppi"
       },
       "OTHER": {
         "TITLE": "Altri documenti",
         "HISTORY_DELETE": "Cronologia eliminazione documenti"
       }
     },
     "SYNCHRO": {
       "TITLE": "Statistiche di sincronizzazioni",
       "COUNT": {
         "TITLE": "Volume sincronizzato",
         "INSERTS": "Inserimenti",
         "UPDATES": "Aggiornamenti",
         "DELETES": "Eliminazioni"
       },
       "PEER": {
         "TITLE": "Nodi interrogati",
         "ES_USER_API": "Nodi dati utenti",
         "ES_SUBSCRIPTION_API": "Noeuds servizi online"
       },
       "PERFORMANCE": {
         "TITLE": "Prestazioni (performance) di esecuzione",
         "DURATION": "Tempo di esecuzione (ms)"
       }
     }
   }
 }
);

$translateProvider.translations("nl-NL", {
  "NETWORK": {
    "VIEW": {
      "BTN_GRAPH": "Statistieken"
    }
  },
  "GRAPH": {
    "COMMON": {
      "LINEAR_SCALE" : "Lineaire schaal",
      "LOGARITHMIC_SCALE" : "Logaritmische schaal",
      "BTN_SHOW_STATS": "Zie statistieken",
      "BTN_SHOW_DETAILED_STATS": "Gedetailleerde statistieken",
      "RANGE_DURATION_DIVIDER": "Stap eenheid:",
      "RANGE_DURATION": {
        "HOUR": "Groep per <b>uur</b>",
        "DAY": "Groep per <b>dag</b>",
        "MONTH": "Groep per <b>maand</b>"
      }
    },
    "ACCOUNT": {
      "INPUT_CHART_TITLE": "Som van de binnenkomende stroom, door de zender:",
      "OUTPUT_CHART_TITLE": "Som van de uitstroom per bestemming:"
    },
    "BLOCKCHAIN": {
      "TITLE": "Statistieken",
      "BLOCKS_ISSUERS_DIVIDER": "Schriftelijke blokken door leden",
      "BLOCKS_ISSUERS_HELP": "<b>{{issuerCount|formatInteger}} leden</b> berekend <b>{{blockCount|formatInteger}} blokken</b>",
      "BLOCKS_ISSUERS_TITLE": "Aantal blokken berekend per lid",
      "BLOCKS_ISSUERS_LABEL": "Aantal blokken",
      "TX_DIVIDER": "Analyse van transacties",
      "TX_AMOUNT_TITLE": "Trading volume",
      "TX_AMOUNT_PUBKEY_TITLE": "Trading volume dat wordt berekend door {{issuer | formatPubkey}}",
      "TX_AMOUNT_LABEL": "Verhandeld volume",
      "TX_COUNT_TITLE": "Aantal schriftelijke transacties",
      "TX_COUNT_LABEL": "Aantal transacties",
      "TX_AVG_BY_BLOCK": "Gemiddeld aantal transacties / blok"
    },
    "CURRENCY": {
      "MONETARY_MASS_TITLE": "Evolutie van de monetaire massa",
      "MONETARY_MASS_LABEL": "Monetaire massa",
      "MONETARY_MASS_SHARE_LABEL": "Gemiddelde leden",
      "UD_TITLE": "Ontwikkeling van de universele dividend",
      "MEMBERS_COUNT_TITLE": "Evolutie van het aantal leden",
      "MEMBERS_COUNT_LABEL": "Aantal leden"
    }
  }
}
);

$translateProvider.translations("ca", {
  "COMMON": {
    "ABUSES_TEXT": "{{total}} person{{total > 1 ? 'es' : 'a'}} {{total > 1 ? 'han' : 'ha'}} informat d'un problema",
    "BTN_LIKE": "M'agrada",
    "BTN_LIKE_REMOVE": "Ja no m'agrada",
    "BTN_REMOVE_REPORTED_ABUSE": "Cancel·la el meu informe",
    "BTN_REPORT_ABUSE_DOTS": "Informeu d'un problema o un abús...",
    "COMMENT_HELP": "Comentari",
    "LIKES_TEXT": "A {{total}} person{{total > 1 ? 'es' : 'a'}} {{total > 1 ? 'els' : 'li'}} agrada aquesta pàgina",
    "NOTIFICATION": {
        "HAS_UNREAD": "Hi ha {{count}} notificaci{{count>0?'ons':'ó'}} sense llegir",
        "TITLE": "Notificació nova | {{'COMMON.APP_NAME'|translate}}"
    },
    "REPORT_ABUSE": {
      "ASK_DELETE": "Solicitud d'eliminació?",
      "CONFIRM": {
          "SENT": "Informe enviat. Gràcies!"
      },
      "REASON_HELP": "Jo explico el problema...",
      "SUB_TITLE": "Si us plau feu-nos cinc cèntims del problema:",
      "TITLE": "Comuniqueu un problema"
    },

    "CATEGORY": "Categoria",
    "CATEGORIES": "Categories",
    "CATEGORY_SEARCH_HELP": "Cerca",
    "LAST_MODIFICATION_DATE": "Actualitzat a",
    "SUBMIT_BY": "Enviat per",
    "BTN_PUBLISH": "Publica",
    "BTN_PICTURE_DELETE": "Suprimeix",
    "BTN_PICTURE_FAVORISE": "Principal",
    "BTN_PICTURE_ROTATE": "Gira",
    "BTN_ADD_PICTURE": "Afegeix una foto",
    "NOTIFICATIONS": {
      "TITLE": "Notificacions",
      "MARK_ALL_AS_READ": "Marca-ho tot com a llegit",
      "NO_RESULT": "Sense notificacions",
      "SHOW_ALL": "Veure-ho tot",
      "LOAD_NOTIFICATIONS_FAILED": "Ha fallat la càrrega de les notificacions"
    }
  },
  "DOCUMENT": {
    "HASH": "Hash: ",
    "LOOKUP": {
      "BTN_COMPACT": "Compacta",
      "HAS_CREATE_OR_UPDATE_PROFILE": "ha creat o modificat el seu perfil",
      "LAST_DOCUMENTS_DOTS": "Últims documents :",
      "TITLE": "Búsqueda de documents",
      "BTN_ACTIONS": "Accions",
      "SEARCH_HELP": "issuer:AAA*, time:1508406169",
      "LAST_DOCUMENTS": "Últims documents",
      "SHOW_QUERY": "Mostra la búsqueda",
      "HIDE_QUERY": "Amaga la búsqueda",
      "HEADER_TIME": "Data/Hora",
      "HEADER_ISSUER": "Emissor",
      "HEADER_RECIPIENT": "Destinatari",
      "HEADER_AMOUNT": "Import",
      "READ": "Llegit",
      "BTN_REMOVE": "Sumpreix aquest document",
      "POPOVER_ACTIONS": {
        "TITLE": "Accions",
        "REMOVE_ALL": "Suprimeix aquests documents..."
      }
    },
    "INFO": {
      "REMOVED": "Document suprimit"
    },
    "CONFIRM": {
      "REMOVE": "Vol <b>suprimir aquest document</b>?",
      "REMOVE_ALL": "Vol <b>suprimir aquests documents</b>?"
    },
    "ERROR": {
      "LOAD_DOCUMENTS_FAILED": "Error en buscar els documents",
      "REMOVE_FAILED": "Error en suprimir el document",
      "REMOVE_ALL_FAILED": "Error en suprimir els documents"
    }
  },
  "MENU": {
    "REGISTRY": "Pàgines",
    "USER_PROFILE": "El meu perfil",
    "MESSAGES": "Missatges",
    "NOTIFICATIONS": "Notificacions",
    "INVITATIONS": "Invitacions"
  },
  "ACCOUNT": {
    "NEW": {
      "ORGANIZATION_ACCOUNT": "Compte per a una organització",
      "ORGANIZATION_ACCOUNT_HELP": "Si representeu una empresa, una associació, etc.<br/>Aquest compte no crearà cap divident universal."
    },
    "EVENT": {
      "MEMBER_WITHOUT_PROFILE": "Per aconseguir les vostres certificacions més ràpid, completi <a ui-sref=\"app.edit_profile\">el seu perfil d'usuari</a>. Els membres donaran la seva confiança més fàcilment a una identitat verificable."
    },
    "ERROR": {
      "WS_CONNECTION_FAILED": "Cesium no pot rebre les notificacions, degut a un error tècnic (connexió al node de dades Cesium+).<br/><br/>Si el problema persisteix, si us plau <b>trieu un altre node de dades</b> als ajustos de Cesium+."
    }
  },
  "WOT": {
    "BTN_SUGGEST_CERTIFICATIONS_DOTS": "Suggeriu identidats a certificar...",
    "BTN_ASK_CERTIFICATIONS_DOTS": "Demaneu a uns membres que us certifiquin...",
    "BTN_ASK_CERTIFICATION": "Demaneu una certificació",
    "SUGGEST_CERTIFICATIONS_MODAL": {
      "TITLE": "Suggeriu certificacions",
      "HELP": "Trieu els vostres suggeriments"
    },
    "ASK_CERTIFICATIONS_MODAL": {
      "TITLE": "Sol·liciteu certificacions",
      "HELP": "Seleccioneu els destinataris"
    },
    "SEARCH": {
      "DIVIDER_PROFILE": "Comptes",
      "DIVIDER_PAGE": "Pàgines",
      "DIVIDER_GROUP": "Grups"
    },
    "CONFIRM": {
      "SUGGEST_CERTIFICATIONS": "Vols <b>enviar aquests suggeriments de certificatió</b>?",
      "ASK_CERTIFICATION": "Vols <b>enviar una sol·licitut de certificació</b>?",
      "ASK_CERTIFICATIONS": "Vols <b>enviar una sol·licitut de certificació</b> a aquestes persones?"
    }
  },
  "INVITATION": {
    "TITLE": "Invitacions",
    "NO_RESULT": "No hi ha cap invitació en espera",
    "BTN_DELETE_ALL": "Suprimeix totes les invitacions",
    "BTN_DELETE": "Suprimeix la invitació",
    "BTN_NEW_INVITATION": "Nova invitació",
    "ASK_CERTIFICATION": "<a href=\"#/app/wot/{{pubkey}}/{{::uid}}\">{{::name||uid}}</a> ha sol·licitat la teva certificació",
    "SUGGESTION_CERTIFICATION": "<a href=\"#/app/wot/{{::pubkey}}/{{::uid}}\">{{::name||uid}}</a> ha sigut suggerit per certificar-se",
    "SUGGESTED_BY": "Suggeriment enviat per <a class=\"positive\" href=\"#/app/wot/{{::issuer.pubkey}}/{{::issuer.uid}}\">{{::issuer.name||issuer.uid}}</a>",
    "NOTIFICATIONS": {
      "TITLE": "Invitacions"
    },
    "LIST": {
      "TITLE": "Invitacions"
    },
    "NEW": {
      "TITLE": "Nova invitació",
      "RECIPIENTS": "A",
      "RECIPIENTS_HELP": "Destinataris de la invitació",
      "RECIPIENTS_MODAL_TITLE": "Destinataris",
      "RECIPIENTS_MODAL_HELP": "Si us plau, tria els destinataris:",
      "SUGGESTION_IDENTITIES": "Suggeriment de certificació",
      "SUGGESTION_IDENTITIES_HELP": "Certificacions a suggerir",
      "SUGGESTION_IDENTITIES_MODAL_TITLE": "Suggeriments",
      "SUGGESTION_IDENTITIES_MODAL_HELP": "Si us plau, tria els teus suggeriments:"
    },
    "CONFIRM": {
      "DELETE_ALL_CONFIRMATION": "La supresión de las invitaciones es una <b>operación ireversible</b>.<br/><br/>¿ Desea continuar ?",
      "SEND_INVITATIONS_TO_CERTIFY": "¿ Desea <b>mandar esta invitación a certificar</b> ?"
    },
    "INFO": {
      "INVITATION_SENT": "Invitación mandada"
    },
    "ERROR": {
      "LOAD_INVITATIONS_FAILED": "Fallo en la carga de las invitaciones",
      "REMOVE_INVITATION_FAILED": "Fallo durante la supresión de la invitación",
      "REMOVE_ALL_INVITATIONS_FAILED": "Fallo durante la supresión de las invitaciones",
      "SEND_INVITATION_FAILED": "Fallo durante el envío de la invitación",
      "BAD_INVITATION_FORMAT": "<span class=\"assertive\"><i class=\"ion-close-circled\"></i> Invitación ilegible (formato desconocido)</span> - mandada por <a ui-sref=\"app.wot_identity({pubkey: '{{::pubkey}}', uid: '{{::uid}}' })\">{{::name||uid}}</a>"
    }
  },
  "COMMENTS": {
    "DIVIDER": "Comentarios",
    "SHOW_MORE_COMMENTS": "Visualizar los comentarios anteriores",
    "COMMENT_HELP": "Su comentario, preguntas, etc.",
    "COMMENT_HELP_REPLY_TO": "Su repuesta…",
    "BTN_SEND": "Mandar",
    "POPOVER_SHARE_TITLE": "Mensaje #{{number}}",
    "MODIFIED_ON": "modificado el {{time|formatDate}}",
    "MODIFIED_PARENTHESIS": "(modificado entonces)",
    "REPLY": "Responder",
    "REPLY_TO": "Repuesta a :",
    "REPLY_TO_LINK": "En repuesta a ",
    "REPLY_TO_DELETED_COMMENT": "En repuesta a un comentario suprimido",
    "REPLY_COUNT": "{{replyCount}} repuestas",
    "DELETED_COMMENT": "Comentario suprimido",
    "ERROR": {
      "FAILED_SAVE_COMMENT": "Fallo durante el respaldo del comentario",
      "FAILED_REMOVE_COMMENT": "Fallo durante la supresión del comentario"
    }
  },
  "MESSAGE": {
    "REPLY_TITLE_PREFIX": "Rep: ",
    "FORWARD_TITLE_PREFIX": "Tr: ",
    "BTN_REPLY": "Responder",
    "BTN_COMPOSE": "Nuevo mensaje",
    "BTN_WRITE": "Escribir",
    "NO_MESSAGE_INBOX": "Ningun mensaje recibido",
    "NO_MESSAGE_OUTBOX": "Ningun mensaje mandado",
    "NOTIFICATIONS": {
      "TITLE": "Mensajes",
      "MESSAGE_RECEIVED": "Ha <b>recibido un mensaje</b><br/>de"
    },
    "LIST": {
      "INBOX": "Bandeja de entrada",
      "OUTBOX": "Mensajes enviados",
      "LAST_INBOX": "Nuevos mensajes",
      "LAST_OUTBOX": "Mensajes enviados",
      "BTN_LAST_MESSAGES": "Mensajes recientes",
      "TITLE": "Mensajes",
      "SEARCH_HELP": "Buscar en mensajes",
      "POPOVER_ACTIONS": {
        "TITLE": "Opciones",
        "DELETE_ALL": "Suprimir todos los mensajes"
      }
    },
    "COMPOSE": {
      "TITLE": "Nuevo mensaje",
      "TITLE_REPLY": "Responder",
      "SUB_TITLE": "Nuevo mensaje",
      "TO": "A",
      "OBJECT": "Objeto",
      "OBJECT_HELP": "Objeto",
      "ENCRYPTED_HELP": "Tenga en cuenta que este mensaje será cifrado antes del envío, con el fin de que solo el destinatario pueda leerlo, y que se tenga la seguridad de que la autoría es suya.",
      "MESSAGE": "Mensaje",
      "MESSAGE_HELP": "Contenido del mensaje",
      "CONTENT_CONFIRMATION": "El contenido del mensaje está vacío.<br/><br/>¿ Sin embargo, quiere mandar el mensaje ?"
    },
    "VIEW": {
      "TITLE": "Mensaje",
      "SENDER": "Enviado por",
      "RECIPIENT": "Enviado a",
      "NO_CONTENT": "Mensaje vacío",
      "DELETE": "Eliminar el mensaje"
    },
    "CONFIRM": {
      "REMOVE": "¿ Desea <b>suprimir este mensaje</b> ?<br/><br/>Esta operación es ireversible.",
      "REMOVE_ALL" : "¿ Desea <b>suprimir todos los mensajes</b> ?<br/><br/>Esta operación es ireversible.",
      "MARK_ALL_AS_READ": "¿ Desea <b>marcar todos los mensajes como leído</b> ?",
      "USER_HAS_NO_PROFILE": "Esta identidad no tiene ningún perfil Cesium+. Puede que no tenga habilitada la extensión Cesium+, y <b>no podrá ver su mensaje</b>.<br/><br/>¿ Desea <b>continuar</b> a pesar de todo ?"
    },
    "INFO": {
      "MESSAGE_REMOVED": "Mensaje suprimido",
      "All_MESSAGE_REMOVED": "Todos los mensajes fueron suprimido",
      "MESSAGE_SENT": "Mensaje mandado"
    },
    "ERROR": {
      "SEND_MSG_FAILED": "Fallo durante el envío del mensaje.",
      "LOAD_MESSAGES_FAILED": "Fallo durante la recuperación de los mensajes.",
      "LOAD_MESSAGE_FAILED": "Fallo durante la recuperación del mensaje.",
      "MESSAGE_NOT_READABLE": "Lectura del mensaje imposible.",
      "USER_NOT_RECIPIENT": "No está el destinatario de este mensaje : deciframiento imposible.",
      "NOT_AUTHENTICATED_MESSAGE": "La autenticidad del mensaje es dudosa o su contenido está corrupto.",
      "REMOVE_MESSAGE_FAILED": "Fallo en la supresión del mensaje",
      "MESSAGE_CONTENT_TOO_LONG": "Valor demasiado largo ({{maxLength}} carácteres max).",
      "MARK_AS_READ_FAILED": "Imposible marcar el mensaje como 'leído'.",
      "LOAD_NOTIFICATIONS_FAILED": "Fallo durante la recuperación de las notificaciones de mensajes.",
      "REMOVE_All_MESSAGES_FAILED": "Fallo durante la supresión de todos los mensajes.",
      "MARK_ALL_AS_READ_FAILED": "Fallo durante el marcaje de los mensajes como leído.",
      "RECIPIENT_IS_MANDATORY": "El destinatario es obligatorio."
    }
  },
  "BLOCKCHAIN": {
    "LOOKUP": {
      "SEARCH_HELP": "Número de bloque, hash, llave pública, etc.",
      "POPOVER_FILTER_TITLE": "Filtros",
      "HEADER_MEDIAN_TIME": "Fecha / Hora",
      "HEADER_BLOCK": "Bloque #",
      "HEADER_ISSUER": "Nodo emisor",
      "BTN_LAST": "Últimos bloques",
      "DISPLAY_QUERY": "Mostrar la consulta",
      "HIDE_QUERY": "Ocultar la consulta",
      "TX_SEARCH_FILTER": {
        "MEMBER_FLOWS": "Entradas/salidas de miembros",
        "EXISTING_TRANSACTION": "Con transacciones",
        "PERIOD": "<b class=\"ion-clock\"></b> Entre el <b class=\"gray\">{{params[1]|medianDateShort}}</b> ({{params[1]|medianTime}}) y el <b class=\"gray\">{{params[2]|medianDateShort}}</b> ({{params[2]|medianTime}})",
        "ISSUER": "<b class=\"ion-android-desktop\"></b> Calculado por <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}",
        "TX_PUBKEY": "<b class=\"ion-card\"></b> Transacciones que implican <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}"
      }
    },
    "ERROR": {
      "SEARCH_BLOCKS_FAILED": "Fallo en la búsqueda de los bloques."
    }
  },
  "GROUP": {
    "GENERAL_DIVIDER": "Informaciones generales",
    "LOCATION_DIVIDER": "Dirección",
    "SOCIAL_NETWORKS_DIVIDER": "Redes sociales y página web",
    "TECHNICAL_DIVIDER": "Informaciones técnicas",
    "CREATED_TIME": "Creada {{creationTime|formatFromNow}}",
    "NOTIFICATIONS": {
      "TITLE": "Invitaciones"
    },
    "LOOKUP": {
      "TITLE": "Grupos",
      "SEARCH_HELP": "Nombre de grupo, palabras , lugar, etc.",
      "LAST_RESULTS_LIST": "Nuevos grupos :",
      "OPEN_RESULTS_LIST": "Grupos abiertos :",
      "MANAGED_RESULTS_LIST": "Grupos cerrados :",
      "BTN_LAST": "Nuevos grupos",
      "BTN_NEW": "Añado un grupo"
    },
    "TYPE": {
      "TITLE": "Nuevo grupo",
      "SELECT_TYPE": "Tipo de grupo :",
      "OPEN_GROUP": "Grupo abierto",
      "OPEN_GROUP_HELP": "Un grupo abierto es accesible por cualquier miembro de la moneda.",
      "MANAGED_GROUP": "Grupo administrado",
      "MANAGED_GROUP_HELP": "Un grupo administrado es gestionado por administradores y moderadores, que pueden aceptar, rechazar o excluir un miembro en su seno.",
      "ENUM": {
        "OPEN": "Grupo abierto",
        "MANAGED": "Grupo administrado"
      }
    },
    "VIEW": {
      "POPOVER_SHARE_TITLE": "{{title}}",
      "MENU_TITLE": "Opciones",
      "REMOVE_CONFIRMATION" : "¿Desea eliminar este grupo?<br/><br/>Esta operación es irreversible."
    },
    "EDIT": {
      "TITLE": "Grupo",
      "TITLE_NEW": "Nuevo grupo",
      "RECORD_TITLE": "Título",
      "RECORD_TITLE_HELP": "Título",
      "RECORD_DESCRIPTION": "Descripción",
      "RECORD_DESCRIPTION_HELP": "Descripción"
    },
    "ERROR": {
      "SEARCH_GROUPS_FAILED": "Fallo en la búsqueda de grupos",
      "REMOVE_RECORD_FAILED": "Error al eliminar el grupo"
    },
    "INFO": {
      "RECORD_REMOVED" : "Grupo eliminado"
    }
  },
  "REGISTRY": {
    "CATEGORY": "Actividad principal",
    "GENERAL_DIVIDER": "Informaciones generales",
    "LOCATION_DIVIDER": "Dirección",
    "SOCIAL_NETWORKS_DIVIDER": "Redes sociales y sitio web",
    "TECHNICAL_DIVIDER": "Informaciones técnicas",
    "BTN_SHOW_WOT": "Personas",
    "BTN_SHOW_WOT_HELP": "Buscar personas",
    "BTN_SHOW_PAGES": "Páginas",
    "BTN_SHOW_PAGES_HELP": "Búsqueda de páginas",
    "BTN_NEW": "Crear una página",
    "MY_PAGES": "Mis páginas",
    "NO_PAGE": "Sin páginas",
    "SEARCH": {
      "TITLE": "Páginas",
      "SEARCH_HELP": "Qué, Quién, ej: peluquería, restaurante Sol.",
      "BTN_ADD": "Nuevo",
      "BTN_LAST_RECORDS": "Páginas recientes",
      "BTN_ADVANCED_SEARCH": "búsqueda avanzada",
      "BTN_OPTIONS": "Búsqueda avanzada",
      "TYPE": "Tipo de página",
      "LOCATION_HELP": "Ciudad",
      "RESULTS": "Resultados",
      "RESULT_COUNT_LOCATION": "{{count}} Resultado{{count>0?'s':''}}, cerca de {{location}}",
      "RESULT_COUNT": "{{count}} resultado{{count>0?'s':''}}",
      "LAST_RECORDS": "Páginas recientes",
      "LAST_RECORD_COUNT_LOCATION": "{{count}} página{{count>0?'s':''}} reciente{{count>0?'s':''}}, cerca de {{location}}",
      "LAST_RECORD_COUNT": "{{count}} página{{count>0?'s':''}} reciente{{count>0?'s':''}}",
      "POPOVER_FILTERS": {
        "BTN_ADVANCED_SEARCH": "Opciones avanzadas"
      }
    },
    "VIEW": {
      "TITLE": "Anuario",
      "CATEGORY": "Actividad principal :",
      "LOCATION": "Dirección :",
      "MENU_TITLE": "Opciones",
      "POPOVER_SHARE_TITLE": "{{title}}",
      "REMOVE_CONFIRMATION" : "¿ Desea suprimir esta página ?<br/><br/>Esta operación es ireversible."
    },
    "TYPE": {
      "TITLE": "Nueva página",
      "SELECT_TYPE": "Tipo de página :",
      "ENUM": {
        "SHOP": "Comercio local",
        "COMPANY": "Empresa",
        "ASSOCIATION": "Asociación",
        "INSTITUTION": "Institución"
      }
    },
    "EDIT": {
      "TITLE": "Edición",
      "TITLE_NEW": "Nueva página",
      "RECORD_TYPE":"Tipo de página",
      "RECORD_TITLE": "Nombre",
      "RECORD_TITLE_HELP": "Nombre",
      "RECORD_DESCRIPTION": "Descripción",
      "RECORD_DESCRIPTION_HELP": "Descripción de la actividad",
      "RECORD_ADDRESS": "Calle",
      "RECORD_ADDRESS_HELP": "Calle, edificio…",
      "RECORD_CITY": "Ciudad",
      "RECORD_CITY_HELP": "Ciudad",
      "RECORD_SOCIAL_NETWORKS": "Redes sociales y sitio web",
      "RECORD_PUBKEY": "Llave pública",
      "RECORD_PUBKEY_HELP": "Llave pública para recibir pagos"
    },
    "WALLET": {
      "PAGE_DIVIDER": "Páginas",
      "PAGE_DIVIDER_HELP": "Las páginas se refieren a colectivos que aceptan moneda o la promocionan: tiendas, empresas, negocios, asociaciones, instituciones. Se almacenan fuera de la red de la moneda, en <a ui-sref=\"app.es_network\">la red Cesium+</a>."
    },
    "ERROR": {
      "LOAD_CATEGORY_FAILED": "Fallo en la carga de la lista de actividades",
      "LOAD_RECORD_FAILED": "Fallo durante la carga de la página",
      "LOOKUP_RECORDS_FAILED": "Fallo durante la ejecución de la búsqueda.",
      "REMOVE_RECORD_FAILED": "Fallo en la supresión de la página",
      "SAVE_RECORD_FAILED": "Fallo durante el respaldo",
      "RECORD_NOT_EXISTS": "Página inexistente",
      "GEO_LOCATION_NOT_FOUND": "Ciudad o código postal no encontrado"
    },
    "INFO": {
      "RECORD_REMOVED" : "Página suprimida",
      "RECORD_SAVED": "Página guardada"
    }
  },
  "PROFILE": {
    "PROFILE_DIVIDER": "Perfil Cesium+",
    "PROFILE_DIVIDER_HELP": "Estos son datos auxiliares, almacenados fuera de la red monetaria",
    "NO_PROFILE_DEFINED": "Ningún perfil Cesium+",
    "BTN_ADD": "Ingresar mi perfil",
    "BTN_EDIT": "Editar mi perfil",
    "BTN_DELETE": "Eliminar mi perfil",
    "BTN_REORDER": "Reordenar",
    "UID": "Seudónimo",
    "TITLE": "Nombre, Apellidos",
    "TITLE_HELP": "Nombre, Apellidos",
    "DESCRIPTION": "Sobre mí",
    "DESCRIPTION_HELP": "Sobre mí…",
    "SOCIAL_HELP": "http://...",
    "GENERAL_DIVIDER": "Informaciones generales",
    "SOCIAL_NETWORKS_DIVIDER": "Redes sociales, sitios web",
    "TECHNICAL_DIVIDER": "Informaciones técnicas",
    "MODAL_AVATAR": {
      "TITLE": "Foto de perfil",
      "SELECT_FILE_HELP": "Por favor, <b>elija una imagen</b>:",
      "BTN_SELECT_FILE": "Eligir una imagen",
      "RESIZE_HELP": "<b>Encuadre la imagen</b>, si es necesario. Un clic presionado sobre la imagen permite desplazarla. Haga clic en la zona inferior izquierda para hacer zoom.",
      "RESULT_HELP": "<b>Aquí está el resultado</b> tal como se verá sobre su perfil :"
    },
    "CONFIRM": {
      "DELETE": "¿Desea <b>eliminar su perfil Cesium+?</b><br/><br/>Esta operación es irreversible.",
      "DELETE_BY_MODERATOR": "¿Desea <b>eliminar este perfil Cesium+?</b><br/><br/>Esta operación es irreversible."
    },
    "ERROR": {
      "DELETE_PROFILE_FAILED": "Error durante la eliminación del perfil",
      "REMOVE_PROFILE_FAILED": "Error de eliminación del perfil",
      "LOAD_PROFILE_FAILED": "Fallo en la carga del perfil usuario.",
      "SAVE_PROFILE_FAILED": "Fallo durante el respaldo",
      "INVALID_SOCIAL_NETWORK_FORMAT": "Formato inválido: por favor, indique una dirección válida.<br/><br/>Ejemplos :<ul><li>- Una página Facebook (https://www.facebook.com/user)</li><li>- Una página web (http://www.misitio.es)</li><li>- Una dirección de correo (joe@dalton.com)</li></ul>",
      "IMAGE_RESIZE_FAILED": "Falló el redimensionado de la imagen"
    },
    "INFO": {
      "PROFILE_REMOVED": "Perfil eliminado",
      "PROFILE_SAVED": "Perfil guardado"
    },
    "HELP": {
      "WARNING_PUBLIC_DATA": "La información de su perfil <b>es pública</b>: visible también por personas <b>sin cuenta</b>.<br/>{{'PROFILE.PROFILE_DIVIDER_HELP'|translate}}"
    }
  },
  "LIKE": {
    "ERROR": {
        "FAILED_TOGGLE_LIKE": "Imposible ejecutar esta acción."
    }
  },
  "LOCATION": {
    "BTN_GEOLOC_ADDRESS": "Actualizar desde la dirección",
    "USE_GEO_POINT": "Aparecer en el mapa {{'COMMON.APP_NAME'|translate}}",
    "LOADING_LOCATION": "Encontrar la dirección…",
    "LOCATION_DIVIDER": "Dirección",
    "ADDRESS": "Calle",
    "ADDRESS_HELP": "Calle, número, etc…",
    "CITY": "Ciudad",
    "CITY_HELP": "Ciudad, País",
    "DISTANCE": "Distancia máxima alrededor de la ciudad",
    "DISTANCE_UNIT": "km",
    "DISTANCE_OPTION": "{{value}} {{'LOCATION.DISTANCE_UNIT'|translate}}",
    "SEARCH_HELP": "Ciudad, País",
    "PROFILE_POSITION": "Posición del perfil",
    "MODAL": {
      "TITLE": "Búsqueda de dirección",
      "SEARCH_HELP": "Ciudad, País",
      "ALTERNATIVE_RESULT_DIVIDER": "Resultados alternativos para <b>{{address}}</b> :",
      "POSITION": "Latitud/Longitud : {{lat}} / {{lon}}"
    },
    "ERROR": {
      "CITY_REQUIRED_IF_STREET": "Requerido si una calle ha sido llenada",
      "REQUIRED_FOR_LOCATION": "Campo obligatorio para aparecer en el mapa",
      "INVALID_FOR_LOCATION": "Dirección desconocida",
      "GEO_LOCATION_FAILED": "No se puede recuperar su ubicación Por favor usa el botón de búsqueda.",
      "ADDRESS_LOCATION_FAILED": "No se puede recuperar la posición de la dirección."
    }
  },
  "SUBSCRIPTION": {
    "SUBSCRIPTION_DIVIDER": "Servicios en línea",
    "SUBSCRIPTION_DIVIDER_HELP": "Los servicios en línea ofrecen servicios adicionales, proporcionados por un tercero.",
    "BTN_ADD": "Agregar un servicio",
    "BTN_EDIT": "Administrar mis servicios",
    "NO_SUBSCRIPTION": "Ningún servicio definido",
    "SUBSCRIPTION_COUNT": "Servicios / Suscripción",
    "EDIT": {
      "TITLE": "Servicios en línea",
      "HELP_TEXT": "Gestione sus suscripciones y otros servicios en línea aquí",
      "PROVIDER": "Proveedor:"
    },
    "TYPE": {
      "ENUM": {
        "EMAIL": "Recibir notificaciones por correo electrónico"
      }
    },
    "CONFIRM": {
      "DELETE_SUBSCRIPTION": "¿ Deseas <b>eliminar</b> esta suscripción ?"
    },
    "ERROR": {
      "LOAD_SUBSCRIPTIONS_FAILED": "Error al cargar servicios en línea",
      "ADD_SUBSCRIPTION_FAILED": "Error al agregar suscripción",
      "UPDATE_SUBSCRIPTION_FAILED": "Error durante la actualización de la suscripción",
      "DELETE_SUBSCRIPTION_FAILED": "Error al eliminar la suscripción"
    },
    "MODAL_EMAIL": {
      "TITLE" : "Notificación por correo electrónico",
      "HELP" : "Rellene este formulario para <b>ser notificado por correo electrónico</b> de los eventos de su cuenta. <br/> Su dirección de correo electrónico se cifrará y únicamente será visible para el proveedor de servicios.",
      "EMAIL_LABEL" : "Su correo electrónico :",
      "EMAIL_HELP": "maria@dominio.com",
      "FREQUENCY_LABEL": "Frecuencia de las notificaciones :",
      "FREQUENCY_DAILY": "Diaria",
      "FREQUENCY_WEEKLY": "Semanal",
      "PROVIDER": "Proveedor de servicio :"
    }
  },
  "ES_PEER": {
    "DOCUMENT_COUNT": "Número de documentos",
    "DOCUMENTS": "Documentos",
    "EMAIL_SUBSCRIPTION_COUNT": "{{emailSubscription}} suscrito/a{{emailSubscription ? 's' : ''}} a notificaciones por correo",
    "NAME": "Nombre",
    "SOFTWARE": "Software"
  },
  "ES_SETTINGS": {
    "PLUGIN_NAME": "Cesium+",
    "PLUGIN_NAME_HELP": "Perfiles, notificaciones, mensajes privados",
    "ENABLE_TOGGLE": "Activar la extensión",
    "ENABLE_MESSAGE_TOGGLE": "Activar los mensajes privados",
    "ENABLE_REMOTE_STORAGE": "Activar el almacenamiento remoto",
    "ENABLE_REMOTE_STORAGE_HELP": "Permite almacenar (con cifrado) sus ajustes en los nodos Cesium+",
    "PEER": "Dirección del nodo de datos",
    "POPUP_PEER": {
      "TITLE" : "Nodo de datos",
      "HELP" : "Ingrese la dirección del nodo que quiere utilizar:",
      "PEER_HELP": "servidor.dominio.com:puerto"
    },
    "NOTIFICATIONS": {
      "DIVIDER": "Notificaciones",
      "HELP_TEXT": "Active los tipos de notificaciones que desea recibir:",
      "ENABLE_TX_SENT": "Notificar la validación de los <b>pagos emitidos</b>",
      "ENABLE_TX_RECEIVED": "Notificar la validación de los <b>pagos recibidos</b>",
      "ENABLE_CERT_SENT": "Notificar la validación de las <b>certificaciones emitidas</b>",
      "ENABLE_CERT_RECEIVED": "Notificar la validación de las <b>certificaciones recibidas</b>",
      "ENABLE_HTML5_NOTIFICATION": "Avisar con cada nueva notificación",
      "ENABLE_HTML5_NOTIFICATION_HELP": "Abre una pequeña ventana emergente con cada nueva notificación."
    },
    "CONFIRM": {
      "ASK_ENABLE_TITLE": "Otras funcionalidades",
      "ASK_ENABLE": "La extensión de Cesium+ está deshabilitada en sus ajutes, desactivando ciertas funcionalidades: <ul><li>&nbsp;&nbsp;<b><i class=\"icon ion-person\"></i> Perfiles de usuario/a</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-android-notifications\"></i> Notificaciones</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-email\"></i> Mensajes privados</b>.</ul><br/><br/>¿<b>Desea re-activar</b> la extensión?"
    }
  },
  "ES_WALLET": {
    "ERROR": {
      "RECIPIENT_IS_MANDATORY": "Un destinatario es obligatorio para el cifrado."
    }
  },
  "EVENT": {
    "NODE_STARTED": "Su nodo ES API <b>{{params[0]}}</b> es comenzado",
    "NODE_BMA_DOWN": "El nodo <b>{{params[0]}}:{{params[1]}}</b> (utilizado por su nodo ES API) <b>no es localizable</b>.",
    "NODE_BMA_UP": "El nodo <b>{{params[0]}}:{{params[1]}}</b> es de nuevo accesible.",
    "MEMBER_JOIN": "Ahora es <b>miembro</b> de la moneda <b>{{params[0]}}</b> !",
    "MEMBER_LEAVE": "No es <b>miembro</b> de la moneda <b>{{params[0]}}</b>!",
    "MEMBER_EXCLUDE": "Usted ya no es miembro de la moneda <b>{{params[0]}}</b>, por falta de renovación o certificaciones.",
    "MEMBER_REVOKE": "Su membresía ha sido revocada. Ya no es miembro de la moneda <b>{{params[0]}}</b>.",
    "MEMBER_ACTIVE": "Su membresía a <b>{{params[0]}}</b> ha sido <b>renovada con éxito</b>.",
    "TX_SENT": "Su <b>pago</b> a <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> fue efectuado.",
    "TX_SENT_MULTI": "Su <b>pago</b> a <b>{{params[1]}}</b> fue efectuado.",
    "TX_RECEIVED": "Ha <b>recibido un pago</b> de <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "TX_RECEIVED_MULTI": "Ha <b>recibido un pago</b> de <b>{{params[1]}}</b>.",
    "CERT_SENT": "Su <b>certificación</b> a <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> fue efectuada.",
    "CERT_RECEIVED": "Ha <b>recibido una certificación</b> de <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "USER": {
        "ABUSE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha reportardo su perfil",
        "DELETION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha reportado un perfil para suprimir : <b>{{params[2]}}</b>",
        "FOLLOW_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> sigue la actividad de su perfil",
        "LIKE_RECEIVED": "A <span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> le ha gustado su perfil</b>",
        "MODERATION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> os pide moderación sobre el perfil : <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
        "STAR_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> os ha puntuado con ({{params[3]}} <b class=\"ion-star\">)"
    },
    "PAGE": {
      "ABUSE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha reportardo su página : <b>{{params[2]}}</b>",
      "DELETION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha reportado una página para suprimir : <b>{{params[2]}}</b>",
      "FOLLOW_CLOSE": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha cerrado la página : <b>{{params[2]}}</b>",
      "FOLLOW_NEW": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha añadido la página : <b>{{params[2]}}</b>",
      "FOLLOW_NEW_COMMENT": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha comentado la página : <b>{{params[2]}}</b>",
      "FOLLOW_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> sigue su página : <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha modificado la página : <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE_COMMENT": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha modificado su comentario en la página : <b>{{params[2]}}</b>",
      "MODERATION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> os pide moderación sobre la página : <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",

      "NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha comentado su referencia : <b>{{params[2]}}</b>",
      "UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha modificado su comentario sobre su referencia : <b>{{params[2]}}</b>",
      "NEW_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha contestado a su comentario sobre la referencia : <b>{{params[2]}}</b>",
      "UPDATE_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha modificado la repuesta a su comentario sobre la referencia : <b>{{params[2]}}</b>"
    }
  },
  "CONFIRM": {
    "ES_USE_FALLBACK_NODE": "Nodo de datos <b>{{old}}</b> inalcanzable o dirección no válida.<br/><br/>¿Desea utilizar temporalmente el nodo de datos <b>{{new}}</b>?"
  },
  "ERROR": {
    "ES_CONNECTION_ERROR": "Nodo de datos <b>{{server}}</b> inalcanzable o dirección no válida.<br/><br/>Cesium continuará funcionando, <b>sin la extensión Cesium+</b> (perfiles de usuario, mensajes privados), mapas y gráficos).<br/><br/>Verifique su conexión a Internet, o cambie el nodo de datos en <a class=\"positive\" ng-click=\"doQuickFix('settings')\"> los ajustes de la extensión </a>.",
    "ES_MAX_UPLOAD_BODY_SIZE": "El volumen de datos a enviar excede el límite establecido por el servidor.<br/><br/>Por favor, inténtelo de nuevo después, por ejemplo, borrando fotos."
  }
}
);

$translateProvider.translations("de-DE", {
  "COMMON": {
    "CATEGORY": "Kategorie",
    "CATEGORIES": "Kategorien",
    "CATEGORY_SEARCH_HELP": "Suche",
    "COMMENT_HELP": "Kommentar",
    "LAST_MODIFICATION_DATE": "Aktualisiert am",
    "SUBMIT_BY": "Eingereicht von",
    "BTN_LIKE": "Mag ich",
    "BTN_LIKE_REMOVE": "Mag ich nicht mehr",
    "LIKES_TEXT": "{{total}} Person{{total > 1? 'en': ''}} {{total > 1? 'haben': 'hat'}} mögen diese Seite",
    "ABUSES_TEXT": "{{total}} Person{{total > 1? 'en': ''}} {{total > 1? 'haben' : 'hat'}} ein Problem gemeldet",
    "BTN_REPORT_ABUSE_DOTS": "Melden Sie ein Problem oder einen Missbrauch...",
    "BTN_REMOVE_REPORTED_ABUSE": "Meine Meldung zurückziehen",
    "BTN_PUBLISH": "Veröffentlichen",
    "BTN_PICTURE_DELETE": "Löschen",
    "BTN_PICTURE_FAVORISE": "Vorgabe",
    "BTN_PICTURE_ROTATE": "Drehen",
    "BTN_ADD_PICTURE": "Bild hinzufügen",
    "NOTIFICATION": {
      "TITLE": "Neue Benachrichtigung | {{'COMMON.APP_NAME'|translate}}",
      "HAS_UNREAD": "Du hast {{count}} ungelesene Benachrichtigung{{count>0?'en':''}}"
    },
    "NOTIFICATIONS": {
      "TITLE": "Benachrichtigungen",
      "MARK_ALL_AS_READ": "Alle als gelesen markieren",
      "NO_RESULT": "Keine Benachrichtigungen",
      "SHOW_ALL": "Alle anzeigen",
      "LOAD_NOTIFICATIONS_FAILED": "Fehler beim Laden von Benachrichtigungen"
    },
    "REPORT_ABUSE": {
      "TITLE": "Problem melden",
      "SUB_TITLE": "Bitte kurz das Problem erläutern:",
      "REASON_HELP": "Ich erkläre das Problem...",
      "ASK_DELETE": "Löschung beantragen?",
      "CONFIRM": {
        "SENT": "Bericht gesendet. Danke!"
      }
    }
  },
  "MENU": {
    "REGISTRY": "Seiten",
    "USER_PROFILE": "Mein Profil",
    "MESSAGES": "Nachrichten",
    "NOTIFICATIONS": "Benachrichtigungen",
    "INVITATIONS": "Einladungen"
  },
  "ACCOUNT": {
    "NEW": {
      "ORGANIZATION_ACCOUNT": "Konto für eine Organisation",
      "ORGANIZATION_ACCOUNT_HELP": "Wenn Sie ein Unternehmen, einen Verein usw. vertreten, wird von diesem Konto keine Universelle Dividende gebildet."
    },
    "EVENT": {
      "MEMBER_WITHOUT_PROFILE": "Du kannst <a ui-sref=\"app.edit_profile\">dein Cäsium+-Profil ausfüllen</a> (optional), um dein Konto besser sichtbar zu machen. Dieses Profil wird in <b>einem unabhängigen, dezentralen Verzeichnis</b> der Währung gespeichert."
    },
    "ERROR": {
      "WS_CONNECTION_FAILED": "{{'COMMON.APP_NAME' | translate}} kann aufgrund eines technischen Fehlers (Verbindung zum Cesium+ Datenknoten) keine Benachrichtigungen empfangen.<br/><br/>Wenn das Problem weiterhin besteht, <b>wählen Sie bitte einen anderen Datenknoten</b> in den Cesium+ Einstellungen."
    }
  },
  "WOT": {
    "BTN_SUGGEST_CERTIFICATIONS_DOTS": "Zu zertifizierende Identitäten vorschlagen...",
    "BTN_ASK_CERTIFICATIONS_DOTS": "Mitglieder bitten, mich zu zertifizieren...",
    "BTN_ASK_CERTIFICATION": "Zertifizierung anfragen",
    "SUGGEST_CERTIFICATIONS_MODAL": {
      "TITLE": "Zertifizierungen vorschlagen",
      "HELP": "Wähle deine Vorschläge aus"
    },
    "ASK_CERTIFICATIONS_MODAL": {
      "TITLE": "Frage nach Zertifizierungen",
      "HELP": "Empfänger auswählen"
    },
    "SEARCH": {
      "DIVIDER_PROFILE": "Konten",
      "DIVIDER_PAGE": "Seiten",
      "DIVIDER_GROUP": "Gruppen"
    },
    "CONFIRM": {
      "SUGGEST_CERTIFICATIONS": "Möchten Sie wirklich <b>diese Zertifizierungsvorschläge einreichen</b>?",
      "ASK_CERTIFICATION": "Möchten Sie wirklich <b>eine Zertifizierungsanfrage senden</b>?",
      "ASK_CERTIFICATIONS": "Möchten Sie diesen Personen wirklich <b>eine Zertifizierungsanfrage senden</b>?"
    }
  },
  "INVITATION": {
    "TITLE": "Einladungen",
    "NO_RESULT": "Keine ausstehenden Einladungen",
    "BTN_DELETE_ALL": "Alle Einladungen löschen",
    "BTN_DELETE": "Einladung löschen",
    "BTN_NEW_INVITATION": "Neue Einladung",
    "ASK_CERTIFICATION": "<a href=\"#/app/wot/{{pubkey}}/{{::uid}}\">{{::name||uid}}</a> Zertifizierung anfragen",
    "SUGGESTION_CERTIFICATION": "<a href=\"#/app/wot/{{::pubkey}}/{{::uid}}\">{{::name||uid}}</a> wird dir zur Zertifizierung vorgeschlagen",
    "SUGGESTED_BY": "Vorschlag eingereicht von <a class=\"positive\" href=\"#/app/wot/{{::issuer.pubkey}}/{{::issuer.uid}}\">{{::issuer.name||issuer.uid}}</a>",
    "NOTIFICATIONS": {
      "TITLE": "Einladungen"
    },
    "LIST": {
      "TITLE": "Einladungen"
    },
    "NEW": {
      "TITLE": "Neue Einladung",
      "RECIPIENTS": "An",
      "RECIPIENTS_HELP": "Einladungsempfänger",
      "RECIPIENTS_MODAL_TITLE": "Empfänger",
      "RECIPIENTS_MODAL_HELP": "Bitte Empfänger auswählen:",
      "SUGGESTION_IDENTITIES": "Zertifizierungsvorschläge",
      "SUGGESTION_IDENTITIES_HELP": "Zertifizierungen zum Vorschlagen",
      "SUGGESTION_IDENTITIES_MODAL_TITLE": "Vorschläge",
      "SUGGESTION_IDENTITIES_MODAL_HELP": "Bitte wählen Sie Ihre Vorschläge aus:"
    },
    "CONFIRM": {
      "DELETE_ALL_CONFIRMATION": "Das Löschen von Einladungen ist ein <b>nicht umkehrbarer Vorgang</b>.<br/><br/><b>Bist du sicher</b>, dass du fortfahren möchtest?",
      "SEND_INVITATIONS_TO_CERTIFY": "Bist du sicher, dass du <b>diese Einladung zur Zertifizierung senden</b> möchtest?"
    },
    "INFO": {
      "INVITATION_SENT": "Einladung versendet"
    },
    "ERROR": {
      "LOAD_INVITATIONS_FAILED": "Einladungen konnten nicht geladen werden",
      "REMOVE_INVITATION_FAILED": "Fehler beim Löschen der Einladung",
      "REMOVE_ALL_INVITATIONS_FAILED": "Fehler beim Löschen von Einladungen",
      "SEND_INVITATION_FAILED": "Fehler beim Senden der Einladung",
      "BAD_INVITATION_FORMAT": "<span class=\"assertive\"><i class=\"ion-close-circled\"></i>Unlesbare Einladung (unbekanntes Format)</span> – gesendet von <a ui-sref=\"app.wot_identity({pubkey: '{{::pubkey}}', uid: '{{::uid}}' })\">{{::name||uid}}</a>"
    }
  },
  "COMMENTS": {
    "DIVIDER": "Kommentare",
    "SHOW_MORE_COMMENTS": "Vorherige Kommentare anzeigen",
    "COMMENT_HELP": "Dein Kommentar, deine Frage usw.",
    "COMMENT_HELP_REPLY_TO": "Deine Antwort...",
    "BTN_SEND": "Schicken",
    "POPOVER_SHARE_TITLE": "Nachricht #{{number}}",
    "REPLY": "Antworten",
    "REPLY_TO": "Antworten auf:",
    "REPLY_TO_LINK": "Als Antwort auf ",
    "REPLY_TO_DELETED_COMMENT": "Als Antwort auf einen gelöschten Kommentar",
    "REPLY_COUNT": "{{replyCount}} Antworten",
    "DELETED_COMMENT": "Kommentar gelöscht",
    "MODIFIED_ON": "geändert am {{time|formatDate}}",
    "MODIFIED_PARENTHESIS": "(später geändert)",
    "ERROR": {
      "FAILED_SAVE_COMMENT": "Fehler beim Speichern des Kommentars",
      "FAILED_REMOVE_COMMENT": "Fehler beim Löschen des Kommentars"
    }
  },
  "MESSAGE": {
    "REPLY_TITLE_PREFIX": "AW: ",
    "FORWARD_TITLE_PREFIX": "WG: ",
    "BTN_REPLY": "Antworten",
    "BTN_COMPOSE": "Neue Nachricht",
    "BTN_WRITE": "Schreiben",
    "NO_MESSAGE_INBOX": "Keine Nachricht erhalten",
    "NO_MESSAGE_OUTBOX": "Keine Nachricht gesendet",
    "NOTIFICATIONS": {
      "TITLE": "Nachrichten",
      "MESSAGE_RECEIVED": "Sie haben <b>eine Nachricht erhalten</b><br/>von"
    },
    "LIST": {
      "INBOX": "Posteingang",
      "OUTBOX": "Postausgang",
      "LAST_INBOX": "Neue Nachrichten",
      "LAST_OUTBOX": "Gesendete Nachrichten",
      "BTN_LAST_MESSAGES": "Kürzliche Nachrichten",
      "TITLE": "Nachrichten",
      "SEARCH_HELP": "In Nachrichten suchen",
      "POPOVER_ACTIONS": {
        "TITLE": "Optionen",
        "DELETE_ALL": "Alle Nachrichten löschen"
      }
    },
    "COMPOSE": {
      "TITLE": "Neue Nachricht",
      "TITLE_REPLY": "Antworten",
      "SUB_TITLE": "Neue Nachricht",
      "TO": "An",
      "OBJECT": "Betreff",
      "OBJECT_HELP": "Betreff",
      "ENCRYPTED_HELP": "Bitte beachte, dass diese Nachricht vor dem Versand verschlüsselt und signiert wird, sodass nur der Empfänger sie lesen kann und sichergestellt ist, dass du der Verfasser bist.",
      "MESSAGE": "Nachricht",
      "MESSAGE_HELP": "Nachrichteninhalt",
      "CONTENT_CONFIRMATION": "Der Inhalt der Nachricht ist leer.<br/><br/>Möchtest du die Nachricht trotzdem senden?"
    },
    "VIEW": {
      "TITLE": "Nachricht",
      "SENDER": "Gesendet von",
      "RECIPIENT": "Gesendet an",
      "NO_CONTENT": "Leere Nachricht",
      "DELETE": "Nachricht löschen"
    },
    "CONFIRM": {
      "REMOVE": "Möchtest du <b>diese Nachricht wirklich löschen</b>?<br/><br/>Dieser Vorgang kann nicht rückgängig gemacht werden.",
      "REMOVE_ALL": "Möchtest du wirklich <b>alle Nachrichten löschen</b>?<br/><br/>Dieser Vorgang kann nicht rückgängig gemacht werden.",
      "MARK_ALL_AS_READ": "Möchtest du wirklich <b>alle Nachrichten als gelesen markieren</b>?",
      "USER_HAS_NO_PROFILE": "Diese Identität hat kein Cesium+ Profil. Sie verwendet möglicherweise nicht die Cesium+ Erweiterung und wird daher <b>deine Nachricht nicht angezeigt bekommen</b>.<br/><br/>Bist du sicher, dass du trotzdem <b>fortfahren</b> möchtest?"
    },
    "INFO": {
      "MESSAGE_REMOVED": "Nachricht gelöscht",
      "All_MESSAGE_REMOVED": "Alle Nachrichten wurden gelöscht",
      "MESSAGE_SENT": "Nachricht gesendet"
    },
    "ERROR": {
      "SEND_MSG_FAILED": "Beim Senden der Nachricht ist ein Fehler aufgetreten.",
      "LOAD_MESSAGES_FAILED": "Fehler beim Abrufen von Nachrichten.",
      "LOAD_MESSAGE_FAILED": "Fehler beim Abrufen der Nachricht.",
      "MESSAGE_NOT_READABLE": "Nachricht kann nicht gelesen werden.",
      "USER_NOT_RECIPIENT": "Du bist nicht der Empfänger dieser Nachricht: Entschlüsselung unmöglich.",
      "NOT_AUTHENTICATED_MESSAGE": "Die Echtheit der Nachricht ist fraglich oder ihr Inhalt ist beschädigt.",
      "REMOVE_MESSAGE_FAILED": "Fehler beim Löschen der Nachricht",
      "MESSAGE_CONTENT_TOO_LONG": "Wert zu lang (max. {{maxLength}} Zeichen).",
      "MARK_AS_READ_FAILED": "Nachricht kann nicht als gelesen markiert werden.",
      "LOAD_NOTIFICATIONS_FAILED": "Fehler beim Abrufen von Nachrichtenbenachrichtigungen.",
      "REMOVE_All_MESSAGES_FAILED": "Fehler beim Löschen aller Nachrichten.",
      "MARK_ALL_AS_READ_FAILED": "Fehler beim Markieren von Nachrichten als gelesen.",
      "RECIPIENT_IS_MANDATORY": "Empfänger ist obligatorisch."
    }
  },
  "BLOCKCHAIN": {
    "LOOKUP": {
      "SEARCH_HELP": "Nummer des Blocks, Hash, öffentlicher Schlüssel usw.",
      "POPOVER_FILTER_TITLE": "Filter",
      "HEADER_MEDIAN_TIME": "Datum / Uhrzeit",
      "HEADER_BLOCK": "Block Nr.",
      "HEADER_ISSUER": "Berechnender Knoten",
      "BTN_LAST": "Letzte Blöcke",
      "BTN_TX": "Transaktionen",
      "DISPLAY_QUERY": "Abfrage anzeigen",
      "HIDE_QUERY": "Abfrage ausblenden",
      "TX_SEARCH_FILTER": {
        "MEMBER_FLOWS": "<b class=\"ion-person\"></b> Eintritte/Austritte von Mitgliedern",
        "EXISTING_TRANSACTION": "<b class=\"ion-card\"></b> Mit Transaktionen",
        "PERIOD": "<b class=\"ion-clock\"></b> Zwischen <b class=\"gray\">{{params[1]|medianDateShort}}</b> ({{params[1]|medianTime}}) und <b class=\"gray\">{{params[2]|medianDateShort}}</b> ({{params[2]|medianTime}})",
        "ISSUER": "<b class=\"ion-android-desktop\"></b> Berechnet von <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}",
        "TX_PUBKEY": "<b class=\"ion-card\"></b> Transaktionen bezüglich <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}"
      }
    },
    "ERROR": {
      "SEARCH_BLOCKS_FAILED": "Fehler beim Suchen von Blöcken."
    }
  },
  "GROUP": {
    "GENERAL_DIVIDER": "Allgemeine Informationen",
    "LOCATION_DIVIDER": "Adresse",
    "SOCIAL_NETWORKS_DIVIDER": "Soziale Netzwerke und Webseiten",
    "TECHNICAL_DIVIDER": "Technische Informationen",
    "CREATED_TIME": "{{creationTime|formatFromNow}} erstellt",
    "NOTIFICATIONS": {
      "TITLE": "Einladungen"
    },
    "LOOKUP": {
      "TITLE": "Gruppen",
      "SEARCH_HELP": "Gruppenname, Wörter, Ort usw.",
      "LAST_RESULTS_LIST": "Neue Gruppen:",
      "OPEN_RESULTS_LIST": "Offene Gruppen:",
      "MANAGED_RESULTS_LIST": "Geschlossene Gruppen:",
      "BTN_LAST": "Neue Gruppen",
      "BTN_NEW": "Gruppe hinzufügen"
    },
    "TYPE": {
      "TITLE": "Neue Gruppe",
      "SELECT_TYPE": "Gruppentyp:",
      "OPEN_GROUP": "Offene Gruppe",
      "OPEN_GROUP_HELP": "Eine offene Gruppe ist für jedes Mitglied der Währung zugänglich.",
      "MANAGED_GROUP": "Administrierte Gruppe",
      "MANAGED_GROUP_HELP": "Eine verwaltete Gruppe wird von Administratoren und Moderatoren verwaltet, die ein Mitglied darin akzeptieren, ablehnen oder ausschließen können.",
      "ENUM": {
        "OPEN": "Offene Gruppe",
        "MANAGED": "Administrierte Gruppe"
      }
    },
    "VIEW": {
      "POPOVER_SHARE_TITLE": "{{title}}",
      "MENU_TITLE": "Optionen",
      "REMOVE_CONFIRMATION": "Möchten Sie diese Gruppe wirklich löschen?<br/><br/>Dieser Vorgang kann nicht rückgängig gemacht werden."
    },
    "EDIT": {
      "TITLE": "Gruppe",
      "TITLE_NEW": "Neue Gruppe",
      "RECORD_TITLE": "Titel",
      "RECORD_TITLE_HELP": "Titel",
      "RECORD_DESCRIPTION": "Beschreibung",
      "RECORD_DESCRIPTION_HELP": "Beschreibung"
    },
    "ERROR": {
      "SEARCH_GROUPS_FAILED": "Fehler beim Suchen von Gruppen",
      "REMOVE_RECORD_FAILED": "Fehler beim Löschen der Gruppe"
    },
    "INFO": {
      "RECORD_REMOVED": "Gruppe gelöscht"
    }
  },
  "REGISTRY": {
    "CATEGORY": "Hauptaktivität",
    "GENERAL_DIVIDER": "Allgemeine Informationen",
    "LOCATION_DIVIDER": "Adresse",
    "SOCIAL_NETWORKS_DIVIDER": "Soziale Netzwerke und Webseite",
    "TECHNICAL_DIVIDER": "Technische Informationen",
    "BTN_SHOW_WOT": "Leute",
    "BTN_SHOW_WOT_HELP": "Leute suchen",
    "BTN_SHOW_PAGES": "Seiten",
    "BTN_SHOW_PAGES_HELP": "Seiten suchen",
    "BTN_NEW": "Neue Seite",
    "MY_PAGES": "Meine Seiten",
    "NO_PAGE": "Keine Seiten",
    "SEARCH": {
      "TITLE": "Seiten",
      "SEARCH_HELP": "Was, Wer: Restaurant, Zum Eck, ...",
      "BTN_ADD": "Neu",
      "BTN_LAST_RECORDS": "Neueste Seiten",
      "BTN_ADVANCED_SEARCH": "Erweiterte Suche",
      "BTN_OPTIONS": "Erweiterte Suche",
      "TYPE": "Seitentyp",
      "LOCATION_HELP": "Wo: PLZ, Ort",
      "RESULTS": "Ergebnisse",
      "RESULT_COUNT_LOCATION": "{{count}} Ergebnis{{count>0?'se':''}}, in der Nähe von {{location}}",
      "RESULT_COUNT": "{{count}} Ergebnis{{count>0?'se':''}}",
      "LAST_RECORDS": "Neueste Seiten",
      "LAST_RECORD_COUNT_LOCATION": "{{count}} neueste Seite{{count>0?'n':''}} in der Nähe von {{location}}",
      "LAST_RECORD_COUNT": "{{count}} neueste Seite{{count>0?'n':''}}",
      "POPOVER_FILTERS": {
        "BTN_ADVANCED_SEARCH": "Erweiterte Optionen?"
      }
    },
    "VIEW": {
      "TITLE": "Verzeichnis",
      "CATEGORY": "Hauptaktivität:",
      "LOCATION": "Adresse:",
      "MENU_TITLE": "Optionen",
      "POPOVER_SHARE_TITLE": "{{title}}",
      "REMOVE_CONFIRMATION": "Möchten Sie diese Seite wirklich löschen?<br/><br/>Dieser Vorgang kann nicht rückgängig gemacht werden."
    },
    "TYPE": {
      "TITLE": "Neue Seite",
      "SELECT_TYPE": "Seitentyp:",
      "ENUM": {
        "SHOP": "Lokale Geschäfte",
        "COMPANY": "Unternehmen",
        "ASSOCIATION": "Verband",
        "INSTITUTION": "Institution"
      }
    },
    "EDIT": {
      "TITLE": "Bearbeiten",
      "TITLE_NEW": "Neue Seite",
      "RECORD_TYPE": "Seitentyp",
      "RECORD_TITLE": "Name",
      "RECORD_TITLE_HELP": "Name",
      "RECORD_DESCRIPTION": "Beschreibung",
      "RECORD_DESCRIPTION_HELP": "Beschreibung der Aktivität",
      "RECORD_ADDRESS": "Straße",
      "RECORD_ADDRESS_HELP": "Straße, Gebäude...",
      "RECORD_CITY": "Stadt",
      "RECORD_CITY_HELP": "Stadt, Land",
      "RECORD_SOCIAL_NETWORKS": "Soziale Netzwerke und Webseite",
      "RECORD_PUBKEY": "Öffentlicher Schlüssel",
      "RECORD_PUBKEY_HELP": "Öffentlicher Schlüssel zum Empfangen von Zahlungen"
    },
    "WALLET": {
      "PAGE_DIVIDER": "Seiten",
      "PAGE_DIVIDER_HELP": "Seiten beziehen sich auf Aktivitäten, die die Währung akzeptieren oder fördern: Lokale Geschäfte, Unternehmen, Verbände, Institutionen. Sie werden außerhalb des Währungsnetzwerks im <a ui-sref=\"app.es_network\">Netzwerk der Cäsium+ Knoten</a> gespeichert."
    },
    "ERROR": {
      "LOAD_CATEGORY_FAILED": "Fehler beim Laden der Aktivitätsliste",
      "LOAD_RECORD_FAILED": "Fehler beim Laden der Seite",
      "LOOKUP_RECORDS_FAILED": "Fehler beim Ausführen der Suche",
      "REMOVE_RECORD_FAILED": "Fehler beim Löschen der Seite",
      "SAVE_RECORD_FAILED": "Fehler beim Speichern",
      "RECORD_NOT_EXISTS": "Seite existiert nicht",
      "GEO_LOCATION_NOT_FOUND": "Stadt oder Postleitzahl nicht gefunden"
    },
    "INFO": {
      "RECORD_REMOVED": "Seite gelöscht",
      "RECORD_SAVED": "Seite gespeichert"
    }
  },
  "PROFILE": {
    "PROFILE_DIVIDER": "Cesium+ Profil",
    "PROFILE_DIVIDER_HELP": "Dies sind zusätzliche, optionale Daten. Sie werden außerhalb des Währungsnetzwerks im <a ui-sref=\"app.es_network\">Cesium+ Netzwerk</a> gespeichert.",
    "NO_PROFILE_DEFINED": "Kein Cesium+ Profil vorhanden",
    "BTN_ADD": "Mein Profil erstellen",
    "BTN_EDIT": "Mein Profil bearbeiten",
    "BTN_DELETE": "Mein Profil löschen",
    "BTN_REORDER": "Neu anordnen",
    "UID": "Pseudonym",
    "TITLE": "Nachname, Vorname",
    "TITLE_HELP": "Nachname, Vorname",
    "DESCRIPTION": "Über mich",
    "DESCRIPTION_HELP": "Über mich...",
    "SOCIAL_HELP": "http://...",
    "GENERAL_DIVIDER": "Allgemeine Informationen",
    "SOCIAL_NETWORKS_DIVIDER": "Soziale Netzwerke, Webseiten",
    "TECHNICAL_DIVIDER": "Technische Informationen",
    "MODAL_AVATAR": {
      "TITLE": "Profilbild",
      "SELECT_FILE_HELP": "Bitte <b>Bilddatei auswählen</b>:",
      "BTN_SELECT_FILE": "Wähle ein Bild",
      "RESIZE_HELP": "<b>Bild zuschneiden</b>, falls erforderlich. Ein Klick auf das Bild ermöglicht es, es zu verschieben. Klicken Sie auf den unteren linken Bereich, um zu zoomen.",
      "RESULT_HELP": "<b>Hier ist das Ergebnis</b>, wie es in deinem Profil zu sehen ist:"
    },
    "CONFIRM": {
      "DELETE": "Möchtest du wirklich <b>dein Cesium+ Profil löschen?</b><br/><br/>Dieser Vorgang kann nicht rückgängig gemacht werden.",
      "DELETE_BY_MODERATOR": "Möchtest du <b>dieses Cesium+ Profil wirklich löschen?</b><br/><br/>Dieser Vorgang kann nicht rückgängig gemacht werden."
    },
    "ERROR": {
      "REMOVE_PROFILE_FAILED": "Fehler beim Löschen des Profils",
      "LOAD_PROFILE_FAILED": "Fehler beim Laden des Profils",
      "SAVE_PROFILE_FAILED": "Fehler beim Speichern des Profils",
      "DELETE_PROFILE_FAILED": "Fehler beim Löschen des Profils",
      "INVALID_SOCIAL_NETWORK_FORMAT": "Nicht berücksichtigtes Format: Bitte geben Sie eine gültige Adresse ein.<br/><br/>Beispiele:<ul><li>- Eine Facebook-Seite (https://www.facebook.com/user)</li><li>- Eine Webseite (http://www.monsite.fr)</li><li>- Eine E-Mail-Adresse (joe@dalton.com)</li></ul>",
      "IMAGE_RESIZE_FAILED": "Fehler beim Ändern der Bildgröße"
    },
    "INFO": {
      "PROFILE_REMOVED": "Profil gelöscht",
      "PROFILE_SAVED": "Profil gespeichert"
    },
    "HELP": {
      "WARNING_PUBLIC_DATA": "Die in Ihrem Profil eingegebenen Informationen sind <b>öffentlich</b>. Sie sind sogar für Personen sichtbar, die <b>nicht verbunden sind</b>.<br/>{{'PROFILE.PROFILE_DIVIDER_HELP'|translate}}"
    }
  },
  "LOCATION": {
    "BTN_GEOLOC_ADDRESS": "Meine Adresse auf der Karte finden",
    "USE_GEO_POINT": "Auf {{'COMMON.APP_NAME'|translate}}-Karten erscheinen?",
    "LOADING_LOCATION": "Suche nach der Adresse...",
    "LOCATION_DIVIDER": "Adresse",
    "ADDRESS": "Straße, Hausnummer",
    "ADDRESS_HELP": "Straße, zusätzliche Adresse... (optional)",
    "CITY": "Ort",
    "CITY_HELP": "PLZ, Ort, Land",
    "DISTANCE": "Maximale Entfernung um die Stadt",
    "DISTANCE_UNIT": "km",
    "DISTANCE_OPTION": "{{value}} {{'LOCATION.DISTANCE_UNIT'|translate}}",
    "SEARCH_HELP": "PLZ, Ort",
    "PROFILE_POSITION": "Profilposition",
    "MODAL": {
      "TITLE": "Adresssuche",
      "SEARCH_HELP": "Ort, Postleitzahl, Land",
      "ALTERNATIVE_RESULT_DIVIDER": "Alternative Ergebnisse für <b>{{address}}</b>:",
      "POSITION": "Breite/Länge: {{lat}}/{{lon}}"
    },
    "ERROR": {
      "CITY_REQUIRED_IF_STREET": "Pflichtfeld (weil Straße eingetragen ist)",
      "REQUIRED_FOR_LOCATION": "Pflichtfeld um auf der Karte zu erscheinen",
      "INVALID_FOR_LOCATION": "Adresse unbekannt",
      "GEO_LOCATION_FAILED": "Ihr Standort kann nicht abgerufen werden. Bitte benutzen Sie die Suchfunktion.",
      "ADDRESS_LOCATION_FAILED": "Die Position kann nicht von der Adresse abgeleitet werden"
    }
  },
  "SUBSCRIPTION": {
    "SUBSCRIPTION_DIVIDER": "Online-Dienste",
    "SUBSCRIPTION_DIVIDER_HELP": "Die Online-Dienste bieten zusätzliche und optionale Dienste, die an einen Dienstanbieter Ihrer Wahl delegiert werden. Zum Beispiel, um Zahlungsbenachrichtigungen per E-Mail zu erhalten.",
    "BTN_ADD": "Dienst hinzufügen",
    "BTN_EDIT": "Meine Dienste Verwalten",
    "NO_SUBSCRIPTION": "Kein Dienst verwendet",
    "SUBSCRIPTION_COUNT": "Dienste / Abonnements",
    "EDIT": {
      "TITLE": "Online-Dienste",
      "HELP_TEXT": "Verwalte hier deine Abonnements und anderen Online-Dienste",
      "PROVIDER": "Dienstanbieter:"
    },
    "TYPE": {
      "ENUM": {
        "EMAIL": "Erhalten Sie Benachrichtigungen per E-Mail"
      }
    },
    "CONFIRM": {
      "DELETE_SUBSCRIPTION": "Möchten Sie wirklich <b>dieses Abonnement löschen</b>?"
    },
    "ERROR": {
      "LOAD_SUBSCRIPTIONS_FAILED": "Fehler beim Laden von Online-Diensten",
      "ADD_SUBSCRIPTION_FAILED": "Fehler beim Senden des Abonnements",
      "UPDATE_SUBSCRIPTION_FAILED": "Fehler bei der Aktualisierung des Abonnements",
      "DELETE_SUBSCRIPTION_FAILED": "Fehler beim Löschen des Abonnements"
    },
    "MODAL_EMAIL": {
      "TITLE": "Benachrichtigung per E-Mail",
      "HELP": "Fülle dieses Formular aus, um über Ereignisse in deinem Konto <b>per E-Mail benachrichtigt zu werden</b>.<br/>Deine E-Mail-Adresse wird verschlüsselt, damit sie nur für den Dienstanbieter sichtbar ist.",
      "EMAIL_LABEL": "Deine E-Mail-Adresse:",
      "EMAIL_HELP": "max.mustermann@example.com",
      "FREQUENCY_LABEL": "Häufigkeit der Benachrichtigungen:",
      "FREQUENCY_DAILY": "Täglich",
      "FREQUENCY_WEEKLY": "Wöchentlich",
      "PROVIDER": "Dienstanbieter:"
    }
  },
  "DOCUMENT": {
    "HASH": "Hash:",
    "LOOKUP": {
      "TITLE": "Dokumentensuche",
      "BTN_ACTIONS": "Aktionen",
      "SEARCH_HELP": "issuer:AAA*, time:1508406169",
      "LAST_DOCUMENTS_DOTS": "Neueste Dokumente:",
      "LAST_DOCUMENTS": "Neueste Dokumente",
      "SHOW_QUERY": "Anfrage anzeigen",
      "HIDE_QUERY": "Anfrage ausblenden",
      "HEADER_TIME": "Datum/Uhrzeit",
      "HEADER_ISSUER": "Sender",
      "HEADER_RECIPIENT": "Empfänger",
      "HEADER_AMOUNT": "Anzahl",
      "READ": "Gelesen",
      "BTN_REMOVE": "Dieses Dokument löschen",
      "BTN_COMPACT": "Komprimieren",
      "HAS_CREATE_OR_UPDATE_PROFILE": "Profil erstellt oder aktualisiert",
      "POPOVER_ACTIONS": {
        "TITLE": "Aktionen",
        "REMOVE_ALL": "Diese Dokumente löschen..."
      }
    },
    "INFO": {
      "REMOVED": "Dokument gelöscht"
    },
    "CONFIRM": {
      "REMOVE": "Möchten Sie wirklich <b>dieses Dokument löschen</b>?",
      "REMOVE_ALL": "Möchten Sie wirklich <b>diese Dokumente löschen</b>?"
    },
    "ERROR": {
      "LOAD_DOCUMENTS_FAILED": "Fehler beim Suchen nach Dokumenten",
      "REMOVE_FAILED": "Fehler beim Löschen des Dokuments",
      "REMOVE_ALL_FAILED": "Fehler beim Löschen von Dokumenten"
    }
  },
  "ES_SETTINGS": {
    "PLUGIN_NAME": "Cesium+",
    "PLUGIN_NAME_HELP": "Benutzerprofile, Benachrichtigungen, private Nachrichten",
    "ENABLE_TOGGLE": "Plugin aktivieren?",
    "ENABLE_REMOTE_STORAGE": "Remotespeicherung aktivieren?",
    "ENABLE_REMOTE_STORAGE_HELP": "Ermöglicht (verschlüsseltes) Speichern Ihrer Einstellungen auf den Cesium+ Knoten",
    "ENABLE_MESSAGE_TOGGLE": "Private Nachrichten aktivieren?",
    "PEER": "Cesium+ Datenknoten",
    "POPUP_PEER": {
      "TITLE": "Datenknoten",
      "HELP": "Geben Sie die Adresse des Knotens ein, den Sie verwenden möchten:",
      "PEER_HELP": "server.domain.com:port"
    },
    "NOTIFICATIONS": {
      "DIVIDER": "Benachrichtigungen",
      "HELP_TEXT": "Aktivieren Sie die Arten von Benachrichtigungen, die Sie erhalten möchten:",
      "ENABLE_TX_SENT": "Über <b>ausgehende Zahlungen</b> benachrichtigen?",
      "ENABLE_TX_RECEIVED": "Über <b>erhaltene Zahlungen</b> benachrichtigen?",
      "ENABLE_CERT_SENT": "Über <b>ausgestellte Zertifizierungen</b> benachrichtigen?",
      "ENABLE_CERT_RECEIVED": "Über <b>erhaltene Zertifikate</b> benachrichtigen?",
      "ENABLE_HTML5_NOTIFICATION": "Bei jeder neuen Benachrichtigung warnen?",
      "ENABLE_HTML5_NOTIFICATION_HELP": "Öffnet bei jeder neuen Benachrichtigung ein kleines Fenster."
    },
    "CONFIRM": {
      "ASK_ENABLE_TITLE": "Optionale Funktionen",
      "ASK_ENABLE": "Die Cesium+ Erweiterung ist in Ihren Einstellungen <b>deaktiviert</b>, wodurch diese Funktionen deaktiviert werden: <ul><li><b><i class=\"icon ion-person\"></i>Cesium+ Profile</b>;</li><li><b><i class=\"icon ion-android-notifications\"></i>Benachrichtigungen</b>;</li><li><b><i class=\"icon ion-email\"></i> Private Nachrichten</b>.</li><li><b><i class=\"icon ion-location\"></i>Karten etc.</b>.</li></ul><br/><b >Möchtest du die Erweiterung wieder aktivieren</b>?"
    }
  },
  "ES_WALLET": {
    "ERROR": {
      "RECIPIENT_IS_MANDATORY": "Für die Verschlüsselung wird ein Empfänger benötigt."
    }
  },
  "ES_PEER": {
    "NAME": "Name",
    "DOCUMENTS": "Dokumente",
    "SOFTWARE": "Software",
    "DOCUMENT_COUNT": "Anzahl der Dokumente",
    "EMAIL_SUBSCRIPTION_COUNT": "{{emailSubscription}} Abonnent{{emailSubscription ? 'en': ''}} für E-Mail-Benachrichtigungen"
  },
  "EVENT": {
    "NODE_STARTED": "Ihr ES-API-Knoten <b>{{params[0]}}</b> wird gestartet",
    "NODE_BMA_DOWN": "Der Knoten <b>{{params[0]}}:{{params[1]}}</b> (verwendet von Ihrem ES-API-Knoten) ist <b>nicht erreichbar</b>.",
    "NODE_BMA_UP": "Der Knoten <b>{{params[0]}}:{{params[1]}}</b> ist wieder erreichbar.",
    "MEMBER_JOIN": "Sie sind jetzt ein <b>Mitglied</b> der Währung <b>{{params[0]}}</b>!",
    "MEMBER_LEAVE": "Sie sind <b>kein Mitglied</b> der Währung <b>{{params[0]}}</b> mehr!",
    "MEMBER_EXCLUDE": "Sie sind <b>kein Mitglied</b> der Währung <b>{{params[0]}}</b> mehr, mangels Nichterneuerung oder fehlender Zertifizierungen.",
    "MEMBER_REVOKE": "Ihr Konto wurde widerrufen. Es kann kein Mitgliedskonto der Währung <b>{{params[0]}}</b> mehr sein.",
    "MEMBER_ACTIVE": "Ihre <b>{{params[0]}}</b> Verlängerung der Währungsmitgliedschaft wurde <b>bestätigt</b>.",
    "TX_SENT": "Ihre <b>Zahlung</b> an <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>{{name||uid||params[1]}}</span> wurde abgeschlossen.",
    "TX_SENT_MULTI": "Ihre <b>Zahlung</b> an <b>{{params[1]}}</b> wurde abgeschlossen.",
    "TX_RECEIVED": "Sie haben eine <b>Zahlung erhalten</b>, von: <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>{{name||uid||params[1]}}</span>.",
    "TX_RECEIVED_MULTI": "Sie haben <b>eine Zahlung erhalten</b>, von: <b>{{params[1]}}</b>.",
    "CERT_SENT": "Ihre <b>Zertifizierung</b> unter <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>{{name||uid||params[1]}}</span> wurde abgeschlossen.",
    "CERT_RECEIVED": "Sie haben <b>eine Zertifizierung erhalten</b> von <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>{{name||uid||params[1]}}</span>.",
    "USER": {
      "LIKE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> mag dein Profil",
      "FOLLOW_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> verfolgt Ihre Aktivitäten",
      "STAR_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> hat dich bewertet ({{params[3]}} <b class=\"ion-star\">)",
      "MODERATION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> fordert Moderation für das Profil an: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "DELETION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> fordert Profillöschung an: <b>{{params[2]}}</b>",
      "ABUSE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> hat dein Profil gemeldet"
    },
    "PAGE": {
      "NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> hat deine Seite kommentiert: <b>{{params[2]}}</b>",
      "UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> hat den Kommentar auf deiner Seite bearbeitet: <b>{{params[2]}}</b>",
      "NEW_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> hat auf deinen Kommentar auf der Seite geantwortet: <b>{{params[2]}}</b>",
      "UPDATE_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> hat die Antwort auf Ihren Kommentar auf der Seite bearbeitet: <b>{{params[2]}}</b>",
      "FOLLOW_NEW_COMMENT": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> hat die Seite kommentiert: <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE_COMMENT": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> hat seinen Kommentar auf der Seite bearbeitet: <b>{{params[2]}}</b>",
      "FOLLOW_NEW": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> hat die Seite hinzugefügt: <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> hat die Seite geändert: <b>{{params[2]}}</b>",
      "MODERATION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> bittet dich um Moderation auf der Seite: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "DELETION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> hat eine Seite zum Entfernen gemeldet: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "ABUSE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> hat deine Seite gemeldet: <b>{{params[2]}}</b>"
    }
  },
  "LIKE": {
    "ERROR": {
      "FAILED_TOGGLE_LIKE": "Diese Aktion kann nicht ausgeführt werden."
    }
  },
  "CONFIRM": {
    "ES_USE_FALLBACK_NODE": "Datenknoten <b>{{old}}</b> nicht erreichbar oder ungültige Adresse.<br/><br/>Möchtest du Datenknoten <b>{{new}}</b> vorübergehend verwenden?"
  },
  "ERROR": {
    "ES_CONNECTION_ERROR": "Datenknoten <b>{{server}}</b> nicht erreichbar oder ungültige Adresse.<br/><br/>Cesium wird weiterhin betrieben, <b>ohne die Erweiterung Cesium+</b> (Benutzerprofile, private Nachrichten, Karten und Grafiken)<br/><br/>Überprüfe deine Internetverbindung oder ändere den Datenknoten in den <a class=\"positive\" ng-click=\"doQuickFix('settings')\">Einstellungen der Erweiterung</a>.",
    "ES_MAX_UPLOAD_BODY_SIZE": "Die zu sendende Datenmenge überschreitet das vom Server festgelegte Limit.<br/>Bitte versuche es erneut, nachdem du z.B. Fotos gelöscht hast."
  },
  "ADMIN": {
    "MESSAGE": {
      "BTN_SEND_EMAIL": "Gruppen-E-Mail senden"
    },
    "LOG": {
      "BTN_SHOW_LOG": "Protokolle blockierter IP-Adressen",
      "VIEW": {
        "TITLE": "Protokolle",
        "LOG_REQUEST_DIVIDER": "Liste der blockierten Anfragen"
      }
    },
    "ERROR": {
      "NO_SSL_ACCESS": "Zugriff aufgrund von TLS-Einschränkungen nicht möglich.",
      "LOAD_LOG_FAILED": "Fehler beim Lesen der Protokolle.",
      "AUTH_ERROR": "Fehler bei der POD-Authentifizierung"
    }
  }
}
);

$translateProvider.translations("en-GB", {
  "COMMON": {
    "CATEGORY": "Category",
    "CATEGORIES": "Categories",
    "CATEGORY_SEARCH_HELP": "Search",
    "COMMENT_HELP": "Comments",
    "LAST_MODIFICATION_DATE": "Updated on ",
    "SUBMIT_BY": "Submitted by",
    "BTN_LIKE": "I like",
    "BTN_LIKE_REMOVE": "I don't like anymore",
    "LIKES_TEXT": "{{total}} {{total > 1 ? 'people' : 'person'}} liked this page",
    "ABUSES_TEXT": "{{total}} {{total > 1 ? 'people' : 'person'}} reported a problem on this page",
    "BTN_REPORT_ABUSE_DOTS": "Report a problem or an abuse...",
    "BTN_REMOVE_REPORTED_ABUSE": "Cancel my problem report",
    "BTN_PUBLISH": "Publish",
    "BTN_PICTURE_DELETE": "Delete",
    "BTN_PICTURE_FAVORISE": "Default",
    "BTN_PICTURE_ROTATE": "Rotate",
    "BTN_ADD_PICTURE": "Add picture",
    "NOTIFICATION": {
      "TITLE": "New notification | {{'COMMON.APP_NAME'|translate}}",
      "HAS_UNREAD": "You have {{count}} unread notification{{count>0?'s':''}}"
    },
    "NOTIFICATIONS": {
      "TITLE": "Notifications",
      "MARK_ALL_AS_READ": "Mark all as read",
      "NO_RESULT": "No notification",
      "SHOW_ALL": "Show all",
      "LOAD_NOTIFICATIONS_FAILED": "Could not load notifications"
    },
    "REPORT_ABUSE": {
      "TITLE": "Report a problem",
      "SUB_TITLE": "Please explain briefly the problem:",
      "REASON_HELP": "I explain the problem...",
      "ASK_DELETE": "Request removal?",
      "CONFIRM": {
        "SENT": "Request sent. Thnak you!"
      }
    }
  },
  "MENU": {
    "REGISTRY": "Pages",
    "USER_PROFILE": "My Profile",
    "MESSAGES": "Messages",
    "NOTIFICATIONS": "Notifications",
    "INVITATIONS": "Invitations"
  },
  "ACCOUNT": {
    "NEW": {
      "ORGANIZATION_ACCOUNT": "Account for an organization",
      "ORGANIZATION_ACCOUNT_HELP": "If you represent a company, association, etc.<br/>No universal dividend will be created by this account."
    },
    "EVENT": {
      "MEMBER_WITHOUT_PROFILE": "You can <a ui-sref=\"app.edit_profile\">fill your Cesium+ profile</a> (optional) to provide better visibility of your account. This profile will be stored in <b>a directory independent</b> of the currency, but decentralized."
    },
    "ERROR": {
      "WS_CONNECTION_FAILED": "Cesium can not receive notifications because of a technical error (connection to the Cesium + data node).<br/><br/>If the problem persists, please <b>choose another data node</b> in Cesium+ settings."
    }
  },
  "WOT": {
    "BTN_SUGGEST_CERTIFICATIONS_DOTS": "Suggest identities to certify...",
    "BTN_ASK_CERTIFICATIONS_DOTS": "Ask members to certify me...",
    "BTN_ASK_CERTIFICATION": "Ask a certification",
    "SUGGEST_CERTIFICATIONS_MODAL": {
      "TITLE": "Suggest certifications",
      "HELP": "Select your suggestions"
    },
    "ASK_CERTIFICATIONS_MODAL": {
      "TITLE": "Ask certifications",
      "HELP": "Select recipients"
    },
    "SEARCH": {
      "DIVIDER_PROFILE": "Accounts",
      "DIVIDER_PAGE": "Pages",
      "DIVIDER_GROUP": "Groups"
    },
    "CONFIRM": {
      "SUGGEST_CERTIFICATIONS": "Are you sure you want <b>to send these certification suggestions</b>?",
      "ASK_CERTIFICATION": "Are you sure you want to <b>send a certification request</b>?",
      "ASK_CERTIFICATIONS": "Are you sure you want to <b>send a certification request</b> to these people?"
    }
  },
  "INVITATION": {
    "TITLE": "Invitations",
    "NO_RESULT": "No invitation received",
    "BTN_DELETE_ALL": "Delete all invitations",
    "BTN_DELETE": "Delete invitation",
    "BTN_NEW_INVITATION": "New invitation",
    "ASK_CERTIFICATION": "<a href=\"#/app/wot/{{pubkey}}/{{::uid}}\">{{::name||uid}}</a> asks for your certification",
    "SUGGESTION_CERTIFICATION": "<a href=\"#/app/wot/{{::pubkey}}/{{::uid}}\">{{::name||uid}}</a> is suggested for certification",
    "SUGGESTED_BY": "Suggestion sent by <a class=\"positive\" href=\"#/app/wot/{{::issuer.pubkey}}/{{::issuer.uid}}\">{{::issuer.name||issuer.uid}}</a>",
    "NOTIFICATIONS": {
      "TITLE": "Invitations"
    },
    "LIST": {
      "TITLE": "Invitations"
    },
    "NEW": {
      "TITLE": "New invitation",
      "RECIPIENTS": "A",
      "RECIPIENTS_HELP": "Recipients of the invitation",
      "RECIPIENTS_MODAL_TITLE": "Recipients",
      "RECIPIENTS_MODAL_HELP": "Please choose recipients:",
      "SUGGESTION_IDENTITIES": "Suggestions for certification",
      "SUGGESTION_IDENTITIES_HELP": "Certifications to suggest",
      "SUGGESTION_IDENTITIES_MODAL_TITLE": "Suggestions",
      "SUGGESTION_IDENTITIES_MODAL_HELP": "Please choose your suggestions:"
    },
    "CONFIRM": {
      "DELETE_ALL_CONFIRMATION": "Removing invitations is <b>an irreversible operation</b>.<br/><br/><b>Are you sure</b> you want to continue",
      "SEND_INVITATIONS_TO_CERTIFY": "<b>Are you sure</b> you want <b>to sent this invitation to certify</b> ?"
    },
    "INFO": {
      "INVITATION_SENT": "Invitation sent"
    },
    "ERROR": {
      "LOAD_INVITATIONS_FAILED": "Error while loading invitations",
      "REMOVE_INVITATION_FAILED": "Error while deleting the invitation",
      "REMOVE_ALL_INVITATIONS_FAILED": "Error while deleting invitations",
      "SEND_INVITATION_FAILED": "Error while sending invitation",
      "BAD_INVITATION_FORMAT": "<span class=\"assertive\"><i class=\"ion-close-circled\"></i> Invitation unreadable (format unknown)</span> - sent by <a ui-sref=\"app.wot_identity({pubkey: '{{::pubkey}}', uid: '{{::uid}}' })\">{{::name||uid}}</a>"
    }
  },
  "COMMENTS": {
    "DIVIDER": "Comments",
    "SHOW_MORE_COMMENTS": "Show previous comments",
    "COMMENT_HELP": "Your comment, question...",
    "COMMENT_HELP_REPLY_TO": "Your answer...",
    "BTN_SEND": "Send",
    "POPOVER_SHARE_TITLE": "Message #{{number}}",
    "REPLY": "Reply",
    "REPLY_TO": "Respond to:",
    "REPLY_TO_LINK": "In response to ",
    "REPLY_TO_DELETED_COMMENT": "In response to a deleted comment",
    "REPLY_COUNT": "{{replyCount}} responses",
    "DELETED_COMMENT": "Comment deleted",
    "MODIFIED_ON": "modified on {{time|formatDate}}",
    "MODIFIED_PARENTHESIS": "(modified then)",
    "ERROR": {
      "FAILED_SAVE_COMMENT": "Saving comment failed",
      "FAILED_REMOVE_COMMENT": "Deleting comment failed"
    }
  },
  "MESSAGE": {
    "REPLY_TITLE_PREFIX": "Re: ",
    "FORWARD_TITLE_PREFIX": "Fw: ",
    "BTN_REPLY": "Reply",
    "BTN_COMPOSE": "New message",
    "BTN_WRITE": "Write",
    "NO_MESSAGE_INBOX": "No message received",
    "NO_MESSAGE_OUTBOX": "No message sent",
    "NOTIFICATIONS": {
      "TITLE": "Messages",
      "MESSAGE_RECEIVED": "You <b>received a message</b><br/>from"
    },
    "LIST": {
      "INBOX": "Inbox",
      "OUTBOX": "Outbox",
      "LAST_INBOX": "New messages",
      "LAST_OUTBOX": "Sent messages",
      "BTN_LAST_MESSAGES": "Recent messages",
      "TITLE": "Private messages",
      "SEARCH_HELP": "Search in messages",
      "POPOVER_ACTIONS": {
        "TITLE": "Options",
        "DELETE_ALL": "Delete all messages"
      }
    },
    "COMPOSE": {
      "TITLE": "New message",
      "TITLE_REPLY": "Reply",
      "SUB_TITLE": "New message",
      "TO": "To",
      "OBJECT": "Object",
      "OBJECT_HELP": "Object",
      "ENCRYPTED_HELP": "Please note this message will be encrypted before sending so that only the recipient can read it and be sure you are the author.",
      "MESSAGE": "Message",
      "MESSAGE_HELP": "Message content",
      "CONTENT_CONFIRMATION": "No message content.<br/><br/>Are your sure you want to send this message?"
    },
    "VIEW": {
      "TITLE": "Message",
      "SENDER": "Sent by",
      "RECIPIENT": "Sent to",
      "NO_CONTENT": "Empty message",
      "DELETE": "Delete the message"
    },
    "CONFIRM": {
      "REMOVE": "Are you sure you want to <b>delete this message</b>?<br/><br/> This operation is irreversible.",
      "REMOVE_ALL": "Are you sure you want to <b>delete all messages</b>?<br/><br/> This operation is irreversible.",
      "MARK_ALL_AS_READ": "Are you sure you want to <b>mark all message as read</b>?",
      "USER_HAS_NO_PROFILE": "This identity has no Cesium + profile. It may not use the Cesium + extension, so it <b>will not read your message</b>.<br/><br/>Are you sure you want <b>to continue</b>?"
    },
    "INFO": {
      "MESSAGE_REMOVED": "Message successfully deleted",
      "All_MESSAGE_REMOVED": "Messages successfully deleted",
      "MESSAGE_SENT": "Message sent"
    },
    "ERROR": {
      "SEND_MSG_FAILED": "Error while sending message.",
      "LOAD_MESSAGES_FAILED": "Error while loading messages.",
      "LOAD_MESSAGE_FAILED": "Error while loading message.",
      "MESSAGE_NOT_READABLE": "Unable to read message.",
      "USER_NOT_RECIPIENT": "You are not the recipient of this message: unable to read it.",
      "NOT_AUTHENTICATED_MESSAGE": "The authenticity of the message is not certain or its content is corrupted.",
      "REMOVE_MESSAGE_FAILED": "Error while deleting message",
      "MESSAGE_CONTENT_TOO_LONG": "Value too long ({{maxLength}} characters max).",
      "MARK_AS_READ_FAILED": "Unable to mark the message as 'read'.",
      "LOAD_NOTIFICATIONS_FAILED": "Error while loading messages notifications.",
      "REMOVE_All_MESSAGES_FAILED": "Error while removing all messages.",
      "MARK_ALL_AS_READ_FAILED": "Error while marking messages as read.",
      "RECIPIENT_IS_MANDATORY": "Recipient is mandatory."
    }
  },
  "BLOCKCHAIN": {
    "LOOKUP": {
      "SEARCH_HELP": "Block number, hash...",
      "POPOVER_FILTER_TITLE": "Filter",
      "HEADER_MEDIAN_TIME": "Date / Time",
      "HEADER_BLOCK": "Block #",
      "HEADER_ISSUER": "Peer owner",
      "BTN_LAST": "Last blocks",
      "BTN_TX": "Transactions",
      "DISPLAY_QUERY": "View query",
      "HIDE_QUERY": "Hide query",
      "TX_SEARCH_FILTER": {
        "MEMBER_FLOWS": "<b class=\"ion-person\"></b> Members input/output",
        "EXISTING_TRANSACTION": "<b class=\"ion-card\"></b> Having transactions",
        "PERIOD": "<b class=\"ion-clock\"></b> Between <b class=\"gray\">{{params[1]|medianDateShort}}</b> ({{params[1]|medianTime}}) and <b class=\"gray\">{{params[2]|medianDateShort}}</b> ({{params[2]|medianTime}})",
        "ISSUER": "<b class=\"ion-android-desktop\"></b> Computed by {{params[1]|formatPubkey}}",
        "TX_PUBKEY": "<b class=\"ion-card\"></b> Transactions concerning <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}"
      }
    },
    "ERROR": {
      "SEARCH_BLOCKS_FAILED": "Error while searching blocks."
    }
  },
  "REGISTRY": {
    "CATEGORY": "Main activity",
    "GENERAL_DIVIDER": "Basic information",
    "LOCATION_DIVIDER": "Address",
    "SOCIAL_NETWORKS_DIVIDER": "Social networks, web sites",
    "TECHNICAL_DIVIDER": "Technical data",
    "BTN_SHOW_WOT": "People",
    "BTN_SHOW_WOT_HELP": "Search for people",
    "BTN_SHOW_PAGES": "Pages",
    "BTN_SHOW_PAGES_HELP": "Search for pages",
    "BTN_NEW": "New page",
    "MY_PAGES": "My pages",
    "NO_PAGE": "No page",
    "SEARCH": {
      "TITLE": "Pages",
      "SEARCH_HELP": "What, Who: hairdresser, Lili's restaurant, ...",
      "BTN_ADD": "New",
      "BTN_LAST_RECORDS": "Recent pages",
      "BTN_ADVANCED_SEARCH": "Advanced search",
      "BTN_OPTIONS": "Advanced search",
      "TYPE": "Kind of organization",
      "LOCATION_HELP": "Where: City, Country",
      "RESULTS": "Results",
      "RESULT_COUNT_LOCATION": "{{count}} result{{count>0?'s':''}}, near {{location}}",
      "RESULT_COUNT": "{{count}} result{{count>0?'s':''}}",
      "LAST_RECORDS": "Recent pages",
      "LAST_RECORD_COUNT_LOCATION": "{{count}} recent page{{count>0?'s':''}}, near {{location}}",
      "LAST_RECORD_COUNT": "{{count}} recent page{{count>0?'s':''}}",
      "POPOVER_FILTERS": {
        "BTN_ADVANCED_SEARCH": "Advanced options?"
      }
    },
    "VIEW": {
      "TITLE": "Registry",
      "CATEGORY": "Main activity:",
      "LOCATION": "Address:",
      "MENU_TITLE": "Options",
      "POPOVER_SHARE_TITLE": "{{title}}",
      "REMOVE_CONFIRMATION" : "Are you sure you want to delete this reference?<br/><br/>This is irreversible."
    },
    "TYPE": {
      "TITLE": "New page",
      "SELECT_TYPE": "Kind of organization:",
      "ENUM": {
        "SHOP": "Local shops",
        "COMPANY": "Company",
        "ASSOCIATION": "Association",
        "INSTITUTION": "Institution"
      }
    },
    "EDIT": {
      "TITLE": "Edit",
      "TITLE_NEW": "New page",
      "RECORD_TYPE":"Kind of organization",
      "RECORD_TITLE": "Name",
      "RECORD_TITLE_HELP": "Name",
      "RECORD_DESCRIPTION": "Description",
      "RECORD_DESCRIPTION_HELP": "Describe activity",
      "RECORD_ADDRESS": "Street",
      "RECORD_ADDRESS_HELP": "Street, building...",
      "RECORD_CITY": "City",
      "RECORD_CITY_HELP": "City, Country",
      "RECORD_SOCIAL_NETWORKS": "Social networks and web site",
      "RECORD_PUBKEY": "Public key",
      "RECORD_PUBKEY_HELP": "Public key to receive payments"
    },
    "WALLET": {
      "PAGE_DIVIDER": "Pages",
      "PAGE_DIVIDER_HELP": "Pages refer to activities accepting money or promoting it: local shops, companies, associations, institutions. They are stored outside the currency network, in <a ui-sref=\"app.es_network\">the Cesium+ network</a>."
    },
    "ERROR": {
      "LOAD_CATEGORY_FAILED": "Loading main activities failed",
      "LOAD_RECORD_FAILED": "Loading failed",
      "LOOKUP_RECORDS_FAILED": "Error while loading records.",
      "REMOVE_RECORD_FAILED": "Deleting failed",
      "SAVE_RECORD_FAILED": "Saving failed",
      "RECORD_NOT_EXISTS": "Record not found",
      "GEO_LOCATION_NOT_FOUND": "City or zip code not found"
    },
    "INFO": {
      "RECORD_REMOVED" : "Page successfully deleted",
      "RECORD_SAVED": "Page successfully saved"
    }
  },
  "PROFILE": {
    "PROFILE_DIVIDER": "Cesium+ profile",
    "PROFILE_DIVIDER_HELP": "These are ancillary data, stored outside the currency network, in <a ui-sref=\"app.es_network\">the Cesium+ network</a>.",
    "NO_PROFILE_DEFINED": "No Cesium+ profile",
    "BTN_ADD": "Create my profile",
    "BTN_EDIT": "Edit my profile",
    "BTN_DELETE": "Delete my profile",
    "BTN_REORDER": "Reorder",
    "UID": "Pseudonym",
    "TITLE": "Lastname, FirstName",
    "TITLE_HELP": "Name",
    "DESCRIPTION": "About me",
    "DESCRIPTION_HELP": "About me...",
    "SOCIAL_HELP": "http://...",
    "GENERAL_DIVIDER": "General data",
    "SOCIAL_NETWORKS_DIVIDER": "Social networks and web site",
    "TECHNICAL_DIVIDER": "Technical data",
    "MODAL_AVATAR": {
      "TITLE": "Avatar",
      "SELECT_FILE_HELP": "<b>Choose an image file</b>:",
      "BTN_SELECT_FILE": "Choose an image",
      "RESIZE_HELP": "<b>Re-crop the image</b> if necessary. A click on the image allows to move it. Click on the area at the bottom left to zoom in.",
      "RESULT_HELP": "<b>Here is the result</b> as seen on your profile:"
    },
    "CONFIRM": {
      "DELETE": "Are you sure you want to <b>delete your Cesium+ profile ?</b><br/><br/>This operation is irreversible."
    },
    "ERROR": {
      "REMOVE_PROFILE_FAILED": "Deleting profile failed",
      "LOAD_PROFILE_FAILED": "Could not load user profile.",
      "SAVE_PROFILE_FAILED": "Saving profile failed",
      "INVALID_SOCIAL_NETWORK_FORMAT": "Invalid format: please fill a valid Internet address.<br/><br/>Examples :<ul><li>- A Facebook page (https://www.facebook.com/user)</li><li>- A web page (http://www.domain.com)</li><li>- An email address (joe@dalton.com)</li></ul>",
      "IMAGE_RESIZE_FAILED": "Error while resizing picture"
    },
    "INFO": {
      "PROFILE_REMOVED": "Profile deleted",
      "PROFILE_SAVED": "Profile saved"
    },
    "HELP": {
      "WARNING_PUBLIC_DATA": "Please note that the information published here <b>is public</b>: visible including by <b>not logged in people</b>.<br/>{{'PROFILE.PROFILE_DIVIDER_HELP'|translate}}"
    }
  },
  "LOCATION": {
    "BTN_GEOLOC_ADDRESS": "Find my address on the map",
    "USE_GEO_POINT": "Appear on {{'COMMON.APP_NAME'|translate}} maps?",
    "LOADING_LOCATION": "Searching address...",
    "LOCATION_DIVIDER": "Localisation",
    "ADDRESS": "Address",
    "ADDRESS_HELP": "Address (optional)",
    "CITY": "City",
    "CITY_HELP": "City, Country",
    "DISTANCE": "Maximum distance around the city",
    "DISTANCE_UNIT": "mi",
    "DISTANCE_OPTION": "{{value}} {{'LOCATION.DISTANCE_UNIT'|translate}}",
    "SEARCH_HELP": "City, Country",
    "PROFILE_POSITION": "Profile position",
    "MODAL": {
      "TITLE": "Search address",
      "SEARCH_HELP": "City, Country",
      "ALTERNATIVE_RESULT_DIVIDER": "Alternative results for <b>{{address}}</b>:",
      "POSITION": "lat/lon : {{lat}} / {{lon}}"
    },
    "ERROR": {
      "CITY_REQUIRED_IF_STREET": "Required if a street has been filled",
      "REQUIRED_FOR_LOCATION": "Required field to appear on the map",
      "INVALID_FOR_LOCATION": "Unknown address",
      "GEO_LOCATION_FAILED": "Unable to retrieve your current position. Please use the search button.",
      "ADDRESS_LOCATION_FAILED": "Unable to retrieve the address position"
    }
  },
  "SUBSCRIPTION": {
    "SUBSCRIPTION_DIVIDER": "Online services",
    "SUBSCRIPTION_DIVIDER_HELP": "Online services offer optional additional services, delegated to a third party.",
    "BTN_ADD": "Add a service",
    "BTN_EDIT": "Manage my services",
    "NO_SUBSCRIPTION": "No service defined",
    "SUBSCRIPTION_COUNT": "Services / Subscription",
    "EDIT": {
      "TITLE": "Online services",
      "HELP_TEXT": "Manage your subscriptions and other online services here",
      "PROVIDER": "Provider:"
    },
    "TYPE": {
      "ENUM": {
        "EMAIL": "Receive email notifications"
      }
    },
    "CONFIRM": {
      "DELETE_SUBSCRIPTION": "Are you sur you want to <b>delete this subscription</b>?"
    },
    "ERROR": {
      "LOAD_SUBSCRIPTIONS_FAILED": "Error while loading online services",
      "ADD_SUBSCRIPTION_FAILED": "Error while adding subscription",
      "UPDATE_SUBSCRIPTION_FAILED": "Error during subscription update",
      "DELETE_SUBSCRIPTION_FAILED": "Error while deleting subscription"
    },
    "MODAL_EMAIL": {
      "TITLE" : "Notification by email",
      "HELP" : "Fill out this form to <b>be notified by email</ b> of your account's events. <br/>Your email address will be encrypted only to be visible to the service provider.",
      "EMAIL_LABEL" : "Your email:",
      "EMAIL_HELP": "john@domain.com",
      "FREQUENCY_LABEL": "Frequency of notifications:",
      "FREQUENCY_DAILY": "Daily",
      "FREQUENCY_WEEKLY": "Weekly",
      "PROVIDER": "Service Provider:"
    }
  },
  "DOCUMENT": {
    "HASH": "Hash: ",
    "LOOKUP": {
      "TITLE": "Document search",
      "BTN_ACTIONS": "Actions",
      "SEARCH_HELP": "issuer:AAA*, time:1508406169",
      "LAST_DOCUMENTS_DOTS": "Last documents:",
      "LAST_DOCUMENTS": "Last documents",
      "SHOW_QUERY": "Show query",
      "HIDE_QUERY": "Hide query",
      "HEADER_TIME": "Time/Hour",
      "HEADER_ISSUER": "Issuer",
      "HEADER_RECIPIENT": "Recipient",
      "HEADER_AMOUNT": "Amount",
      "READ": "Read",
      "BTN_REMOVE": "Delete this document",
      "BTN_COMPACT": "Compact",
      "HAS_CREATE_OR_UPDATE_PROFILE": "create or edit his profile",
      "POPOVER_ACTIONS": {
        "TITLE": "Actions",
        "REMOVE_ALL": "Delete these documents..."
      }
    },
    "INFO": {
      "REMOVED": "Deleted document"
    },
    "CONFIRM": {
      "REMOVE": "Are you sure you want to <b>delete this document</b>?",
      "REMOVE_ALL": "Are you sure you want to <b>delete these documents</b>?"
    },
    "ERROR": {
      "LOAD_DOCUMENTS_FAILED": "Error searching documents",
      "REMOVE_FAILED": "Error deleting the document",
      "REMOVE_ALL_FAILED": "Error deleting documents"
    }
  },
  "ES_SETTINGS": {
    "PLUGIN_NAME": "Cesium+",
    "PLUGIN_NAME_HELP": "User profiles, notifications, private messages",
    "ENABLE_TOGGLE": "Enable extension?",
    "ENABLE_REMOTE_STORAGE": "Enable remote storage for settings?",
    "ENABLE_REMOTE_STORAGE_HELP": "Enables (encrypted) storage of your settings on Cesium + nodes",
    "ENABLE_MESSAGE_TOGGLE": "Enable private messages?",
    "PEER": "Data peer address",
    "POPUP_PEER": {
      "TITLE" : "Data peer",
      "HELP" : "Set the address of the peer to use:",
      "PEER_HELP": "server.domain.com:port"
    },
    "NOTIFICATIONS": {
      "DIVIDER": "Notifications",
      "HELP_TEXT": "Enable the types of notifications you want to receive:",
      "ENABLE_TX_SENT": "Notify the validation of <b>sent payments</b>?",
      "ENABLE_TX_RECEIVED": "Notify the validation of <b>received payments</b>?",
      "ENABLE_CERT_SENT": "Notify the validation of <b>sent certifications</b>?",
      "ENABLE_CERT_RECEIVED": "Notify the validation of <b>received certifications</b>?",
      "ENABLE_HTML5_NOTIFICATION": "Warn with each new notification?",
      "ENABLE_HTML5_NOTIFICATION_HELP": "Opens a small popup window with each new notification."
    },
    "CONFIRM": {
      "ASK_ENABLE_TITLE": "Optional features",
      "ASK_ENABLE": "Cesium+ extension is <b>disabled</b> in your settings, making some features inactive: <ul><li>&nbsp;&nbsp;<b><i class=\"icon ion-person\"></i> user profiles</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-android-notifications\"></i> Notifications</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-email\"></i> Private messages</b>.</ul><br/><br/><b>Do you want to enable</b> the extension?"
    }
  },
  "ES_WALLET": {
    "ERROR": {
      "RECIPIENT_IS_MANDATORY": "A recipient is required for encryption."
    },
    "ES_PEER": {
      "NAME": "Name",
      "DOCUMENTS": "Documents",
      "SOFTWARE": "Software",
      "DOCUMENT_COUNT": "Number of documents",
      "EMAIL_SUBSCRIPTION_COUNT": "{{emailSubscription}} subscribers to email notification"
    }
  },
  "EVENT": {
    "NODE_STARTED": "Your node ES API <b>{{params[0]}}</b> is UP",
    "NODE_BMA_DOWN": "Node <b>{{params[0]}}:{{params[1]}}</b> (used by your ES API) is <b>unreachable</b>.",
    "NODE_BMA_UP": "Node <b>{{params[0]}}:{{params[1]}}</b> is reachable again.",
    "MEMBER_JOIN": "You are now a <b>member</b> of currency <b>{{params[0]}}</b>!",
    "MEMBER_LEAVE": "You are <b>not a member anymore</b> of currency <b>{{params[0]}}</b>!",
    "MEMBER_EXCLUDE": "You are <b>not more member</b> of the currency <b>{{params[0]}}</b>, for lack of renewal or lack of certifications.",
    "MEMBER_REVOKE": "Your account has been revoked. It will no longer be a member of the currency <b>{{params[0]}}</b>.",
    "MEMBER_ACTIVE": "Your membership to <b>{{params[0]}}</b> has been <b>renewed successfully</b>.",
    "TX_SENT": "Your payment to <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> was executed.",
    "TX_SENT_MULTI": "Your payment to <b>{{params[1]}}</b> was executed.",
    "TX_RECEIVED": "You received a payment from <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "TX_RECEIVED_MULTI": "You received a payment from <b>{{params[1]}}</b>.",
    "CERT_SENT": "Your <b>certification</b> to <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> was executed.",
    "CERT_RECEIVED": "You  have <b>received a certification</b> from <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "USER": {
      "LIKE_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> like your profile",
      "FOLLOW_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> follows your activity",
      "STAR_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> rated you ({{params[3]}} <i class=\"ion-star\">)",
      "MODERATION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> asks you for a moderation on the profile: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "DELETION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> reported a profile to be deleted: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "ABUSE_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has requested moderation on your profile"
    },
    "PAGE": {
      "NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has commented on your referencing: <b>{{params[2]}}</b>",
      "UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has modified his comment on your referencing: <b>{{params[2]}}</b>",
      "NEW_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has replied to your comment on the referencing: <b>{{params[2]}}</b>",
      "UPDATE_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has modified his answer to your comment, on the referencing: <b>{{params[2]}}</b>",
      "FOLLOW_NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has commented on the page: <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has modified his comment on the page: <b>{{params[2]}}</b>",
      "FOLLOW_NEW": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> added a page: <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> updated the page: <b>{{params[2]}}</b>",
      "MODERATION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> asks you for a moderation on the page: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "DELETION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> reported a page to be deleted: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "ABUSE_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has requested moderation on your page: <b>{{params[2]}}</b>"
    }
  },
  "LIKE": {
    "ERROR": {
      "FAILED_TOGGLE_LIKE": "Unable to execute this action."
    }
  },
  "CONFIRM": {
    "ES_USE_FALLBACK_NODE": "Data node <b>{{old}}</b> unreachable or invalid address.<br/><br/>Do you want to temporarily use the data node <b>{{new}}</b>?"
  },
  "ERROR": {
    "ES_CONNECTION_ERROR": "Data node <b>{{server}}</b> unreachable or invalid address.<br/><br/>Cesium will continue to work, <b>without the Cesium+</b> extension (user profiles, private messages, maps and graphics).<br/><br/>Check your Internet connection, or change data node in <a class=\"positive\" ng-click=\"doQuickFix('settings')\">extension settings</a>.",
    "ES_MAX_UPLOAD_BODY_SIZE": "The volume of data to be sent exceeds the limit set by the server.<br/><br/>Please try again after, for example, deleting photos."
  }
}
);

$translateProvider.translations("en", {
  "COMMON": {
    "CATEGORY": "Category",
    "CATEGORIES": "Categories",
    "CATEGORY_SEARCH_HELP": "Search",
    "COMMENT_HELP": "Comments",
    "LAST_MODIFICATION_DATE": "Updated on ",
    "SUBMIT_BY": "Submitted by",
    "BTN_LIKE": "I like",
    "BTN_LIKE_REMOVE": "I don't like anymore",
    "LIKES_TEXT": "{{total}} {{total > 1 ? 'people' : 'person'}} liked this page",
    "ABUSES_TEXT": "{{total}} {{total > 1 ? 'people' : 'person'}} reported a problem on this page",
    "BTN_REPORT_ABUSE_DOTS": "Report a problem or an abuse...",
    "BTN_REMOVE_REPORTED_ABUSE": "Cancel my problem report",
    "BTN_PUBLISH": "Publish",
    "BTN_PICTURE_DELETE": "Delete",
    "BTN_PICTURE_FAVORISE": "Default",
    "BTN_PICTURE_ROTATE": "Rotate",
    "BTN_ADD_PICTURE": "Add picture",
    "NOTIFICATION": {
      "TITLE": "New notification | {{'COMMON.APP_NAME'|translate}}",
      "HAS_UNREAD": "You have {{count}} unread notification{{count>0?'s':''}}"
    },
    "NOTIFICATIONS": {
      "TITLE": "Notifications",
      "MARK_ALL_AS_READ": "Mark all as read",
      "NO_RESULT": "No notification",
      "SHOW_ALL": "Show all",
      "LOAD_NOTIFICATIONS_FAILED": "Could not load notifications"
    },
    "REPORT_ABUSE": {
      "TITLE": "Report a problem",
      "SUB_TITLE": "Please explain briefly the problem:",
      "REASON_HELP": "I explain the problem...",
      "ASK_DELETE": "Request removal?",
      "CONFIRM": {
        "SENT": "Request sent. Thank you!"
      }
    }
  },
  "MENU": {
    "REGISTRY": "Pages",
    "USER_PROFILE": "My Profile",
    "MESSAGES": "Messages",
    "NOTIFICATIONS": "Notifications",
    "INVITATIONS": "Invitations"
  },
  "ACCOUNT": {
    "NEW": {
      "ORGANIZATION_ACCOUNT": "Account for an organization",
      "ORGANIZATION_ACCOUNT_HELP": "If you represent a company, association, etc.<br/>No universal dividend will be created by this account."
    },
    "EVENT": {
      "MEMBER_WITHOUT_PROFILE": "You can <a ui-sref=\"app.edit_profile\">fill your Cesium+ profile</a> (optional) to provide better visibility of your account. This profile will be stored in <b>a directory independent</b> of the currency, but decentralized."
    },
    "ERROR": {
      "WS_CONNECTION_FAILED": "Cesium can not receive notifications because of a technical error (connection to the Cesium + data node).<br/><br/>If the problem persists, please <b>choose another data node</b> in Cesium+ settings."
    }
  },
  "WOT": {
    "BTN_SUGGEST_CERTIFICATIONS_DOTS": "Suggest identities to certify...",
    "BTN_ASK_CERTIFICATIONS_DOTS": "Ask members to certify me...",
    "BTN_ASK_CERTIFICATION": "Ask a certification",
    "SUGGEST_CERTIFICATIONS_MODAL": {
      "TITLE": "Suggest certifications",
      "HELP": "Select your suggestions"
    },
    "ASK_CERTIFICATIONS_MODAL": {
      "TITLE": "Ask certifications",
      "HELP": "Select recipients"
    },
    "SEARCH": {
      "DIVIDER_PROFILE": "Accounts",
      "DIVIDER_PAGE": "Pages",
      "DIVIDER_GROUP": "Groups"
    },
    "CONFIRM": {
      "SUGGEST_CERTIFICATIONS": "Are you sure you want <b>to send these certification suggestions</b>?",
      "ASK_CERTIFICATION": "Are you sure you want to <b>send a certification request</b>?",
      "ASK_CERTIFICATIONS": "Are you sure you want to <b>send a certification request</b> to these people?"
    }
  },
  "INVITATION": {
    "TITLE": "Invitations",
    "NO_RESULT": "No invitation received",
    "BTN_DELETE_ALL": "Delete all invitations",
    "BTN_DELETE": "Delete invitation",
    "BTN_NEW_INVITATION": "New invitation",
    "ASK_CERTIFICATION": "<a href=\"#/app/wot/{{pubkey}}/{{::uid}}\">{{::name||uid}}</a> asks for your certification",
    "SUGGESTION_CERTIFICATION": "<a href=\"#/app/wot/{{::pubkey}}/{{::uid}}\">{{::name||uid}}</a> is suggested for certification",
    "SUGGESTED_BY": "Suggestion sent by <a class=\"positive\" href=\"#/app/wot/{{::issuer.pubkey}}/{{::issuer.uid}}\">{{::issuer.name||issuer.uid}}</a>",
    "NOTIFICATIONS": {
      "TITLE": "Invitations"
    },
    "LIST": {
      "TITLE": "Invitations"
    },
    "NEW": {
      "TITLE": "New invitation",
      "RECIPIENTS": "A",
      "RECIPIENTS_HELP": "Recipients of the invitation",
      "RECIPIENTS_MODAL_TITLE": "Recipients",
      "RECIPIENTS_MODAL_HELP": "Please choose recipients:",
      "SUGGESTION_IDENTITIES": "Suggestions for certification",
      "SUGGESTION_IDENTITIES_HELP": "Certifications to suggest",
      "SUGGESTION_IDENTITIES_MODAL_TITLE": "Suggestions",
      "SUGGESTION_IDENTITIES_MODAL_HELP": "Please choose your suggestions:"
    },
    "CONFIRM": {
      "DELETE_ALL_CONFIRMATION": "Removing invitations is <b>an irreversible operation</b>.<br/><br/><b>Are you sure</b> you want to continue",
      "SEND_INVITATIONS_TO_CERTIFY": "<b>Are you sure</b> you want <b>to sent this invitation to certify</b> ?"
    },
    "INFO": {
      "INVITATION_SENT": "Invitation sent"
    },
    "ERROR": {
      "LOAD_INVITATIONS_FAILED": "Error while loading invitations",
      "REMOVE_INVITATION_FAILED": "Error while deleting the invitation",
      "REMOVE_ALL_INVITATIONS_FAILED": "Error while deleting invitations",
      "SEND_INVITATION_FAILED": "Error while sending invitation",
      "BAD_INVITATION_FORMAT": "<span class=\"assertive\"><i class=\"ion-close-circled\"></i> Invitation unreadable (format unknown)</span> - sent by <a ui-sref=\"app.wot_identity({pubkey: '{{::pubkey}}', uid: '{{::uid}}' })\">{{::name||uid}}</a>"
    }
  },
  "COMMENTS": {
    "DIVIDER": "Comments",
    "SHOW_MORE_COMMENTS": "Show previous comments",
    "COMMENT_HELP": "Your comment, question...",
    "COMMENT_HELP_REPLY_TO": "Your answer...",
    "BTN_SEND": "Send",
    "POPOVER_SHARE_TITLE": "Message #{{number}}",
    "REPLY": "Reply",
    "REPLY_TO": "Respond to:",
    "REPLY_TO_LINK": "In response to ",
    "REPLY_TO_DELETED_COMMENT": "In response to a deleted comment",
    "REPLY_COUNT": "{{replyCount}} responses",
    "DELETED_COMMENT": "Comment deleted",
    "MODIFIED_ON": "modified on {{time|formatDate}}",
    "MODIFIED_PARENTHESIS": "(modified then)",
    "ERROR": {
      "FAILED_SAVE_COMMENT": "Saving comment failed",
      "FAILED_REMOVE_COMMENT": "Deleting comment failed"
    }
  },
  "MESSAGE": {
    "REPLY_TITLE_PREFIX": "Re: ",
    "FORWARD_TITLE_PREFIX": "Fw: ",
    "BTN_REPLY": "Reply",
    "BTN_COMPOSE": "New message",
    "BTN_WRITE": "Write",
    "NO_MESSAGE_INBOX": "No message received",
    "NO_MESSAGE_OUTBOX": "No message sent",
    "NOTIFICATIONS": {
      "TITLE": "Messages",
      "MESSAGE_RECEIVED": "You <b>received a message</b><br/>from"
    },
    "LIST": {
      "INBOX": "Inbox",
      "OUTBOX": "Outbox",
      "LAST_INBOX": "New messages",
      "LAST_OUTBOX": "Sent messages",
      "BTN_LAST_MESSAGES": "Recent messages",
      "TITLE": "Private messages",
      "SEARCH_HELP": "Search in messages",
      "POPOVER_ACTIONS": {
        "TITLE": "Options",
        "DELETE_ALL": "Delete all messages"
      }
    },
    "COMPOSE": {
      "TITLE": "New message",
      "TITLE_REPLY": "Reply",
      "SUB_TITLE": "New message",
      "TO": "To",
      "OBJECT": "Object",
      "OBJECT_HELP": "Object",
      "ENCRYPTED_HELP": "Please note this message will be encrypted before sending so that only the recipient can read it and be sure you are the author.",
      "MESSAGE": "Message",
      "MESSAGE_HELP": "Message content",
      "CONTENT_CONFIRMATION": "No message content.<br/><br/>Are your sure you want to send this message?"
    },
    "VIEW": {
      "TITLE": "Message",
      "SENDER": "Sent by",
      "RECIPIENT": "Sent to",
      "NO_CONTENT": "Empty message",
      "DELETE": "Delete the message"
    },
    "CONFIRM": {
      "REMOVE": "Are you sure you want to <b>delete this message</b>?<br/><br/> This operation is irreversible.",
      "REMOVE_ALL": "Are you sure you want to <b>delete all messages</b>?<br/><br/> This operation is irreversible.",
      "MARK_ALL_AS_READ": "Are you sure you want to <b>mark all message as read</b>?",
      "USER_HAS_NO_PROFILE": "This identity has no Cesium + profile. It may not use the Cesium + extension, so it <b>will not read your message</b>.<br/><br/>Are you sure you want <b>to continue</b>?"
    },
    "INFO": {
      "MESSAGE_REMOVED": "Message successfully deleted",
      "All_MESSAGE_REMOVED": "Messages successfully deleted",
      "MESSAGE_SENT": "Message sent"
    },
    "ERROR": {
      "SEND_MSG_FAILED": "Error while sending message.",
      "LOAD_MESSAGES_FAILED": "Error while loading messages.",
      "LOAD_MESSAGE_FAILED": "Error while loading message.",
      "MESSAGE_NOT_READABLE": "Unable to read message.",
      "USER_NOT_RECIPIENT": "You are not the recipient of this message: unable to read it.",
      "NOT_AUTHENTICATED_MESSAGE": "The authenticity of the message is not certain or its content is corrupted.",
      "REMOVE_MESSAGE_FAILED": "Error while deleting message",
      "MESSAGE_CONTENT_TOO_LONG": "Value too long ({{maxLength}} characters max).",
      "MARK_AS_READ_FAILED": "Unable to mark the message as 'read'.",
      "LOAD_NOTIFICATIONS_FAILED": "Error while loading messages notifications.",
      "REMOVE_All_MESSAGES_FAILED": "Error while removing all messages.",
      "MARK_ALL_AS_READ_FAILED": "Error while marking messages as read.",
      "RECIPIENT_IS_MANDATORY": "Recipient is mandatory."
    }
  },
  "BLOCKCHAIN": {
    "LOOKUP": {
      "SEARCH_HELP": "Block number, hash...",
      "POPOVER_FILTER_TITLE": "Filter",
      "HEADER_MEDIAN_TIME": "Date / Time",
      "HEADER_BLOCK": "Block #",
      "HEADER_ISSUER": "Peer owner",
      "BTN_LAST": "Last blocks",
      "BTN_TX": "Transactions",
      "DISPLAY_QUERY": "View query",
      "HIDE_QUERY": "Hide query",
      "TX_SEARCH_FILTER": {
        "MEMBER_FLOWS": "<b class=\"ion-person\"></b> Members input/output",
        "EXISTING_TRANSACTION": "<b class=\"ion-card\"></b> Having transactions",
        "PERIOD": "<b class=\"ion-clock\"></b> Between <b class=\"gray\">{{params[1]|medianDateShort}}</b> ({{params[1]|medianTime}}) and <b class=\"gray\">{{params[2]|medianDateShort}}</b> ({{params[2]|medianTime}})",
        "ISSUER": "<b class=\"ion-android-desktop\"></b> Computed by {{params[1]|formatPubkey}}",
        "TX_PUBKEY": "<b class=\"ion-card\"></b> Transactions concerning <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}"
      }
    },
    "ERROR": {
      "SEARCH_BLOCKS_FAILED": "Error while searching blocks."
    }
  },
  "REGISTRY": {
    "CATEGORY": "Main activity",
    "GENERAL_DIVIDER": "Basic information",
    "LOCATION_DIVIDER": "Address",
    "SOCIAL_NETWORKS_DIVIDER": "Social networks, web sites",
    "TECHNICAL_DIVIDER": "Technical data",
    "BTN_SHOW_WOT": "People",
    "BTN_SHOW_WOT_HELP": "Search for people",
    "BTN_SHOW_PAGES": "Pages",
    "BTN_SHOW_PAGES_HELP": "Search for pages",
    "BTN_NEW": "New page",
    "MY_PAGES": "My pages",
    "NO_PAGE": "No page",
    "SEARCH": {
      "TITLE": "Pages",
      "SEARCH_HELP": "What, Who: hairdresser, Lili's restaurant, ...",
      "BTN_ADD": "New",
      "BTN_LAST_RECORDS": "Recent pages",
      "BTN_ADVANCED_SEARCH": "Advanced search",
      "BTN_OPTIONS": "Advanced search",
      "TYPE": "Kind of organization",
      "LOCATION_HELP": "Where: City, Country",
      "RESULTS": "Results",
      "RESULT_COUNT_LOCATION": "{{count}} result{{count>0?'s':''}}, near {{location}}",
      "RESULT_COUNT": "{{count}} result{{count>0?'s':''}}",
      "LAST_RECORDS": "Recent pages",
      "LAST_RECORD_COUNT_LOCATION": "{{count}} recent page{{count>0?'s':''}}, near {{location}}",
      "LAST_RECORD_COUNT": "{{count}} recent page{{count>0?'s':''}}",
      "POPOVER_FILTERS": {
        "BTN_ADVANCED_SEARCH": "Advanced options?"
      }
    },
    "VIEW": {
      "TITLE": "Registry",
      "CATEGORY": "Main activity:",
      "LOCATION": "Address:",
      "MENU_TITLE": "Options",
      "POPOVER_SHARE_TITLE": "{{title}}",
      "REMOVE_CONFIRMATION" : "Are you sure you want to delete this reference?<br/><br/>This is irreversible."
    },
    "TYPE": {
      "TITLE": "New page",
      "SELECT_TYPE": "Kind of organization:",
      "ENUM": {
        "SHOP": "Local shops",
        "COMPANY": "Company",
        "ASSOCIATION": "Association",
        "INSTITUTION": "Institution"
      }
    },
    "EDIT": {
      "TITLE": "Edit",
      "TITLE_NEW": "New page",
      "RECORD_TYPE":"Kind of organization",
      "RECORD_TITLE": "Name",
      "RECORD_TITLE_HELP": "Name",
      "RECORD_DESCRIPTION": "Description",
      "RECORD_DESCRIPTION_HELP": "Describe activity",
      "RECORD_ADDRESS": "Street",
      "RECORD_ADDRESS_HELP": "Street, building...",
      "RECORD_CITY": "City",
      "RECORD_CITY_HELP": "City, Country",
      "RECORD_SOCIAL_NETWORKS": "Social networks and web site",
      "RECORD_PUBKEY": "Public key",
      "RECORD_PUBKEY_HELP": "Public key to receive payments"
    },
    "WALLET": {
      "PAGE_DIVIDER": "Pages",
      "PAGE_DIVIDER_HELP": "Pages refer to activities accepting money or promoting it: local shops, companies, associations, institutions. They are stored outside the currency network, in <a ui-sref=\"app.es_network\">the Cesium+ network</a>."
    },
    "ERROR": {
      "LOAD_CATEGORY_FAILED": "Loading main activities failed",
      "LOAD_RECORD_FAILED": "Loading failed",
      "LOOKUP_RECORDS_FAILED": "Error while loading records.",
      "REMOVE_RECORD_FAILED": "Deleting failed",
      "SAVE_RECORD_FAILED": "Saving failed",
      "RECORD_NOT_EXISTS": "Record not found",
      "GEO_LOCATION_NOT_FOUND": "City or zip code not found"
    },
    "INFO": {
      "RECORD_REMOVED" : "Page successfully deleted",
      "RECORD_SAVED": "Page successfully saved"
    }
  },
  "PROFILE": {
    "PROFILE_DIVIDER": "Cesium+ profile",
    "PROFILE_DIVIDER_HELP": "These are ancillary data, stored outside the currency network, in <a ui-sref=\"app.es_network\">the Cesium+ network</a>.",
    "NO_PROFILE_DEFINED": "No Cesium+ profile",
    "BTN_ADD": "Create my profile",
    "BTN_EDIT": "Edit my profile",
    "BTN_DELETE": "Delete my profile",
    "BTN_REORDER": "Reorder",
    "UID": "Pseudonym",
    "TITLE": "Lastname, FirstName",
    "TITLE_HELP": "Name",
    "DESCRIPTION": "About me",
    "DESCRIPTION_HELP": "About me...",
    "SOCIAL_HELP": "http://...",
    "GENERAL_DIVIDER": "General data",
    "SOCIAL_NETWORKS_DIVIDER": "Social networks and web site",
    "TECHNICAL_DIVIDER": "Technical data",
    "MODAL_AVATAR": {
      "TITLE": "Avatar",
      "SELECT_FILE_HELP": "<b>Choose an image file</b>:",
      "BTN_SELECT_FILE": "Choose an image",
      "RESIZE_HELP": "<b>Re-crop the image</b> if necessary. A click on the image allows to move it. Click on the area at the bottom left to zoom in.",
      "RESULT_HELP": "<b>Here is the result</b> as seen on your profile:"
    },
    "CONFIRM": {
      "DELETE": "Are you sure you want to <b>delete your Cesium+ profile ?</b><br/><br/>This operation is irreversible."
    },
    "ERROR": {
      "REMOVE_PROFILE_FAILED": "Deleting profile failed",
      "LOAD_PROFILE_FAILED": "Could not load user profile.",
      "SAVE_PROFILE_FAILED": "Saving profile failed",
      "INVALID_SOCIAL_NETWORK_FORMAT": "Invalid format: please fill a valid Internet address.<br/><br/>Examples :<ul><li>- A Facebook page (https://www.facebook.com/user)</li><li>- A web page (http://www.domain.com)</li><li>- An email address (joe@dalton.com)</li></ul>",
      "IMAGE_RESIZE_FAILED": "Error while resizing picture"
    },
    "INFO": {
      "PROFILE_REMOVED": "Profile deleted",
      "PROFILE_SAVED": "Profile saved"
    },
    "HELP": {
      "WARNING_PUBLIC_DATA": "Please note that the information published here <b>is public</b>: visible including by <b>not logged in people</b>.<br/>{{'PROFILE.PROFILE_DIVIDER_HELP'|translate}}"
    }
  },
  "LOCATION": {
    "BTN_GEOLOC_ADDRESS": "Find my address on the map",
    "USE_GEO_POINT": "Appear on {{'COMMON.APP_NAME'|translate}} maps?",
    "LOADING_LOCATION": "Searching address...",
    "LOCATION_DIVIDER": "Localisation",
    "ADDRESS": "Address",
    "ADDRESS_HELP": "Address (optional)",
    "CITY": "City",
    "CITY_HELP": "City, Country",
    "DISTANCE": "Maximum distance around the city",
    "DISTANCE_UNIT": "mi",
    "DISTANCE_OPTION": "{{value}} {{'LOCATION.DISTANCE_UNIT'|translate}}",
    "SEARCH_HELP": "City, Country",
    "PROFILE_POSITION": "Profile position",
    "MODAL": {
      "TITLE": "Search address",
      "SEARCH_HELP": "City, Country",
      "ALTERNATIVE_RESULT_DIVIDER": "Alternative results for <b>{{address}}</b>:",
      "POSITION": "lat/lon : {{lat}} / {{lon}}"
    },
    "ERROR": {
      "CITY_REQUIRED_IF_STREET": "Required if a street has been filled",
      "REQUIRED_FOR_LOCATION": "Required field to appear on the map",
      "INVALID_FOR_LOCATION": "Unknown address",
      "GEO_LOCATION_FAILED": "Unable to retrieve your current position. Please use the search button.",
      "ADDRESS_LOCATION_FAILED": "Unable to retrieve the address position"
    }
  },
  "SUBSCRIPTION": {
    "SUBSCRIPTION_DIVIDER": "Online services",
    "SUBSCRIPTION_DIVIDER_HELP": "Online services offer optional additional services, delegated to a third party.",
    "BTN_ADD": "Add a service",
    "BTN_EDIT": "Manage my services",
    "NO_SUBSCRIPTION": "No service defined",
    "SUBSCRIPTION_COUNT": "Services / Subscription",
    "EDIT": {
      "TITLE": "Online services",
      "HELP_TEXT": "Manage your subscriptions and other online services here",
      "PROVIDER": "Provider:"
    },
    "TYPE": {
      "ENUM": {
        "EMAIL": "Receive email notifications"
      }
    },
    "CONFIRM": {
      "DELETE_SUBSCRIPTION": "Are you sur you want to <b>delete this subscription</b>?"
    },
    "ERROR": {
      "LOAD_SUBSCRIPTIONS_FAILED": "Error while loading online services",
      "ADD_SUBSCRIPTION_FAILED": "Error while adding subscription",
      "UPDATE_SUBSCRIPTION_FAILED": "Error during subscription update",
      "DELETE_SUBSCRIPTION_FAILED": "Error while deleting subscription"
    },
    "MODAL_EMAIL": {
      "TITLE" : "Notification by email",
      "HELP" : "Fill out this form to <b>be notified by email</ b> of your account's events. <br/>Your email address will be encrypted only to be visible to the service provider.",
      "EMAIL_LABEL" : "Your email:",
      "EMAIL_HELP": "john@domain.com",
      "FREQUENCY_LABEL": "Frequency of notifications:",
      "FREQUENCY_DAILY": "Daily",
      "FREQUENCY_WEEKLY": "Weekly",
      "PROVIDER": "Service Provider:"
    }
  },
  "DOCUMENT": {
    "HASH": "Hash: ",
    "LOOKUP": {
      "TITLE": "Document search",
      "BTN_ACTIONS": "Actions",
      "SEARCH_HELP": "issuer:AAA*, time:1508406169",
      "LAST_DOCUMENTS_DOTS": "Last documents:",
      "LAST_DOCUMENTS": "Last documents",
      "SHOW_QUERY": "Show query",
      "HIDE_QUERY": "Hide query",
      "HEADER_TIME": "Time/Hour",
      "HEADER_ISSUER": "Issuer",
      "HEADER_RECIPIENT": "Recipient",
      "HEADER_AMOUNT": "Amount",
      "READ": "Read",
      "BTN_REMOVE": "Delete this document",
      "BTN_COMPACT": "Compact",
      "HAS_CREATE_OR_UPDATE_PROFILE": "create or edit his profile",
      "POPOVER_ACTIONS": {
        "TITLE": "Actions",
        "REMOVE_ALL": "Delete these documents..."
      }
    },
    "INFO": {
      "REMOVED": "Deleted document"
    },
    "CONFIRM": {
      "REMOVE": "Are you sure you want to <b>delete this document</b>?",
      "REMOVE_ALL": "Are you sure you want to <b>delete these documents</b>?"
    },
    "ERROR": {
      "LOAD_DOCUMENTS_FAILED": "Error searching documents",
      "REMOVE_FAILED": "Error deleting the document",
      "REMOVE_ALL_FAILED": "Error deleting documents"
    }
  },
  "ES_SETTINGS": {
    "PLUGIN_NAME": "Cesium+",
    "PLUGIN_NAME_HELP": "User profiles, notifications, private messages",
    "ENABLE_TOGGLE": "Enable extension?",
    "ENABLE_REMOTE_STORAGE": "Enable remote storage for settings?",
    "ENABLE_REMOTE_STORAGE_HELP": "Enables (encrypted) storage of your settings on Cesium + nodes",
    "ENABLE_MESSAGE_TOGGLE": "Enable private messages?",
    "PEER": "Data peer address",
    "POPUP_PEER": {
      "TITLE" : "Data peer",
      "HELP" : "Set the address of the peer to use:",
      "PEER_HELP": "server.domain.com:port"
    },
    "NOTIFICATIONS": {
      "DIVIDER": "Notifications",
      "HELP_TEXT": "Enable the types of notifications you want to receive:",
      "ENABLE_TX_SENT": "Notify the validation of <b>sent payments</b>?",
      "ENABLE_TX_RECEIVED": "Notify the validation of <b>received payments</b>?",
      "ENABLE_CERT_SENT": "Notify the validation of <b>sent certifications</b>?",
      "ENABLE_CERT_RECEIVED": "Notify the validation of <b>received certifications</b>?",
      "ENABLE_HTML5_NOTIFICATION": "Warn with each new notification?",
      "ENABLE_HTML5_NOTIFICATION_HELP": "Opens a small popup window with each new notification."
    },
    "CONFIRM": {
      "ASK_ENABLE_TITLE": "Optional features",
      "ASK_ENABLE": "Cesium+ extension is <b>disabled</b> in your settings, making some features inactive: <ul><li>&nbsp;&nbsp;<b><i class=\"icon ion-person\"></i> user profiles</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-android-notifications\"></i> Notifications</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-email\"></i> Private messages</b>.</ul><br/><br/><b>Do you want to enable</b> the extension?"
    }
  },
  "ES_WALLET": {
    "ERROR": {
      "RECIPIENT_IS_MANDATORY": "A recipient is required for encryption."
    },
    "ES_PEER": {
      "NAME": "Name",
      "DOCUMENTS": "Documents",
      "SOFTWARE": "Software",
      "DOCUMENT_COUNT": "Number of documents",
      "EMAIL_SUBSCRIPTION_COUNT": "{{emailSubscription}} subscribers to email notification"
    }
  },
  "EVENT": {
    "NODE_STARTED": "Your node ES API <b>{{params[0]}}</b> is UP",
    "NODE_BMA_DOWN": "Node <b>{{params[0]}}:{{params[1]}}</b> (used by your ES API) is <b>unreachable</b>.",
    "NODE_BMA_UP": "Node <b>{{params[0]}}:{{params[1]}}</b> is reachable again.",
    "MEMBER_JOIN": "You are now a <b>member</b> of currency <b>{{params[0]}}</b>!",
    "MEMBER_LEAVE": "You are <b>not a member anymore</b> of currency <b>{{params[0]}}</b>!",
    "MEMBER_EXCLUDE": "You are <b>not more member</b> of the currency <b>{{params[0]}}</b>, for lack of renewal or lack of certifications.",
    "MEMBER_REVOKE": "Your account has been revoked. It will no longer be a member of the currency <b>{{params[0]}}</b>.",
    "MEMBER_ACTIVE": "Your membership to <b>{{params[0]}}</b> has been <b>renewed successfully</b>.",
    "TX_SENT": "Your payment to <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> was executed.",
    "TX_SENT_MULTI": "Your payment to <b>{{params[1]}}</b> was executed.",
    "TX_RECEIVED": "You received a payment from <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "TX_RECEIVED_MULTI": "You received a payment from <b>{{params[1]}}</b>.",
    "CERT_SENT": "Your <b>certification</b> to <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> was executed.",
    "CERT_RECEIVED": "You  have <b>received a certification</b> from <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "USER": {
      "LIKE_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> like your profile",
      "FOLLOW_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> follows your activity",
      "STAR_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> rated you ({{params[3]}} <i class=\"ion-star\">)",
      "MODERATION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> asks you for a moderation on the profile: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "DELETION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> reported a profile to be deleted: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "ABUSE_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has requested moderation on your profile"
    },
    "PAGE": {
      "NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has commented on your referencing: <b>{{params[2]}}</b>",
      "UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has modified his comment on your referencing: <b>{{params[2]}}</b>",
      "NEW_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has replied to your comment on the referencing: <b>{{params[2]}}</b>",
      "UPDATE_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has modified his answer to your comment, on the referencing: <b>{{params[2]}}</b>",
      "FOLLOW_NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has commented on the page: <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has modified his comment on the page: <b>{{params[2]}}</b>",
      "FOLLOW_NEW": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> added a page: <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> updated the page: <b>{{params[2]}}</b>",
      "MODERATION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> asks you for a moderation on the page: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "DELETION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> reported a page to be deleted: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "ABUSE_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> has requested moderation on your page: <b>{{params[2]}}</b>"
    }
  },
  "LIKE": {
    "ERROR": {
      "FAILED_TOGGLE_LIKE": "Unable to execute this action."
    }
  },
  "CONFIRM": {
    "ES_USE_FALLBACK_NODE": "Data node <b>{{old}}</b> unreachable or invalid address.<br/><br/>Do you want to temporarily use the data node <b>{{new}}</b>?"
  },
  "ERROR": {
    "ES_CONNECTION_ERROR": "Data node <b>{{server}}</b> unreachable or invalid address.<br/><br/>Cesium will continue to work, <b>without the Cesium+</b> extension (user profiles, private messages, maps and graphics).<br/><br/>Check your Internet connection, or change data node in <a class=\"positive\" ng-click=\"doQuickFix('settings')\">extension settings</a>.",
    "ES_MAX_UPLOAD_BODY_SIZE": "The volume of data to be sent exceeds the limit set by the server.<br/><br/>Please try again after, for example, deleting photos."
  }
}
);

$translateProvider.translations("eo-EO", {
  "COMMON": {
    "CATEGORY": "Kategorio",
    "CATEGORIES": "Kategorioj",
    "CATEGORY_SEARCH_HELP": "Serĉado",
    "COMMENT_HELP": "Komento",
    "LAST_MODIFICATION_DATE": "Ĝisdatigita la",
    "SUBMIT_BY": "Submetita de",
    "BTN_LIKE": "Mi ŝatas",
    "BTN_LIKE_REMOVE": "Mi ne plu ŝatas",
    "LIKES_TEXT": "{{total}} persono{{total > 1 ? 'j' : ''}} ŝatis tiun ĉi paĝon",
    "ABUSES_TEXT": "{{total}} persono{{total > 1 ? 'j' : ''}} atentigis pri problemo",
    "BTN_REPORT_ABUSE_DOTS": "Atentigi pri problemo aŭ misuzo...",
    "BTN_REMOVE_REPORTED_ABUSE": "Nuligi mian atentigon",
    "BTN_PUBLISH": "Publikigi",
    "BTN_PICTURE_DELETE": "Forigi",
    "BTN_PICTURE_FAVORISE": "Precipa",
    "BTN_PICTURE_ROTATE": "Turni",
    "BTN_ADD_PICTURE": "Aldoni foton",
    "NOTIFICATION": {
      "TITLE": "Nova avizo | {{'COMMON.APP_NAME'|translate}}",
      "HAS_UNREAD": "Vi havas {{count}} avizo{{count>0?'j':''}}n ne legita{{count>0?'j':''}}n"
    },
    "NOTIFICATIONS": {
      "TITLE": "Avizoj",
      "MARK_ALL_AS_READ": "Ĉion marki legita",
      "NO_RESULT": "Neniu avizo",
      "SHOW_ALL": "Vidi ĉion",
      "LOAD_NOTIFICATIONS_FAILED": "Malsukceso por ŝarĝi la avizojn"
    },
    "REPORT_ABUSE": {
      "TITLE": "Atentigi pri problemo",
      "SUB_TITLE": "Bonvolu klarigi rapide la problemon:",
      "REASON_HELP": "Mi klarigas la problemon...",
      "ASK_DELETE": "Peti la forigon?",
      "CONFIRM": {
        "SENT": "Atentigo sendita. Dankon!"
      }
    }
  },
  "MENU": {
    "REGISTRY": "Paĝoj",
    "USER_PROFILE": "Mia profilo",
    "MESSAGES": "Mesaĝoj",
    "NOTIFICATIONS": "Avizoj",
    "INVITATIONS": "Invitoj"
  },
  "ACCOUNT": {
    "NEW": {
      "ORGANIZATION_ACCOUNT": "Konto por organizaĵo",
      "ORGANIZATION_ACCOUNT_HELP": "Se vi reprezentas entreprenon, asocion, ktp.<br/>Neniu universala dividendo estos kreita per tiu ĉi konto."
    },
    "EVENT": {
      "MEMBER_WITHOUT_PROFILE": "Vi povas <a ui-sref=\"app.edit_profile\">tajpi vian profilon Cesium+</a> (kromebleco) por disponi pli bonan videblecon por via konto."
    },
    "ERROR": {
      "WS_CONNECTION_FAILED": "Cesium ne povas ricevi la avizojn pro teknika eraro (konekto al la daten-nodo Cesium+).<br/><br/>Se la problemo daŭradas, bonvolu <b>elekti alian daten-nodon</b> ĉe la parametroj Cesium+."
    }
  },
  "WOT": {
    "BTN_SUGGEST_CERTIFICATIONS_DOTS": "Sugesti identecojn atestotajn...",
    "BTN_ASK_CERTIFICATIONS_DOTS": "Peti membrojn atesti min...",
    "BTN_ASK_CERTIFICATION": "Peti atestaĵon",
    "SUGGEST_CERTIFICATIONS_MODAL": {
      "TITLE": "Sugesti atestadojn",
      "HELP": "Elekti viajn sugestojn"
    },
    "ASK_CERTIFICATIONS_MODAL": {
      "TITLE": "Peti atestaĵojn",
      "HELP": "Elekti la ricevontojn"
    },
    "SEARCH": {
      "DIVIDER_PROFILE": "Kontoj",
      "DIVIDER_PAGE": "Paĝoj",
      "DIVIDER_GROUP": "Grupoj"
    },
    "CONFIRM": {
      "SUGGEST_CERTIFICATIONS": "Ĉu vi certas, ke vi volas <b>sendi tiujn sugestojn por atestado</b>?",
      "ASK_CERTIFICATION": "Ĉu vi certas, ke vi volas <b>sendi atesto-peton</b>?",
      "ASK_CERTIFICATIONS": "Ĉu vi certas, ke vi volas <b>sendi atesto-peton</b> al tiuj personoj?"
    }
  },
  "INVITATION": {
    "TITLE": "Invitoj",
    "NO_RESULT": "Neniu invito atendanta",
    "BTN_DELETE_ALL": "Forigi ĉiujn invitojn",
    "BTN_DELETE": "Forigi la inviton",
    "BTN_NEW_INVITATION": "Nova invito",
    "ASK_CERTIFICATION": "<a href=\"#/app/wot/{{pubkey}}/{{::uid}}\">{{::name||uid}}</a> petas vian atestadon",
    "SUGGESTION_CERTIFICATION": "<a href=\"#/app/wot/{{::pubkey}}/{{::uid}}\">{{::name||uid}}</a> estas sugestita al vi por atestado",
    "SUGGESTED_BY": "Sugesto sendita de <a class=\"positive\" href=\"#/app/wot/{{::issuer.pubkey}}/{{::issuer.uid}}\">{{::issuer.name||issuer.uid}}</a>",
    "NOTIFICATIONS": {
      "TITLE": "Invitoj"
    },
    "LIST": {
      "TITLE": "Invitoj"
    },
    "NEW": {
      "TITLE": "Nova invito",
      "RECIPIENTS": "Al",
      "RECIPIENTS_HELP": "Ricevontoj de la invito",
      "RECIPIENTS_MODAL_TITLE": "Ricevontoj",
      "RECIPIENTS_MODAL_HELP": "Bonvolu elekti la ricevontojn:",
      "SUGGESTION_IDENTITIES": "Sugestoj por atestado",
      "SUGGESTION_IDENTITIES_HELP": "Atestadoj sugestotaj",
      "SUGGESTION_IDENTITIES_MODAL_TITLE": "Sugestoj",
      "SUGGESTION_IDENTITIES_MODAL_HELP": "Bonvolu elekti viajn sugestojn:"
    },
    "CONFIRM": {
      "DELETE_ALL_CONFIRMATION": "La forigo de la invitoj estas <b>neinversigebla ago</b>.<br/><br/><b>Ĉu vi certas</b>, ke vi volas daŭrigi?",
      "SEND_INVITATIONS_TO_CERTIFY": "Ĉu vi certas, ke vi volas <b>sendi tiun inviton atestotan</b>?"
    },
    "INFO": {
      "INVITATION_SENT": "Invito sendita"
    },
    "ERROR": {
      "LOAD_INVITATIONS_FAILED": "Malsukceso por ŝarĝi la invitojn",
      "REMOVE_INVITATION_FAILED": "Eraro dum la forigo de la invito",
      "REMOVE_ALL_INVITATIONS_FAILED": "Eraro dum la forigo de la invitoj",
      "SEND_INVITATION_FAILED": "Eraro dum la sendo de la invito",
      "BAD_INVITATION_FORMAT": "<span class=\"assertive\"><i class=\"ion-close-circled\"></i> Invito nelegebla (strukturo nekonata)</span> - sendita de <a ui-sref=\"app.wot_identity({pubkey: '{{::pubkey}}', uid: '{{::uid}}' })\">{{::name||uid}}</a>"
    }
  },
  "COMMENTS": {
    "DIVIDER": "Komentoj",
    "SHOW_MORE_COMMENTS": "Afiŝi la antaŭajn komentojn",
    "COMMENT_HELP": "Via komento, demando, ktp.",
    "COMMENT_HELP_REPLY_TO": "Via respondo...",
    "BTN_SEND": "Sendi",
    "POPOVER_SHARE_TITLE": "Mesaĝo #{{number}}",
    "REPLY": "Respondi",
    "REPLY_TO": "Respondo al:",
    "REPLY_TO_LINK": "Responde al ",
    "REPLY_TO_DELETED_COMMENT": "Responde al forigita komento",
    "REPLY_COUNT": "{{replyCount}} respondoj",
    "DELETED_COMMENT": "Komento forigita",
    "MODIFIED_ON": "modifita la {{time|formatDate}}",
    "MODIFIED_PARENTHESIS": "(modifita poste)",
    "ERROR": {
      "FAILED_SAVE_COMMENT": "Eraro dum la konservo de la komento",
      "FAILED_REMOVE_COMMENT": "Eraro dum la forigo de la komento"
    }
  },
  "MESSAGE": {
    "REPLY_TITLE_PREFIX": "Resp: ",
    "FORWARD_TITLE_PREFIX": "Tr: ",
    "BTN_REPLY": "Respondi",
    "BTN_COMPOSE": "Nova mesaĝo",
    "BTN_WRITE": "Skribi",
    "NO_MESSAGE_INBOX": "Neniu mesaĝo ricevita",
    "NO_MESSAGE_OUTBOX": "Neniu mesaĝo sendita",
    "NOTIFICATIONS": {
      "TITLE": "Mesaĝoj",
      "MESSAGE_RECEIVED": "Vi <b>ricevis mesaĝon</b><br/>de"
    },
    "LIST": {
      "INBOX": "Ricevujo",
      "OUTBOX": "Senditaj mesaĝoj",
      "LAST_INBOX": "Novaj mesaĝoj",
      "LAST_OUTBOX": "Senditaj mesaĝoj",
      "BTN_LAST_MESSAGES": "Freŝdataj mesaĝoj",
      "TITLE": "Mesaĝoj",
      "SEARCH_HELP": "Serĉado en la mesaĝoj",
      "POPOVER_ACTIONS": {
        "TITLE": "Kromaĵoj",
        "DELETE_ALL": "Forigi ĉiujn mesaĝojn"
      }
    },
    "COMPOSE": {
      "TITLE": "Nova mesaĝo",
      "TITLE_REPLY": "Respondi",
      "SUB_TITLE": "Nova mesaĝo",
      "TO": "Al",
      "OBJECT": "Temo",
      "OBJECT_HELP": "Temo",
      "ENCRYPTED_HELP": "Bonvolu noti, ke tiu ĉi mesaĝo estos ĉifrita antaŭ sendo, tiel ke nur la adresato povos legi ĝin, kaj ke li estos certa, ke vi ja estas ties aŭtoro.",
      "MESSAGE": "Mesaĝo",
      "MESSAGE_HELP": "Enhavo de la mesaĝo",
      "CONTENT_CONFIRMATION": "La enhavo de la mesaĝo estas malplena.<br/><br/>Ĉu vi volas tamen sendi la mesaĝon?"
    },
    "VIEW": {
      "TITLE": "Mesaĝo",
      "SENDER": "Sendita de",
      "RECIPIENT": "Sendita al",
      "NO_CONTENT": "Mesaĝo malplena",
      "DELETE": "Forigi la mesaĝon"
    },
    "CONFIRM": {
      "REMOVE": "Ĉu vi certas, ke vi volas <b>forigi tiun ĉi mesaĝon</b>?<br/><br/>Tiu ago estas neinversigebla.",
      "REMOVE_ALL" : "Ĉu vi certas, ke vi volas <b>forigi ĉiujn mesaĝojn</b>?<br/><br/>Tiu ago estas neinversigebla.",
      "MARK_ALL_AS_READ": "Ĉu vi certas, ke vi volas <b>marki ĉiujn mesaĝojn legitaj</b>?",
      "USER_HAS_NO_PROFILE": "Tiu identeco havas neniun profilon Cesium+. Eblas ke ĝi ne uzas la krom-programon Cesium+, kaj <b>do ne legos vian mesaĝon</b>.<br/><br/>Ĉu vi certas, ke vi volas tamen <b>daŭrigi</b>?"
    },
    "INFO": {
      "MESSAGE_REMOVED": "Mesaĝo forigita",
      "All_MESSAGE_REMOVED": "Ĉiuj mesaĝoj estis forigitaj",
      "MESSAGE_SENT": "Mesaĝo sendita"
    },
    "ERROR": {
      "SEND_MSG_FAILED": "Eraro dum la sendo de la mesaĝo.",
      "LOAD_MESSAGES_FAILED": "Eraro dum la ricevo de la mesaĝoj.",
      "LOAD_MESSAGE_FAILED": "Eraro dum la ricevo de la mesaĝo.",
      "MESSAGE_NOT_READABLE": "Legado de la mesaĝo neebla.",
      "USER_NOT_RECIPIENT": "Vi ne estas la adresato de tiu ĉi mesaĝo: malĉifrado neebla.",
      "NOT_AUTHENTICATED_MESSAGE": "La aŭtenteco de la mesaĝo estas dubinda aŭ ties enhavo estas difektita.",
      "REMOVE_MESSAGE_FAILED": "Malsukceso por forigi la mesaĝon",
      "MESSAGE_CONTENT_TOO_LONG": "Signaro tro longa ({{maxLength}} signoj maksimume).",
      "MARK_AS_READ_FAILED": "Neeblas marki la mesaĝon 'legita'.",
      "LOAD_NOTIFICATIONS_FAILED": "Eraro dum la ricevo de la mesaĝo-avizoj.",
      "REMOVE_All_MESSAGES_FAILED": "Eraro dum la forigo de ĉiuj mesaĝoj.",
      "MARK_ALL_AS_READ_FAILED": "Eraro por marki la mesaĝojn legitaj.",
      "RECIPIENT_IS_MANDATORY": "La adresato estas deviga."
    }
  },
  "BLOCKCHAIN": {
    "LOOKUP": {
      "SEARCH_HELP": "Numero de bloko, haketo, publika ŝlosilo, ktp.",
      "POPOVER_FILTER_TITLE": "Filtriloj",
      "HEADER_MEDIAN_TIME": "Dato / Horo",
      "HEADER_BLOCK": "Bloko #",
      "HEADER_ISSUER": "Nodo elsendinta",
      "BTN_LAST": "Lastaj blokoj",
      "BTN_TX": "Spezoj",
      "DISPLAY_QUERY": "Afiŝi la informpeton",
      "HIDE_QUERY": "Kaŝi la informpeton",
      "TX_SEARCH_FILTER": {
        "MEMBER_FLOWS": "<b class=\"ion-person\"></b> Eniroj/eliroj de membroj",
        "EXISTING_TRANSACTION": "<b class=\"ion-card\"></b> Kun spezoj",
        "PERIOD": "<b class=\"ion-clock\"></b> Inter <b class=\"gray\">{{params[1]|medianDateShort}}</b> ({{params[1]|medianTime}}) kaj <b class=\"gray\">{{params[2]|medianDateShort}}</b> ({{params[2]|medianTime}})",
        "ISSUER": "<b class=\"ion-android-desktop\"></b> Kalkulita de <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}",
        "TX_PUBKEY": "<b class=\"ion-card\"></b> Spezoj koncernantaj <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}"
      }
    },
    "ERROR": {
      "SEARCH_BLOCKS_FAILED": "Eraro dum la serĉado de la blokoj."
    }
  },
  "GROUP": {
    "GENERAL_DIVIDER": "Ĝeneralaj informoj",
    "LOCATION_DIVIDER": "Adreso",
    "SOCIAL_NETWORKS_DIVIDER": "Sociaj retoj kaj retejo",
    "TECHNICAL_DIVIDER": "Teknikaj informoj",
    "CREATED_TIME": "Kreita {{creationTime|formatFromNow}}",
    "NOTIFICATIONS": {
      "TITLE": "Invitoj"
    },
    "LOOKUP": {
      "TITLE": "Grupoj",
      "SEARCH_HELP": "Nomo de grupo, vortoj, loko, ktp.",
      "LAST_RESULTS_LIST": "Novaj grupoj:",
      "OPEN_RESULTS_LIST": "Malfermitaj grupoj:",
      "MANAGED_RESULTS_LIST": "Fermitaj grupoj:",
      "BTN_LAST": "Novaj grupoj",
      "BTN_NEW": "Mi aldonas grupon"
    },
    "TYPE": {
      "TITLE": "Nova grupo",
      "SELECT_TYPE": "Tipo de grupo:",
      "OPEN_GROUP": "Malfermita grupo",
      "OPEN_GROUP_HELP": "Malfermita grupo estas alirebla de iu ajn membro de la mono.",
      "MANAGED_GROUP": "Administrita grupo",
      "MANAGED_GROUP_HELP": "Administrita grupo estas mastrumata de administrantoj kaj moderigantoj, kiuj povas akcepti, rifuzi aŭ eksigi membron.",
      "ENUM": {
        "OPEN": "Malfermita grupo",
        "MANAGED": "Administrita grupo"
      }
    },
    "VIEW": {
      "POPOVER_SHARE_TITLE": "{{title}}",
      "MENU_TITLE": "Kromaĵoj",
      "REMOVE_CONFIRMATION" : "Ĉu vi certas, ke vi volas forigi tiun ĉi grupon?<br/><br/>Tiu ago estas neinversigebla."
    },
    "EDIT": {
      "TITLE": "Grupo",
      "TITLE_NEW": "Nova grupo",
      "RECORD_TITLE": "Titolo",
      "RECORD_TITLE_HELP": "Titolo",
      "RECORD_DESCRIPTION": "Priskribo",
      "RECORD_DESCRIPTION_HELP": "Priskribo"
    },
    "ERROR": {
      "SEARCH_GROUPS_FAILED": "Malsukceso por traserĉi grupojn",
      "REMOVE_RECORD_FAILED": "Malsukceso por forigi la grupon"
    },
    "INFO": {
      "RECORD_REMOVED" : "Grupo forigita"
    }
  },
  "REGISTRY": {
    "CATEGORY": "Ĉefa agado",
    "GENERAL_DIVIDER": "Ĝeneralaj informoj",
    "LOCATION_DIVIDER": "Adreso",
    "SOCIAL_NETWORKS_DIVIDER": "Sociaj retoj kaj retejo",
    "TECHNICAL_DIVIDER": "Teknikaj informoj",
    "BTN_SHOW_WOT": "Personoj",
    "BTN_SHOW_WOT_HELP": "Traserĉi personojn",
    "BTN_SHOW_PAGES": "Paĝoj",
    "BTN_SHOW_PAGES_HELP": "Traserĉi paĝojn",
    "BTN_NEW": "Krei paĝon",
    "MY_PAGES": "Miaj paĝoj",
    "NO_PAGE": "Neniu paĝo",
    "SEARCH": {
      "TITLE": "Paĝoj",
      "SEARCH_HELP": "Kio, Kiu: restoracio, Ĉe Marcelo, ...",
      "BTN_ADD": "Nova",
      "BTN_LAST_RECORDS": "Freŝdataj paĝoj",
      "BTN_ADVANCED_SEARCH": "Sperta serĉado",
      "BTN_OPTIONS": "Sperta serĉado",
      "TYPE": "Tipo de paĝo",
      "LOCATION_HELP": "Kie: Poŝto-kodo, Urbo",
      "RESULTS": "Rezultoj",
      "RESULT_COUNT_LOCATION": "{{count}} rezulto{{count>0?'j':''}}, proksime de {{location}}",
      "RESULT_COUNT": "{{count}} rezulto{{count>0?'j':''}}",
      "LAST_RECORDS": "Freŝdataj paĝoj",
      "LAST_RECORD_COUNT_LOCATION": "{{count}} paĝo{{count>0?'j':''}} freŝdata{{count>0?'j':''}}, proksime de {{location}}",
      "LAST_RECORD_COUNT": "{{count}} paĝo{{count>0?'j':''}} freŝdata{{count>0?'j':''}}",
      "POPOVER_FILTERS": {
        "BTN_ADVANCED_SEARCH": "Spertaj kromaĵoj?"
      }
    },
    "VIEW": {
      "TITLE": "Adresaro",
      "CATEGORY": "Ĉefa agado:",
      "LOCATION": "Adreso:",
      "MENU_TITLE": "Kromaĵoj",
      "POPOVER_SHARE_TITLE": "{{title}}",
      "REMOVE_CONFIRMATION" : "Ĉu vi certas, ke vi volas forigi tiun ĉi paĝon?<br/><br/>Tiu ago estas neinversigebla."
    },
    "TYPE": {
      "TITLE": "Tipoj",
      "SELECT_TYPE": "Tipo de paĝo:",
      "ENUM": {
        "SHOP": "Loka komerco",
        "COMPANY": "Entrepreno",
        "ASSOCIATION": "Asocio",
        "INSTITUTION": "Institucio"
      }
    },
    "EDIT": {
      "TITLE": "Redaktado",
      "TITLE_NEW": "Nova paĝo",
      "RECORD_TYPE":"Tipo de paĝo",
      "RECORD_TITLE": "Nomo",
      "RECORD_TITLE_HELP": "Nomo",
      "RECORD_DESCRIPTION": "Priskribo",
      "RECORD_DESCRIPTION_HELP": "Priskribo de la agado",
      "RECORD_ADDRESS": "Strato",
      "RECORD_ADDRESS_HELP": "Strato, konstruaĵo...",
      "RECORD_CITY": "Urbo",
      "RECORD_CITY_HELP": "Urbo",
      "RECORD_SOCIAL_NETWORKS": "Sociaj retoj kaj retejo",
      "RECORD_PUBKEY": "Publika ŝlosilo",
      "RECORD_PUBKEY_HELP": "Publika ŝlosilo por ricevi la pagojn"
    },
    "WALLET": {
      "PAGE_DIVIDER": "Paĝoj",
      "PAGE_DIVIDER_HELP": "La paĝoj listigas agadojn, kiuj akceptas la liberan monon aŭ helpas ĝin: komercoj, entreprenoj, asocioj, institucioj."
    },
    "ERROR": {
      "LOAD_CATEGORY_FAILED": "Malsukceso por ŝarĝi la liston de la agadoj",
      "LOAD_RECORD_FAILED": "Eraro dum la ŝarĝado de la paĝo",
      "LOOKUP_RECORDS_FAILED": "Eraro dum la serĉado",
      "REMOVE_RECORD_FAILED": "Malsukceso por forigi la paĝon",
      "SAVE_RECORD_FAILED": "Eraro dum la konservado",
      "RECORD_NOT_EXISTS": "Paĝo neekzistanta",
      "GEO_LOCATION_NOT_FOUND": "Urbo aŭ poŝto-kodo ne trovita"
    },
    "INFO": {
      "RECORD_REMOVED" : "Paĝo forigita",
      "RECORD_SAVED": "Paĝo konservita"
    }
  },
  "PROFILE": {
    "PROFILE_DIVIDER": "Profilo Cesium+",
    "PROFILE_DIVIDER_HELP": "Temas pri kromaj datenoj, stokitaj ekster la mon-reto.",
    "NO_PROFILE_DEFINED": "Neniu profilo tajpita",
    "BTN_ADD": "Tajpi mian profilon",
    "BTN_EDIT": "Redakti mian profilon",
    "BTN_DELETE": "Forigi mian profilon",
    "BTN_REORDER": "Reordigi",
    "UID": "Pseŭdonimo",
    "TITLE": "Familia nomo, Persona nomo",
    "TITLE_HELP": "Familia nomo, Persona nomo",
    "DESCRIPTION": "Pri mi",
    "DESCRIPTION_HELP": "Pri mi...",
    "SOCIAL_HELP": "http://...",
    "GENERAL_DIVIDER": "Ĝeneralaj informoj",
    "SOCIAL_NETWORKS_DIVIDER": "Sociaj retoj, retejoj",
    "TECHNICAL_DIVIDER": "Teknikaj informoj",
    "MODAL_AVATAR": {
      "TITLE": "Profil-foto",
      "SELECT_FILE_HELP": "Bonvolu <b>elekti bildo-dosieron</b>:",
      "BTN_SELECT_FILE": "Elekti foton",
      "RESIZE_HELP": "<b>Rekadri la bildon</b>, laŭbezone. Pluigi klakon sur la bildo ebligas movi ĝin. Alklaku la zonon malsupre maldekstre por zomi.",
      "RESULT_HELP": "<b>Jen la rezulto</b> tiel videbla ĉe via profilo:"
    },
    "CONFIRM": {
      "DELETE": "Ĉu vi certas, ke vi volas <b>forigi vian profilon Cesium+ ?</b><br/><br/>Tiu ago estas neinversigebla.",
      "DELETE_BY_MODERATOR": "Ĉu vi certas, ke vi volas <b>forigi tiun ĉi profilon Cesium+ ?</b><br/><br/>Tiu ago estas neinversigebla."
    },
    "ERROR": {
      "REMOVE_PROFILE_FAILED": "Malsukceso por forigi la profilon",
      "LOAD_PROFILE_FAILED": "Malsukceso por ŝarĝi la profilon de la uzanto.",
      "SAVE_PROFILE_FAILED": "Eraro dum la konservado",
      "DELETE_PROFILE_FAILED": "Eraro dum la forigo de la profilo",
      "INVALID_SOCIAL_NETWORK_FORMAT": "Strukturo ne rekonata: bonvolu tajpi validan adreson.<br/><br/>Ezemploj:<ul><li>- Facebook-paĝo (https://www.facebook.com/uzanto)</li><li>- Retpaĝo (http://www.miaretejo.net)</li><li>- Retadreso (joe@dalton.com)</li></ul>",
      "IMAGE_RESIZE_FAILED": "Eraro dum la reformatigo de la bildo"
    },
    "INFO": {
      "PROFILE_REMOVED": "Profilo forigita",
      "PROFILE_SAVED": "Profilo konservita"
    },
    "HELP": {
      "WARNING_PUBLIC_DATA": "La informoj afiŝitaj en via profilo <b>estas publikaj</b>: videblaj inkluzive de la personoj <b>ne konektitaj</b>.<br/>{{'PROFILE.PROFILE_DIVIDER_HELP'|translate}}"
    }
  },
  "LOCATION": {
    "BTN_GEOLOC_ADDRESS": "Trovi mian adreson surmape",
    "USE_GEO_POINT": "Aperi sur la mapoj {{'COMMON.APP_NAME'|translate}}?",
    "LOADING_LOCATION": "Serĉado de la adreso...",
    "LOCATION_DIVIDER": "Adreso",
    "ADDRESS": "Strato",
    "ADDRESS_HELP": "Strato, adres-aldonaĵo...",
    "CITY": "Urbo",
    "CITY_HELP": "Poŝto-kodo, Urbo, Lando",
    "DISTANCE": "Maksimuma distanco ĉirkaŭ la urbo",
    "DISTANCE_UNIT": "km",
    "DISTANCE_OPTION": "{{value}} {{'LOCATION.DISTANCE_UNIT'|translate}}",
    "SEARCH_HELP": "Poŝto-kodo, Urbo",
    "PROFILE_POSITION": "Loko de la profilo",
    "MODAL": {
      "TITLE": "Serĉado de la adreso",
      "SEARCH_HELP": "Urbo, Poŝto-kodo, Lando",
      "ALTERNATIVE_RESULT_DIVIDER": "Alternativaj rezultoj por <b>{{address}}</b>:",
      "POSITION": "Lat/Lon: {{lat}}/{{lon}}"
    },
    "ERROR": {
      "CITY_REQUIRED_IF_STREET": "Deviga kampo (ĉar strato estas tajpita)",
      "REQUIRED_FOR_LOCATION": "Deviga kampo por aperi sur la mapo",
      "INVALID_FOR_LOCATION": "Adreso nekonata",
      "GEO_LOCATION_FAILED": "Neeblas ricevi vian lokiĝon. Bonvolu uzi la serĉo-butonon.",
      "ADDRESS_LOCATION_FAILED": "Neeblas ricevi la lokon per la adreso"
    }
  },
  "SUBSCRIPTION": {
    "SUBSCRIPTION_DIVIDER": "Retaj servoj",
    "SUBSCRIPTION_DIVIDER_HELP": "La retaj servoj proponas pliajn nedevigajn servojn, delegitajn al aliulo.",
    "BTN_ADD": "Aldoni servon",
    "BTN_EDIT": "Mastrumi miajn servojn",
    "NO_SUBSCRIPTION": "Neniu servo uzata",
    "SUBSCRIPTION_COUNT": "Servoj / Abonoj",
    "EDIT": {
      "TITLE": "Retaj servoj",
      "HELP_TEXT": "Mastrumu ĉi tie viajn abonojn kaj aliajn retajn servojn",
      "PROVIDER": "Provizanto:"
    },
    "TYPE": {
      "ENUM": {
        "EMAIL": "Ricevi la avizojn per retmesaĝo"
      }
    },
    "CONFIRM": {
      "DELETE_SUBSCRIPTION": "Ĉu vi certas, ke vi volas <b>forigi tiun abonon</b>?"
    },
    "ERROR": {
      "LOAD_SUBSCRIPTIONS_FAILED": "Eraro dum la ŝarĝo de la retaj servoj",
      "ADD_SUBSCRIPTION_FAILED": "Malsukceso por sendi la abonon",
      "UPDATE_SUBSCRIPTION_FAILED": "Malsukceso por ĝisdatigi la abonon",
      "DELETE_SUBSCRIPTION_FAILED": "Eraro dum la forigo de la abono"
    },
    "MODAL_EMAIL": {
      "TITLE" : "Avizo per retmesaĝo",
      "HELP" : "Plenigu tiun ĉi formularon por <b>esti avizita per retmesaĝo</b> pri la okazaĵoj ĉe via konto.<br/>Via retadreso estos ĉifrita por esti videbla nur de la servo-provizanto.",
      "EMAIL_LABEL" : "Via retadreso:",
      "EMAIL_HELP": "johano.stelaro@esperanto.org",
      "FREQUENCY_LABEL": "Periodo de la avizoj:",
      "FREQUENCY_DAILY": "Ĉiutaga",
      "FREQUENCY_WEEKLY": "Ĉiusemajna",
      "PROVIDER": "Servo-provizanto:"
    }
  },
  "DOCUMENT": {
    "HASH": "Haketo: ",
    "LOOKUP": {
      "TITLE": "Serĉado de dokumentoj",
      "BTN_ACTIONS": "Agoj",
      "SEARCH_HELP": "Sendanto:AAA*, tempo:1508406169",
      "LAST_DOCUMENTS_DOTS": "Lastaj dokumentoj:",
      "LAST_DOCUMENTS": "Lastaj dokumentoj",
      "SHOW_QUERY": "Vidi la informpeton",
      "HIDE_QUERY": "Kaŝi la informpeton",
      "HEADER_TIME": "Dato/Horo",
      "HEADER_ISSUER": "Sendanto",
      "HEADER_RECIPIENT": "Ricevonto",
      "HEADER_AMOUNT": "Sumo",
      "READ": "Legita",
      "BTN_REMOVE": "Forigi tiun ĉi dokumenton",
      "BTN_COMPACT": "Densigi",
      "HAS_CREATE_OR_UPDATE_PROFILE": "kreis aŭ modifis sian profilon",
      "POPOVER_ACTIONS": {
        "TITLE": "Agoj",
        "REMOVE_ALL": "Forigi tiujn ĉi dokumentojn..."
      }
    },
    "INFO": {
      "REMOVED": "Dokumento forigita"
    },
    "CONFIRM": {
      "REMOVE": "Ĉu vi certas, ke vi volas <b>forigi tiun ĉi dokumenton</b>?",
      "REMOVE_ALL": "Ĉu vi certas, ke vi volas <b>forigi tiujn ĉi dokumentojn</b>?"
    },
    "ERROR": {
      "LOAD_DOCUMENTS_FAILED": "Eraro dum la serĉado de dokumentoj",
      "REMOVE_FAILED": "Eraro dum la forigo de la dokumento",
      "REMOVE_ALL_FAILED": "Eraro dum la forigo de la dokumentoj"
    }
  },
  "ES_SETTINGS": {
    "PLUGIN_NAME": "Cesium+",
    "PLUGIN_NAME_HELP": "Profiloj, avizoj, privataj mesaĝoj",
    "ENABLE_TOGGLE": "Aktivigi la krom-programon?",
    "ENABLE_REMOTE_STORAGE": "Aktivigi la foran stokadon?",
    "ENABLE_REMOTE_STORAGE_HELP": "Ebligas stoki (ĉifrite) viajn parametrojn ĉe la nodoj Cesium+",
    "ENABLE_MESSAGE_TOGGLE": "Aktivigi la privatajn mesaĝojn?",
    "PEER": "Adreso de la daten-nodo",
    "POPUP_PEER": {
      "TITLE" : "Daten-nodo",
      "HELP" : "Tajpu la adreson de la nodo, kiun vi volas uzi:",
      "PEER_HELP": "servo.domajno.com:port"
    },
    "NOTIFICATIONS": {
      "DIVIDER": "Avizoj",
      "HELP_TEXT": "Aktivigu la avizo-tipojn, kiujn vi deziras ricevi:",
      "ENABLE_TX_SENT": "Avizi pri la <b>senditaj pagoj</b>?",
      "ENABLE_TX_RECEIVED": "Avizi pri la <b>ricevitaj pagoj</b>?",
      "ENABLE_CERT_SENT": "Avizi pri la <b>senditaj atestaĵoj</b>?",
      "ENABLE_CERT_RECEIVED": "Avizi pri <b>la ricevitaj atestaĵoj</b>?",
      "ENABLE_HTML5_NOTIFICATION": "Anonci ĉiun novan avizon?",
      "ENABLE_HTML5_NOTIFICATION_HELP": "Malfermas fenestreton por ĉiu nova avizo."
    },
    "CONFIRM": {
      "ASK_ENABLE_TITLE": "Kromaj funkcioj",
      "ASK_ENABLE": "La krom-programo Cesium+ estas <b>malaktivigita</b> ĉe viaj parametroj, kio senaktivigas la funkciojn: <ul><li>&nbsp;&nbsp;<b><i class=\"icon ion-person\"></i> Profiloj Cesium+</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-android-notifications\"></i> Avizoj</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-email\"></i> Privataj mesaĝoj</b>.<li>&nbsp;&nbsp;<b><i class=\"icon ion-location\"></i> Mapoj, ktp.</b>.</ul><br/><b>Ĉu vi deziras reaktivigi</b> la krom-programon?"
    }
  },
  "ES_WALLET": {
    "ERROR": {
      "RECIPIENT_IS_MANDATORY": "Adresito estas deviga por la ĉifrado."
    }
  },
  "ES_PEER": {
    "NAME": "Nomo",
    "DOCUMENTS": "Dokumentoj",
    "SOFTWARE": "Programo",
    "DOCUMENT_COUNT": "Nombro de dokumentoj",
    "EMAIL_SUBSCRIPTION_COUNT": "{{emailSubscription}} abonantoj pri avizoj per retmesaĝoj"
  },
  "EVENT": {
    "NODE_STARTED": "Via nodo ES API <b>{{params[0]}}</b> ekis",
    "NODE_BMA_DOWN": "La nodo <b>{{params[0]}}:{{params[1]}}</b> (uzata de via nodo ES API) estas <b>neatingebla</b>.",
    "NODE_BMA_UP": "La nodo <b>{{params[0]}}:{{params[1]}}</b> estas denove alirebla.",
    "MEMBER_JOIN": "Vi estas nun <b>membro</b> de la mono <b>{{params[0]}}</b>!",
    "MEMBER_LEAVE": "Vi <b>ne plu estas membro</b> de la mono <b>{{params[0]}}</b>!",
    "MEMBER_EXCLUDE": "Vi <b>ne plu estas membro</b> de la mono <b>{{params[0]}}</b>, pro ne revalidiĝo aŭ pro manko da atestaĵoj.",
    "MEMBER_REVOKE": "La nuligo de via konto efektiviĝis. Ĝi ne plu povos esti membro-konto de la mono <b>{{params[0]}}</b>.",
    "MEMBER_ACTIVE": "La revalidiĝo de via aliĝo al la mono <b>{{params[0]}}</b> estis <b>ricevita</b>.",
    "TX_SENT": "Via <b>pago</b> al <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> efektiviĝis.",
    "TX_SENT_MULTI": "Via <b>pago</b> al <b>{{params[1]}}</b> efektiviĝis.",
    "TX_RECEIVED": "Vi <b>ricevis pagon</b> de <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "TX_RECEIVED_MULTI": "Vi <b>ricevis pagon</b> de <b>{{params[1]}}</b>.",
    "CERT_SENT": "Via <b>atestado</b> al <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> efektiviĝis.",
    "CERT_RECEIVED": "Vi <b>ricevis atestaĵon</b> de <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "USER": {
      "LIKE_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ŝatas vian profilon",
      "FOLLOW_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> sekvas viajn agojn",
      "STAR_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> notis vin ({{params[3]}} <b class=\"ion-star\">)",
      "MODERATION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> petas de vi moderigon pri la profilo: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "DELETION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> atentigis pri profilo foriginda: <b>{{params[2]}}</b>",
      "ABUSE_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> atentigis pri via profilo"
    },
    "PAGE": {
      "NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> komentis vian anoncon: <b>{{params[2]}}</b>",
      "UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> modifis sian komenton pri via anonco: <b>{{params[2]}}</b>",
      "NEW_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> respondis al via komento pri la anonco: <b>{{params[2]}}</b>",
      "UPDATE_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> modifis sian respondon al via komento pri la anonco: <b>{{params[2]}}</b>",
      "FOLLOW_NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> komentis la paĝon: <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> modifis sian komenton ĉe la paĝo: <b>{{params[2]}}</b>",
      "FOLLOW_NEW": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> aldonis la paĝon: <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> modifis la paĝon: <b>{{params[2]}}</b>",
      "MODERATION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> petas de vi moderigon pri la paĝo: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "DELETION_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> atentigis pri paĝo foriginda: <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "ABUSE_RECEIVED": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> atentigis pri via paĝo: <b>{{params[2]}}</b>"
    }
  },
  "LIKE": {
    "ERROR": {
      "FAILED_TOGGLE_LIKE": "Ne eblas fari tiun ĉi agon."
    }
  },
  "CONFIRM": {
    "ES_USE_FALLBACK_NODE": "Daten-nodo <b>{{old}}</b> neatingebla aŭ adreso nevalida.<br/><br/>Ĉu vi volas provizore uzi la daten-nodon <b>{{new}}</b> ?"
  },
  "ERROR": {
    "ES_CONNECTION_ERROR": "Daten-nodo <b>{{server}}</b> neatingebla aŭ adreso nevalida.<br/><br/>Cesium daŭrigos funkcii, <b>sen la krom-programo Cesium+</b> (uzanto-profiloj, privataj mesaĝoj, mapoj kaj grafikoj)<br/><br/>Kontrolu vian ret-konekton, aŭ ŝanĝu daten-nodon ĉe la <a class=\"positive\" ng-click=\"doQuickFix('settings')\">parametroj de la krom-programo</a>.",
    "ES_MAX_UPLOAD_BODY_SIZE": "La kvanto de datenoj sendotaj superas la limon fiksitan de la servilo.<br/>Bonvolu reprovi post, ekzemple, forigo de fotoj."
  }
}
);

$translateProvider.translations("es-ES", {
  "COMMON": {
    "ABUSES_TEXT": "{{total}} personne{{total > 1 ? 's' : ''}} {{total > 1 ? 'ont' : 'a'}} signalé un problème",
    "BTN_LIKE": "Me gusta",
    "BTN_LIKE_REMOVE": "Dejar de gustarme",
    "BTN_REMOVE_REPORTED_ABUSE": "Cancelar mi reporte",
    "BTN_REPORT_ABUSE_DOTS": "Reportar un problema o un abuso...",
    "COMMENT_HELP": "Comentario",
    "LIKES_TEXT": "A {{total}} persona{{total > 1 ? 's' : ''}} {{total > 1 ? 'les' : 'le'}} gusta esta página",
    "NOTIFICATION": {
        "HAS_UNREAD": "Tiene {{count}} notificaci{{count>0?'ones':'ón'}} no leída{{count>0?'s':''}}",
        "TITLE": "Nueva notificación | {{'COMMON.APP_NAME'|translate}}"
    },
    "REPORT_ABUSE": {
      "ASK_DELETE": "¿ Solicitar la eliminación ?",
      "CONFIRM": {
          "SENT": "Reporte enviado. ¡ Gracias !"
      },
      "REASON_HELP": "Yo explico el problema...",
      "SUB_TITLE": "Por favor explique brevemente el problema :",
      "TITLE": "Señalar un problema"
    },

    "CATEGORY": "Categoría",
    "CATEGORIES": "Categorías",
    "CATEGORY_SEARCH_HELP": "Búsqueda",
    "LAST_MODIFICATION_DATE": "Actualización el",
    "SUBMIT_BY": "Enviado por",
    "BTN_PUBLISH": "Publicar",
    "BTN_PICTURE_DELETE": "Suprimir",
    "BTN_PICTURE_FAVORISE": "Principal",
    "BTN_PICTURE_ROTATE": "Girar",
    "BTN_ADD_PICTURE": "Añadir una foto",
    "NOTIFICATIONS": {
      "TITLE": "Notificaciones",
      "MARK_ALL_AS_READ": "Marcar todo como leído",
      "NO_RESULT": "Ningúna notificación",
      "SHOW_ALL": "Ver todo",
      "LOAD_NOTIFICATIONS_FAILED": "Fallo en la carga de las notificaciones"
    }
  },
  "DOCUMENT": {
    "HASH": "Hash: ",
    "LOOKUP": {
      "BTN_COMPACT": "Compactar",
      "HAS_CREATE_OR_UPDATE_PROFILE": "ha creado o modificado su perfil",
      "LAST_DOCUMENTS_DOTS": "Últimos documentos :",
      "TITLE": "Búsqueda de documentos",
      "BTN_ACTIONS": "Acciones",
      "SEARCH_HELP": "issuer:AAA*, time:1508406169",
      "LAST_DOCUMENTS": "Últimos documentos",
      "SHOW_QUERY": "Ver la búsqueda",
      "HIDE_QUERY": "Esconder la búsqueda",
      "HEADER_TIME": "Fecha/Hora",
      "HEADER_ISSUER": "Emisor",
      "HEADER_RECIPIENT": "Destinatario",
      "HEADER_AMOUNT": "Importe",
      "READ": "Leído",
      "BTN_REMOVE": "Sumprimer este documento",
      "POPOVER_ACTIONS": {
        "TITLE": "Acciones",
        "REMOVE_ALL": "Suprimir estos documentos..."
      }
    },
    "INFO": {
      "REMOVED": "Documento suprimido"
    },
    "CONFIRM": {
      "REMOVE": "¿Desea <b>suprimir este documento</b>?",
      "REMOVE_ALL": "¿Desea <b>suprimir estos documentos</b>?"
    },
    "ERROR": {
      "LOAD_DOCUMENTS_FAILED": "Error al buscar los documentos",
      "REMOVE_FAILED": "Error al suprimir el documento",
      "REMOVE_ALL_FAILED": "Error al suprimir los documentos"
    }
  },
  "MENU": {
    "REGISTRY": "Páginas",
    "USER_PROFILE": "Mi perfil",
    "MESSAGES": "Mensajes",
    "NOTIFICATIONS": "Notificaciones",
    "INVITATIONS": "Invitaciones"
  },
  "ACCOUNT": {
    "NEW": {
      "ORGANIZATION_ACCOUNT": "Cuenta para una organización",
      "ORGANIZATION_ACCOUNT_HELP": "Si representa una empresa, una asociación, etc.<br/>Ningún dividendo universal será creado por esta cuenta."
    },
    "EVENT": {
      "MEMBER_WITHOUT_PROFILE": "Para obtener sus certificaciones más rapidamente, complete <a ui-sref=\"app.edit_profile\">su perfil usuario</a>. Los miembros concederán más fácilmente su confianza a una identidad verificable."
    },
    "ERROR": {
      "WS_CONNECTION_FAILED": "Cesium no puede recibir las notificaciones, a causa de un error técnico (conexión al nodo de datos Cesium+).<br/><br/>Si el problema persiste, por favor <b>elige un otro nodo de datos</b> en los ajustes de Cesium+."
    }
  },
  "WOT": {
    "BTN_SUGGEST_CERTIFICATIONS_DOTS": "Sugerir identidades a certificar…",
    "BTN_ASK_CERTIFICATIONS_DOTS": "Pedir a miembros que le certifiquen…",
    "BTN_ASK_CERTIFICATION": "Pedir una certificación",
    "SUGGEST_CERTIFICATIONS_MODAL": {
      "TITLE": "Sugerir certificaciones",
      "HELP": "Selectionar sus sugerencias"
    },
    "ASK_CERTIFICATIONS_MODAL": {
      "TITLE": "Solicitar certificaciones",
      "HELP": "Seleccionar los destinatarios"
    },
    "SEARCH": {
      "DIVIDER_PROFILE": "Cuentas",
      "DIVIDER_PAGE": "Páginas",
      "DIVIDER_GROUP": "Grupos"
    },
    "CONFIRM": {
      "SUGGEST_CERTIFICATIONS": "¿Desea <b>enviar estas sugerencias de certificatión</b> ?",
      "ASK_CERTIFICATION": "¿Desea <b>enviar una solicitud de certificación</b> ?",
      "ASK_CERTIFICATIONS": "¿Desea <b>enviar una solicitud de certificación</b> a estas personas ?"
    }
  },
  "INVITATION": {
    "TITLE": "Invitaciones",
    "NO_RESULT": "Ningúna invitación en espera",
    "BTN_DELETE_ALL": "Suprimir todas las invitaciones",
    "BTN_DELETE": "Suprimir la invitación",
    "BTN_NEW_INVITATION": "Nueva invitación",
    "ASK_CERTIFICATION": "<a href=\"#/app/wot/{{pubkey}}/{{::uid}}\">{{::name||uid}}</a> solicita su certificación",
    "SUGGESTION_CERTIFICATION": "<a href=\"#/app/wot/{{::pubkey}}/{{::uid}}\">{{::name||uid}}</a> ha sido sugerido/a para certificación",
    "SUGGESTED_BY": "Sugerencia mandada por <a class=\"positive\" href=\"#/app/wot/{{::issuer.pubkey}}/{{::issuer.uid}}\">{{::issuer.name||issuer.uid}}</a>",
    "NOTIFICATIONS": {
      "TITLE": "Invitaciones"
    },
    "LIST": {
      "TITLE": "Invitaciones"
    },
    "NEW": {
      "TITLE": "Nueva invitación",
      "RECIPIENTS": "A",
      "RECIPIENTS_HELP": "Destinatarios de la invitación",
      "RECIPIENTS_MODAL_TITLE": "Destinatarios",
      "RECIPIENTS_MODAL_HELP": "Por favor, elige los destinatarios :",
      "SUGGESTION_IDENTITIES": "Sugerencia de certificación",
      "SUGGESTION_IDENTITIES_HELP": "Certificaciones a sugerir",
      "SUGGESTION_IDENTITIES_MODAL_TITLE": "Sugerencias",
      "SUGGESTION_IDENTITIES_MODAL_HELP": "Por favor, elige sus sugerencias :"
    },
    "CONFIRM": {
      "DELETE_ALL_CONFIRMATION": "La supresión de las invitaciones es una <b>operación ireversible</b>.<br/><br/>¿ Desea continuar ?",
      "SEND_INVITATIONS_TO_CERTIFY": "¿ Desea <b>mandar esta invitación a certificar</b> ?"
    },
    "INFO": {
      "INVITATION_SENT": "Invitación mandada"
    },
    "ERROR": {
      "LOAD_INVITATIONS_FAILED": "Fallo en la carga de las invitaciones",
      "REMOVE_INVITATION_FAILED": "Fallo durante la supresión de la invitación",
      "REMOVE_ALL_INVITATIONS_FAILED": "Fallo durante la supresión de las invitaciones",
      "SEND_INVITATION_FAILED": "Fallo durante el envío de la invitación",
      "BAD_INVITATION_FORMAT": "<span class=\"assertive\"><i class=\"ion-close-circled\"></i> Invitación ilegible (formato desconocido)</span> - mandada por <a ui-sref=\"app.wot_identity({pubkey: '{{::pubkey}}', uid: '{{::uid}}' })\">{{::name||uid}}</a>"
    }
  },
  "COMMENTS": {
    "DIVIDER": "Comentarios",
    "SHOW_MORE_COMMENTS": "Visualizar los comentarios anteriores",
    "COMMENT_HELP": "Su comentario, preguntas, etc.",
    "COMMENT_HELP_REPLY_TO": "Su repuesta…",
    "BTN_SEND": "Mandar",
    "POPOVER_SHARE_TITLE": "Mensaje #{{number}}",
    "MODIFIED_ON": "modificado el {{time|formatDate}}",
    "MODIFIED_PARENTHESIS": "(modificado entonces)",
    "REPLY": "Responder",
    "REPLY_TO": "Repuesta a :",
    "REPLY_TO_LINK": "En repuesta a ",
    "REPLY_TO_DELETED_COMMENT": "En repuesta a un comentario suprimido",
    "REPLY_COUNT": "{{replyCount}} repuestas",
    "DELETED_COMMENT": "Comentario suprimido",
    "ERROR": {
      "FAILED_SAVE_COMMENT": "Fallo durante el respaldo del comentario",
      "FAILED_REMOVE_COMMENT": "Fallo durante la supresión del comentario"
    }
  },
  "MESSAGE": {
    "REPLY_TITLE_PREFIX": "Rep: ",
    "FORWARD_TITLE_PREFIX": "Tr: ",
    "BTN_REPLY": "Responder",
    "BTN_COMPOSE": "Nuevo mensaje",
    "BTN_WRITE": "Escribir",
    "NO_MESSAGE_INBOX": "Ningún mensaje recibido",
    "NO_MESSAGE_OUTBOX": "Ningún mensaje mandado",
    "NOTIFICATIONS": {
      "TITLE": "Mensajes",
      "MESSAGE_RECEIVED": "Ha <b>recibido un mensaje</b><br/>de"
    },
    "LIST": {
      "INBOX": "Bandeja de entrada",
      "OUTBOX": "Mensajes enviados",
      "LAST_INBOX": "Nuevos mensajes",
      "LAST_OUTBOX": "Mensajes enviados",
      "BTN_LAST_MESSAGES": "Mensajes recientes",
      "TITLE": "Mensajes",
      "SEARCH_HELP": "Buscar en mensajes",
      "POPOVER_ACTIONS": {
        "TITLE": "Opciones",
        "DELETE_ALL": "Suprimir todos los mensajes"
      }
    },
    "COMPOSE": {
      "TITLE": "Nuevo mensaje",
      "TITLE_REPLY": "Responder",
      "SUB_TITLE": "Nuevo mensaje",
      "TO": "A",
      "OBJECT": "Objeto",
      "OBJECT_HELP": "Objeto",
      "ENCRYPTED_HELP": "Tenga en cuenta que este mensaje será cifrado antes del envío, con el fin de que solo el destinatario pueda leerlo, y que se tenga la seguridad de que la autoría es suya.",
      "MESSAGE": "Mensaje",
      "MESSAGE_HELP": "Contenido del mensaje",
      "CONTENT_CONFIRMATION": "El contenido del mensaje está vacío.<br/><br/>¿ Sin embargo, quiere mandar el mensaje ?"
    },
    "VIEW": {
      "TITLE": "Mensaje",
      "SENDER": "Enviado por",
      "RECIPIENT": "Enviado a",
      "NO_CONTENT": "Mensaje vacío",
      "DELETE": "Eliminar el mensaje"
    },
    "CONFIRM": {
      "REMOVE": "¿ Desea <b>suprimir este mensaje</b> ?<br/><br/>Esta operación es ireversible.",
      "REMOVE_ALL" : "¿ Desea <b>suprimir todos los mensajes</b> ?<br/><br/>Esta operación es ireversible.",
      "MARK_ALL_AS_READ": "¿ Desea <b>marcar todos los mensajes como leído</b> ?",
      "USER_HAS_NO_PROFILE": "Esta identidad no tiene ningún perfil Cesium+. Puede que no tenga habilitada la extensión Cesium+, y <b>no podrá ver su mensaje</b>.<br/><br/>¿ Desea <b>continuar</b> a pesar de todo ?"
    },
    "INFO": {
      "MESSAGE_REMOVED": "Mensaje suprimido",
      "All_MESSAGE_REMOVED": "Todos los mensajes fueron suprimido",
      "MESSAGE_SENT": "Mensaje mandado"
    },
    "ERROR": {
      "SEND_MSG_FAILED": "Fallo durante el envío del mensaje.",
      "LOAD_MESSAGES_FAILED": "Fallo durante la recuperación de los mensajes.",
      "LOAD_MESSAGE_FAILED": "Fallo durante la recuperación del mensaje.",
      "MESSAGE_NOT_READABLE": "Lectura del mensaje imposible.",
      "USER_NOT_RECIPIENT": "No está el destinatario de este mensaje : deciframiento imposible.",
      "NOT_AUTHENTICATED_MESSAGE": "La autenticidad del mensaje es dudosa o su contenido está corrupto.",
      "REMOVE_MESSAGE_FAILED": "Fallo en la supresión del mensaje",
      "MESSAGE_CONTENT_TOO_LONG": "Valor demasiado largo ({{maxLength}} carácteres max).",
      "MARK_AS_READ_FAILED": "Imposible marcar el mensaje como 'leído'.",
      "LOAD_NOTIFICATIONS_FAILED": "Fallo durante la recuperación de las notificaciones de mensajes.",
      "REMOVE_All_MESSAGES_FAILED": "Fallo durante la supresión de todos los mensajes.",
      "MARK_ALL_AS_READ_FAILED": "Fallo durante el marcaje de los mensajes como leído.",
      "RECIPIENT_IS_MANDATORY": "El destinatario es obligatorio."
    }
  },
  "BLOCKCHAIN": {
    "LOOKUP": {
      "SEARCH_HELP": "Número de bloque, hash, llave pública, etc.",
      "POPOVER_FILTER_TITLE": "Filtros",
      "HEADER_MEDIAN_TIME": "Fecha / Hora",
      "HEADER_BLOCK": "Bloque #",
      "HEADER_ISSUER": "Nodo emisor",
      "BTN_LAST": "Últimos bloques",
      "DISPLAY_QUERY": "Mostrar la consulta",
      "HIDE_QUERY": "Ocultar la consulta",
      "TX_SEARCH_FILTER": {
        "MEMBER_FLOWS": "Entradas/salidas de miembros",
        "EXISTING_TRANSACTION": "Con transacciones",
        "PERIOD": "<b class=\"ion-clock\"></b> Entre el <b class=\"gray\">{{params[1]|medianDateShort}}</b> ({{params[1]|medianTime}}) y el <b class=\"gray\">{{params[2]|medianDateShort}}</b> ({{params[2]|medianTime}})",
        "ISSUER": "<b class=\"ion-android-desktop\"></b> Calculado por <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}",
        "TX_PUBKEY": "<b class=\"ion-card\"></b> Transacciones que implican <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}"
      }
    },
    "ERROR": {
      "SEARCH_BLOCKS_FAILED": "Fallo en la búsqueda de los bloques."
    }
  },
  "GROUP": {
    "GENERAL_DIVIDER": "Informaciones generales",
    "LOCATION_DIVIDER": "Dirección",
    "SOCIAL_NETWORKS_DIVIDER": "Redes sociales y página web",
    "TECHNICAL_DIVIDER": "Informaciones técnicas",
    "CREATED_TIME": "Creada {{creationTime|formatFromNow}}",
    "NOTIFICATIONS": {
      "TITLE": "Invitaciones"
    },
    "LOOKUP": {
      "TITLE": "Grupos",
      "SEARCH_HELP": "Nombre de grupo, palabras , lugar, etc.",
      "LAST_RESULTS_LIST": "Nuevos grupos :",
      "OPEN_RESULTS_LIST": "Grupos abiertos :",
      "MANAGED_RESULTS_LIST": "Grupos cerrados :",
      "BTN_LAST": "Nuevos grupos",
      "BTN_NEW": "Añado un grupo"
    },
    "TYPE": {
      "TITLE": "Nuevo grupo",
      "SELECT_TYPE": "Tipo de grupo :",
      "OPEN_GROUP": "Grupo abierto",
      "OPEN_GROUP_HELP": "Un grupo abierto es accesible por cualquier miembro de la moneda.",
      "MANAGED_GROUP": "Grupo administrado",
      "MANAGED_GROUP_HELP": "Un grupo administrado es gestionado por administradores y moderadores, que pueden aceptar, rechazar o excluir un miembro en su seno.",
      "ENUM": {
        "OPEN": "Grupo abierto",
        "MANAGED": "Grupo administrado"
      }
    },
    "VIEW": {
      "POPOVER_SHARE_TITLE": "{{title}}",
      "MENU_TITLE": "Opciones",
      "REMOVE_CONFIRMATION" : "¿Desea eliminar este grupo?<br/><br/>Esta operación es irreversible."
    },
    "EDIT": {
      "TITLE": "Grupo",
      "TITLE_NEW": "Nuevo grupo",
      "RECORD_TITLE": "Título",
      "RECORD_TITLE_HELP": "Título",
      "RECORD_DESCRIPTION": "Descripción",
      "RECORD_DESCRIPTION_HELP": "Descripción"
    },
    "ERROR": {
      "SEARCH_GROUPS_FAILED": "Fallo en la búsqueda de grupos",
      "REMOVE_RECORD_FAILED": "Error al eliminar el grupo"
    },
    "INFO": {
      "RECORD_REMOVED" : "Grupo eliminado"
    }
  },
  "REGISTRY": {
    "CATEGORY": "Actividad principal",
    "GENERAL_DIVIDER": "Informaciones generales",
    "LOCATION_DIVIDER": "Dirección",
    "SOCIAL_NETWORKS_DIVIDER": "Redes sociales y sitio web",
    "TECHNICAL_DIVIDER": "Informaciones técnicas",
    "BTN_SHOW_WOT": "Personas",
    "BTN_SHOW_WOT_HELP": "Buscar personas",
    "BTN_SHOW_PAGES": "Páginas",
    "BTN_SHOW_PAGES_HELP": "Búsqueda de páginas",
    "BTN_NEW": "Crear una página",
    "MY_PAGES": "Mis páginas",
    "NO_PAGE": "Sin páginas",
    "SEARCH": {
      "TITLE": "Páginas",
      "SEARCH_HELP": "Qué, Quién, ej: peluquería, restaurante Sol.",
      "BTN_ADD": "Nuevo",
      "BTN_LAST_RECORDS": "Páginas recientes",
      "BTN_ADVANCED_SEARCH": "búsqueda avanzada",
      "BTN_OPTIONS": "Búsqueda avanzada",
      "TYPE": "Tipo de página",
      "LOCATION_HELP": "Ciudad",
      "RESULTS": "Resultados",
      "RESULT_COUNT_LOCATION": "{{count}} Resultado{{count>0?'s':''}}, cerca de {{location}}",
      "RESULT_COUNT": "{{count}} resultado{{count>0?'s':''}}",
      "LAST_RECORDS": "Páginas recientes",
      "LAST_RECORD_COUNT_LOCATION": "{{count}} página{{count>0?'s':''}} reciente{{count>0?'s':''}}, cerca de {{location}}",
      "LAST_RECORD_COUNT": "{{count}} página{{count>0?'s':''}} reciente{{count>0?'s':''}}",
      "POPOVER_FILTERS": {
        "BTN_ADVANCED_SEARCH": "Opciones avanzadas"
      }
    },
    "VIEW": {
      "TITLE": "Anuario",
      "CATEGORY": "Actividad principal :",
      "LOCATION": "Dirección :",
      "MENU_TITLE": "Opciones",
      "POPOVER_SHARE_TITLE": "{{title}}",
      "REMOVE_CONFIRMATION" : "¿ Desea suprimir esta página ?<br/><br/>Esta operación es ireversible."
    },
    "TYPE": {
      "TITLE": "Nueva página",
      "SELECT_TYPE": "Tipo de página :",
      "ENUM": {
        "SHOP": "Comercio local",
        "COMPANY": "Empresa",
        "ASSOCIATION": "Asociación",
        "INSTITUTION": "Institución"
      }
    },
    "EDIT": {
      "TITLE": "Edición",
      "TITLE_NEW": "Nueva página",
      "RECORD_TYPE":"Tipo de página",
      "RECORD_TITLE": "Nombre",
      "RECORD_TITLE_HELP": "Nombre",
      "RECORD_DESCRIPTION": "Descripción",
      "RECORD_DESCRIPTION_HELP": "Descripción de la actividad",
      "RECORD_ADDRESS": "Calle",
      "RECORD_ADDRESS_HELP": "Calle, edificio…",
      "RECORD_CITY": "Ciudad",
      "RECORD_CITY_HELP": "Ciudad",
      "RECORD_SOCIAL_NETWORKS": "Redes sociales y sitio web",
      "RECORD_PUBKEY": "Llave pública",
      "RECORD_PUBKEY_HELP": "Llave pública para recibir pagos"
    },
    "WALLET": {
      "PAGE_DIVIDER": "Páginas",
      "PAGE_DIVIDER_HELP": "Las páginas se refieren a colectivos que aceptan moneda o la promocionan: tiendas, empresas, negocios, asociaciones, instituciones. Se almacenan fuera de la red de la moneda, en <a ui-sref=\"app.es_network\">la red Cesium+</a>."
    },
    "ERROR": {
      "LOAD_CATEGORY_FAILED": "Fallo en la carga de la lista de actividades",
      "LOAD_RECORD_FAILED": "Fallo durante la carga de la página",
      "LOOKUP_RECORDS_FAILED": "Fallo durante la ejecución de la búsqueda.",
      "REMOVE_RECORD_FAILED": "Fallo en la supresión de la página",
      "SAVE_RECORD_FAILED": "Fallo durante el respaldo",
      "RECORD_NOT_EXISTS": "Página inexistente",
      "GEO_LOCATION_NOT_FOUND": "Ciudad o código postal no encontrado"
    },
    "INFO": {
      "RECORD_REMOVED" : "Página suprimida",
      "RECORD_SAVED": "Página guardada"
    }
  },
  "PROFILE": {
    "PROFILE_DIVIDER": "Perfil Cesium+",
    "PROFILE_DIVIDER_HELP": "Estos son datos auxiliares, almacenados fuera de la red monetaria",
    "NO_PROFILE_DEFINED": "Ningún perfil Cesium+",
    "BTN_ADD": "Ingresar mi perfil",
    "BTN_EDIT": "Editar mi perfil",
    "BTN_DELETE": "Eliminar mi perfil",
    "BTN_REORDER": "Reordenar",
    "UID": "Seudónimo",
    "TITLE": "Nombre, Apellidos",
    "TITLE_HELP": "Nombre, Apellidos",
    "DESCRIPTION": "Sobre mí",
    "DESCRIPTION_HELP": "Sobre mí…",
    "SOCIAL_HELP": "http://...",
    "GENERAL_DIVIDER": "Informaciones generales",
    "SOCIAL_NETWORKS_DIVIDER": "Redes sociales, sitios web",
    "TECHNICAL_DIVIDER": "Informaciones técnicas",
    "MODAL_AVATAR": {
      "TITLE": "Foto de perfil",
      "SELECT_FILE_HELP": "Por favor, <b>elija una imagen</b>:",
      "BTN_SELECT_FILE": "Eligir una imagen",
      "RESIZE_HELP": "<b>Encuadre la imagen</b>, si es necesario. Un clic presionado sobre la imagen permite desplazarla. Haga clic en la zona inferior izquierda para hacer zoom.",
      "RESULT_HELP": "<b>Aquí está el resultado</b> tal como se verá sobre su perfil :"
    },
    "CONFIRM": {
      "DELETE": "¿Desea <b>eliminar su perfil Cesium+?</b><br/><br/>Esta operación es irreversible.",
      "DELETE_BY_MODERATOR": "¿Desea <b>eliminar este perfil Cesium+?</b><br/><br/>Esta operación es irreversible."
    },
    "ERROR": {
      "DELETE_PROFILE_FAILED": "Error durante la eliminación del perfil",
      "REMOVE_PROFILE_FAILED": "Error de eliminación del perfil",
      "LOAD_PROFILE_FAILED": "Fallo en la carga del perfil usuario.",
      "SAVE_PROFILE_FAILED": "Fallo durante el respaldo",
      "INVALID_SOCIAL_NETWORK_FORMAT": "Formato inválido: por favor, indique una dirección válida.<br/><br/>Ejemplos :<ul><li>- Una página Facebook (https://www.facebook.com/user)</li><li>- Una página web (http://www.misitio.es)</li><li>- Una dirección de correo (joe@dalton.com)</li></ul>",
      "IMAGE_RESIZE_FAILED": "Falló el redimensionado de la imagen"
    },
    "INFO": {
      "PROFILE_REMOVED": "Perfil eliminado",
      "PROFILE_SAVED": "Perfil guardado"
    },
    "HELP": {
      "WARNING_PUBLIC_DATA": "La información de su perfil <b>es pública</b>: visible también por personas <b>sin cuenta</b>.<br/>{{'PROFILE.PROFILE_DIVIDER_HELP'|translate}}"
    }
  },
  "LIKE": {
    "ERROR": {
        "FAILED_TOGGLE_LIKE": "Imposible ejecutar esta acción."
    }
  },
  "LOCATION": {
    "BTN_GEOLOC_ADDRESS": "Actualizar desde la dirección",
    "USE_GEO_POINT": "Aparecer en el mapa {{'COMMON.APP_NAME'|translate}}",
    "LOADING_LOCATION": "Encontrar la dirección…",
    "LOCATION_DIVIDER": "Dirección",
    "ADDRESS": "Calle",
    "ADDRESS_HELP": "Calle, número, etc…",
    "CITY": "Ciudad",
    "CITY_HELP": "Ciudad, País",
    "DISTANCE": "Distancia máxima alrededor de la ciudad",
    "DISTANCE_UNIT": "km",
    "DISTANCE_OPTION": "{{value}} {{'LOCATION.DISTANCE_UNIT'|translate}}",
    "SEARCH_HELP": "Ciudad, País",
    "PROFILE_POSITION": "Posición del perfil",
    "MODAL": {
      "TITLE": "Búsqueda de dirección",
      "SEARCH_HELP": "Ciudad, País",
      "ALTERNATIVE_RESULT_DIVIDER": "Resultados alternativos para <b>{{address}}</b> :",
      "POSITION": "Latitud/Longitud : {{lat}} / {{lon}}"
    },
    "ERROR": {
      "CITY_REQUIRED_IF_STREET": "Requerido si una calle ha sido llenada",
      "REQUIRED_FOR_LOCATION": "Campo obligatorio para aparecer en el mapa",
      "INVALID_FOR_LOCATION": "Dirección desconocida",
      "GEO_LOCATION_FAILED": "No se puede recuperar su ubicación Por favor usa el botón de búsqueda.",
      "ADDRESS_LOCATION_FAILED": "No se puede recuperar la posición de la dirección."
    }
  },
  "SUBSCRIPTION": {
    "SUBSCRIPTION_DIVIDER": "Aplicaciones de terceros",
    "SUBSCRIPTION_DIVIDER_HELP": "Las aplicaciones de terceros ofrecen servicios adicionales, proporcionados por un tercero.",
    "BTN_ADD": "Agregar una aplicación",
    "BTN_EDIT": "Administrar mis aplicaciones",
    "NO_SUBSCRIPTION": "Ningúna suscripción definida",
    "SUBSCRIPTION_COUNT": "Suscripciones",
    "EDIT": {
      "TITLE": "Aplicaciones de terceros",
      "HELP_TEXT": "Gestione sus suscripciones y otras aplicaciones de terceros aquí",
      "PROVIDER": "Proveedor:"
    },
    "TYPE": {
      "ENUM": {
        "EMAIL": "Recibir notificaciones por correo electrónico"
      }
    },
    "CONFIRM": {
      "DELETE_SUBSCRIPTION": "¿ Deseas <b>eliminar</b> esta suscripción ?"
    },
    "ERROR": {
      "LOAD_SUBSCRIPTIONS_FAILED": "Error al cargar aplicaciones de terceros",
      "ADD_SUBSCRIPTION_FAILED": "Error al agregar suscripción",
      "UPDATE_SUBSCRIPTION_FAILED": "Error durante la actualización de la suscripción",
      "DELETE_SUBSCRIPTION_FAILED": "Error al eliminar la suscripción"
    },
    "MODAL_EMAIL": {
      "TITLE" : "Notificación por correo electrónico",
      "HELP" : "Rellene este formulario para <b>ser notificado por correo electrónico</b> de los eventos de su cuenta. <br/> Su dirección de correo electrónico se cifrará y únicamente será visible para el proveedor de servicios.",
      "EMAIL_LABEL" : "Su correo electrónico :",
      "EMAIL_HELP": "maria@dominio.com",
      "FREQUENCY_LABEL": "Frecuencia de las notificaciones :",
      "FREQUENCY_DAILY": "Diaria",
      "FREQUENCY_WEEKLY": "Semanal",
      "PROVIDER": "Proveedor de la aplicación :"
    }
  },
  "ES_PEER": {
    "DOCUMENT_COUNT": "Número de documentos",
    "DOCUMENTS": "Documentos",
    "EMAIL_SUBSCRIPTION_COUNT": "{{emailSubscription}} suscrito/a{{emailSubscription ? 's' : ''}} a notificaciones por correo",
    "NAME": "Nombre",
    "SOFTWARE": "Software"
  },
  "ES_SETTINGS": {
    "PLUGIN_NAME": "Cesium+",
    "PLUGIN_NAME_HELP": "Perfiles, notificaciones, mensajes privados",
    "ENABLE_TOGGLE": "Activar la extensión",
    "ENABLE_MESSAGE_TOGGLE": "Activar los mensajes privados",
    "ENABLE_REMOTE_STORAGE": "Activar el almacenamiento remoto",
    "ENABLE_REMOTE_STORAGE_HELP": "Permite almacenar (cifrados) sus ajustes en los nodos Cesium+",
    "PEER": "Dirección del nodo de datos Cesium+",
    "POPUP_PEER": {
      "TITLE" : "Nodo de datos Cesium+",
      "HELP" : "Ingrese la dirección del nodo que quiere utilizar:",
      "PEER_HELP": "servidor.dominio.com:puerto"
    },
    "NOTIFICATIONS": {
      "DIVIDER": "Notificaciones",
      "HELP_TEXT": "Active los tipos de notificaciones que desea recibir:",
      "ENABLE_TX_SENT": "Notificar la validación de los <b>pagos emitidos</b>",
      "ENABLE_TX_RECEIVED": "Notificar la validación de los <b>pagos recibidos</b>",
      "ENABLE_CERT_SENT": "Notificar la validación de las <b>certificaciones emitidas</b>",
      "ENABLE_CERT_RECEIVED": "Notificar la validación de las <b>certificaciones recibidas</b>",
      "ENABLE_HTML5_NOTIFICATION": "Avisar con cada nueva notificación",
      "ENABLE_HTML5_NOTIFICATION_HELP": "Abre una pequeña ventana emergente con cada nueva notificación."
    },
    "CONFIRM": {
      "ASK_ENABLE_TITLE": "Otras funcionalidades",
      "ASK_ENABLE": "La extensión de Cesium+ está deshabilitada en sus ajutes, desactivando ciertas funcionalidades: <ul><li>&nbsp;&nbsp;<b><i class=\"icon ion-person\"></i> Perfiles de usuario/a</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-android-notifications\"></i> Notificaciones</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-email\"></i> Mensajes privados</b>.</ul><br/><br/>¿<b>Desea re-activar</b> la extensión?"
    }
  },
  "ES_WALLET": {
    "ERROR": {
      "RECIPIENT_IS_MANDATORY": "Un destinatario es obligatorio para el cifrado."
    }
  },
  "EVENT": {
    "NODE_STARTED": "Su nodo ES API <b>{{params[0]}}</b> es comenzado",
    "NODE_BMA_DOWN": "El nodo <b>{{params[0]}}:{{params[1]}}</b> (utilizado por su nodo ES API) <b>no es localizable</b>.",
    "NODE_BMA_UP": "El nodo <b>{{params[0]}}:{{params[1]}}</b> es de nuevo accesible.",
    "MEMBER_JOIN": "Ahora es <b>miembro</b> de la moneda <b>{{params[0]}}</b> !",
    "MEMBER_LEAVE": "No es <b>miembro</b> de la moneda <b>{{params[0]}}</b>!",
    "MEMBER_EXCLUDE": "Usted ya no es miembro de la moneda <b>{{params[0]}}</b>, por falta de renovación o certificaciones.",
    "MEMBER_REVOKE": "Su membresía ha sido revocada. Ya no es miembro de la moneda <b>{{params[0]}}</b>.",
    "MEMBER_ACTIVE": "Su membresía a <b>{{params[0]}}</b> ha sido <b>renovada con éxito</b>.",
    "TX_SENT": "Su <b>pago</b> a <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> fue efectuado.",
    "TX_SENT_MULTI": "Su <b>pago</b> a <b>{{params[1]}}</b> fue efectuado.",
    "TX_RECEIVED": "Ha <b>recibido un pago</b> de <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "TX_RECEIVED_MULTI": "Ha <b>recibido un pago</b> de <b>{{params[1]}}</b>.",
    "CERT_SENT": "Su <b>certificación</b> a <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> fue efectuada.",
    "CERT_RECEIVED": "Ha <b>recibido una certificación</b> de <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "USER": {
        "ABUSE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha reportardo su perfil",
        "DELETION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha reportado un perfil para suprimir : <b>{{params[2]}}</b>",
        "FOLLOW_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> sigue la actividad de su perfil",
        "LIKE_RECEIVED": "A <span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> le ha gustado su perfil</b>",
        "MODERATION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> os pide moderación sobre el perfil : <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
        "STAR_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> os ha puntuado con ({{params[3]}} <b class=\"ion-star\">)"
    },
    "PAGE": {
      "ABUSE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha reportardo su página : <b>{{params[2]}}</b>",
      "DELETION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha reportado una página para suprimir : <b>{{params[2]}}</b>",
      "FOLLOW_CLOSE": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha cerrado la página : <b>{{params[2]}}</b>",
      "FOLLOW_NEW": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha añadido la página : <b>{{params[2]}}</b>",
      "FOLLOW_NEW_COMMENT": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha comentado la página : <b>{{params[2]}}</b>",
      "FOLLOW_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> sigue su página : <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha modificado la página : <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE_COMMENT": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> ha modificado su comentario en la página : <b>{{params[2]}}</b>",
      "MODERATION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> os pide moderación sobre la página : <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",

      "NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha comentado su referencia : <b>{{params[2]}}</b>",
      "UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha modificado su comentario sobre su referencia : <b>{{params[2]}}</b>",
      "NEW_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha contestado a su comentario sobre la referencia : <b>{{params[2]}}</b>",
      "UPDATE_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha modificado la repuesta a su comentario sobre la referencia : <b>{{params[2]}}</b>"
    }
  },
  "CONFIRM": {
    "ES_USE_FALLBACK_NODE": "Nodo de datos <b>{{old}}</b> inalcanzable o dirección no válida.<br/><br/>¿Desea utilizar temporalmente el nodo de datos <b>{{new}}</b>?"
  },
  "ERROR": {
    "ES_CONNECTION_ERROR": "Nodo de datos <b>{{server}}</b> inalcanzable o dirección no válida.<br/><br/>Cesium continuará funcionando, <b>sin la extensión Cesium+</b> (perfiles de usuario, mensajes privados), mapas y gráficos).<br/><br/>Verifique su conexión a Internet, o cambie el nodo de datos en <a class=\"positive\" ng-click=\"doQuickFix('settings')\"> los ajustes de la extensión </a>.",
    "ES_MAX_UPLOAD_BODY_SIZE": "El volumen de datos a enviar excede el límite establecido por el servidor.<br/><br/>Por favor, inténtelo de nuevo después, por ejemplo, borrando fotos."
  }
}
);

$translateProvider.translations("fr-FR", {
  "COMMON": {
    "CATEGORY": "Catégorie",
    "CATEGORIES": "Catégories",
    "CATEGORY_SEARCH_HELP": "Recherche",
    "COMMENT_HELP": "Commentaire",
    "LAST_MODIFICATION_DATE": "Mise à jour le",
    "SUBMIT_BY": "Soumis par",
    "BTN_LIKE": "J'aime",
    "BTN_LIKE_REMOVE": "Je n'aime plus",
    "LIKES_TEXT": "{{total}} personne{{total > 1 ? 's' : ''}} {{total > 1 ? 'ont' : 'a'}} aimé cette page",
    "ABUSES_TEXT": "{{total}} personne{{total > 1 ? 's' : ''}} {{total > 1 ? 'ont' : 'a'}} signalé un problème",
    "BTN_REPORT_ABUSE_DOTS": "Signaler un problème ou un abus...",
    "BTN_REMOVE_REPORTED_ABUSE": "Annuler mon signalement",
    "BTN_PUBLISH": "Publier",
    "BTN_PICTURE_DELETE": "Supprimer",
    "BTN_PICTURE_FAVORISE": "Principale",
    "BTN_PICTURE_ROTATE": "Tourner",
    "BTN_ADD_PICTURE": "Ajouter une photo",
    "NOTIFICATION": {
      "TITLE": "Nouvelle notification | {{'COMMON.APP_NAME'|translate}}",
      "HAS_UNREAD": "Vous avez {{count}} notification{{count>0?'s':''}} non lue{{count>0?'s':''}}"
    },
    "NOTIFICATIONS": {
      "TITLE": "Notifications",
      "MARK_ALL_AS_READ": "Tout marquer comme lu",
      "NO_RESULT": "Aucune notification",
      "SHOW_ALL": "Voir tout",
      "LOAD_NOTIFICATIONS_FAILED": "Erreur de chargement des notifications"
    },
    "REPORT_ABUSE": {
      "TITLE": "Signaler un problème",
      "SUB_TITLE": "Merci d'expliquer succintement le problème :",
      "REASON_HELP": "J'explique le problème...",
      "ASK_DELETE": "Demander la suppression ?",
      "CONFIRM": {
        "SENT": "Signalement envoyé. Merci !"
      }
    }
  },
  "MENU": {
    "REGISTRY": "Pages",
    "USER_PROFILE": "Mon profil",
    "MESSAGES": "Messages",
    "NOTIFICATIONS": "Notifications",
    "INVITATIONS": "Invitations"
  },
  "ACCOUNT": {
    "NEW": {
      "ORGANIZATION_ACCOUNT": "Compte pour une organisation",
      "ORGANIZATION_ACCOUNT_HELP": "Si vous représentez une entreprise, une association, etc.<br/>Aucun dividende universel ne sera créé par ce compte."
    },
    "EVENT": {
      "MEMBER_WITHOUT_PROFILE": "Vous pouvez <a ui-sref=\"app.edit_profile\">saisir votre profil Cesium+</a> (optionnel) pour offrir une meilleure visibilité de votre compte. Ce profil sera stocké dans <b>un annuaire indépendant</b> de la monnaie, mais décentralisé."
    },
    "ERROR": {
      "WS_CONNECTION_FAILED": "Cesium ne peut pas recevoir les notifications, à cause d'une erreur technique (connexion au noeud de données Cesium+).<br/><br/>Si le problème persiste, veuillez <b>choisir un autre noeud de données</b> dans les paramètres Cesium+."
    }
  },
  "WOT": {
    "BTN_SUGGEST_CERTIFICATIONS_DOTS": "Suggérer des identités à certifier...",
    "BTN_ASK_CERTIFICATIONS_DOTS": "Demander à des membres de me certifier...",
    "BTN_ASK_CERTIFICATION": "Demander une certification",
    "SUGGEST_CERTIFICATIONS_MODAL": {
      "TITLE": "Suggérer des certifications",
      "HELP": "Sélectionner vos suggestions"
    },
    "ASK_CERTIFICATIONS_MODAL": {
      "TITLE": "Demander des certifications",
      "HELP": "Sélectionner les destinataires"
    },
    "SEARCH": {
      "DIVIDER_PROFILE": "Comptes",
      "DIVIDER_PAGE": "Pages",
      "DIVIDER_GROUP": "Groupes"
    },
    "CONFIRM": {
      "SUGGEST_CERTIFICATIONS": "Êtes-vous sûr de vouloir <b>envoyer ces suggestions de certification</b> ?",
      "ASK_CERTIFICATION": "Êtes-vous sûr de vouloir <b>envoyer une demande de certification</b> ?",
      "ASK_CERTIFICATIONS": "Êtes-vous sûr de vouloir <b>envoyer une demande de certification</b> à ces personnes ?"
    }
  },
  "INVITATION": {
    "TITLE": "Invitations",
    "NO_RESULT": "Aucune invitation en attente",
    "BTN_DELETE_ALL": "Supprimer toutes les invitations",
    "BTN_DELETE": "Supprimer l'invitation",
    "BTN_NEW_INVITATION": "Nouvelle invitation",
    "ASK_CERTIFICATION": "<a href=\"#/app/wot/{{pubkey}}/{{::uid}}\">{{::name||uid}}</a> demande votre certification",
    "SUGGESTION_CERTIFICATION": "<a href=\"#/app/wot/{{::pubkey}}/{{::uid}}\">{{::name||uid}}</a> vous est suggéré pour certification",
    "SUGGESTED_BY": "Suggestion envoyée par <a class=\"positive\" href=\"#/app/wot/{{::issuer.pubkey}}/{{::issuer.uid}}\">{{::issuer.name||issuer.uid}}</a>",
    "NOTIFICATIONS": {
      "TITLE": "Invitations"
    },
    "LIST": {
      "TITLE": "Invitations"
    },
    "NEW": {
      "TITLE": "Nouvelle invitation",
      "RECIPIENTS": "A",
      "RECIPIENTS_HELP": "Destinataires de l'invitation",
      "RECIPIENTS_MODAL_TITLE": "Destinataires",
      "RECIPIENTS_MODAL_HELP": "Veuillez choisir les destinataires :",
      "SUGGESTION_IDENTITIES": "Suggestions de certification",
      "SUGGESTION_IDENTITIES_HELP": "Certifications à suggérer",
      "SUGGESTION_IDENTITIES_MODAL_TITLE": "Suggestions",
      "SUGGESTION_IDENTITIES_MODAL_HELP": "Veuillez choisir vos suggestions :"
    },
    "CONFIRM": {
      "DELETE_ALL_CONFIRMATION": "La suppression des invitations est une <b>opération irréversible</b>.<br/><br/><b>Êtes-vous sûr</b> de vouloir continuer ?",
      "SEND_INVITATIONS_TO_CERTIFY": "Êtes-vous sûr de vouloir <b>envoyer cette invitation à certifier</b> ?"
    },
    "INFO": {
      "INVITATION_SENT": "Invitation envoyée"
    },
    "ERROR": {
      "LOAD_INVITATIONS_FAILED": "Échec du chargement des invitations",
      "REMOVE_INVITATION_FAILED": "Erreur lors de la suppression de l'invitation",
      "REMOVE_ALL_INVITATIONS_FAILED": "Erreur lors de la suppression des invitations",
      "SEND_INVITATION_FAILED": "Erreur lors de l'envoi de l'invitation",
      "BAD_INVITATION_FORMAT": "<span class=\"assertive\"><i class=\"ion-close-circled\"></i> Invitation illisible (format inconnu)</span> - envoyée par <a ui-sref=\"app.wot_identity({pubkey: '{{::pubkey}}', uid: '{{::uid}}' })\">{{::name||uid}}</a>"
    }
  },
  "COMMENTS": {
    "DIVIDER": "Commentaires",
    "SHOW_MORE_COMMENTS": "Afficher les commentaires précédents",
    "COMMENT_HELP": "Votre commentaire, question, etc.",
    "COMMENT_HELP_REPLY_TO": "Votre réponse...",
    "BTN_SEND": "Envoyer",
    "POPOVER_SHARE_TITLE": "Message #{{number}}",
    "REPLY": "Répondre",
    "REPLY_TO": "Réponse à :",
    "REPLY_TO_LINK": "En réponse à ",
    "REPLY_TO_DELETED_COMMENT": "En réponse à un commentaire supprimé",
    "REPLY_COUNT": "{{replyCount}} réponses",
    "DELETED_COMMENT": "Commentaire supprimé",
    "MODIFIED_ON": "modifié le {{time|formatDate}}",
    "MODIFIED_PARENTHESIS": "(modifié ensuite)",
    "ERROR": {
      "FAILED_SAVE_COMMENT": "Erreur lors de la sauvegarde du commentaire",
      "FAILED_REMOVE_COMMENT": "Erreur lors de la suppression du commentaire"
    }
  },
  "MESSAGE": {
    "REPLY_TITLE_PREFIX": "Rep: ",
    "FORWARD_TITLE_PREFIX": "Tr: ",
    "BTN_REPLY": "Répondre",
    "BTN_COMPOSE": "Nouveau message",
    "BTN_WRITE": "Ecrire",
    "NO_MESSAGE_INBOX": "Aucun message reçu",
    "NO_MESSAGE_OUTBOX": "Aucun message envoyé",
    "NOTIFICATIONS": {
      "TITLE": "Messages",
      "MESSAGE_RECEIVED": "Vous avez <b>reçu un message</b><br/>de"
    },
    "LIST": {
      "INBOX": "Boîte de réception",
      "OUTBOX": "Messages envoyés",
      "LAST_INBOX": "Nouveaux messages",
      "LAST_OUTBOX": "Messages envoyés",
      "BTN_LAST_MESSAGES": "Messages récents",
      "TITLE": "Messages",
      "SEARCH_HELP": "Recherche dans les messages",
      "POPOVER_ACTIONS": {
        "TITLE": "Options",
        "DELETE_ALL": "Supprimer tous les messages"
      }
    },
    "COMPOSE": {
      "TITLE": "Nouveau message",
      "TITLE_REPLY": "Répondre",
      "SUB_TITLE": "Nouveau message",
      "TO": "A",
      "OBJECT": "Objet",
      "OBJECT_HELP": "Objet",
      "ENCRYPTED_HELP": "Veuillez noter que ce message sera chiffré avant envoi, afin que seul le destinataire puisse le lire, et qu'il soit assuré que vous soyez bien son auteur.",
      "MESSAGE": "Message",
      "MESSAGE_HELP": "Contenu du message",
      "CONTENT_CONFIRMATION": "Le contenu du message est vide.<br/><br/>Voulez-vous néanmoins envoyer le message ?"
    },
    "VIEW": {
      "TITLE": "Message",
      "SENDER": "Envoyé par",
      "RECIPIENT": "Envoyé à",
      "NO_CONTENT": "Message vide",
      "DELETE": "Supprimer le message"
    },
    "CONFIRM": {
      "REMOVE": "Êtes-vous sûr de vouloir <b>supprimer ce message</b> ?<br/><br/>Cette opération est irréversible.",
      "REMOVE_ALL" : "Êtes-vous sûr de vouloir <b>supprimer tous les messages</b> ?<br/><br/>Cette opération est irréversible.",
      "MARK_ALL_AS_READ": "Êtes-vous sûr de vouloir <b>marquer tous les messages comme lus</b> ?",
      "USER_HAS_NO_PROFILE": "Cette identité n'a aucun profil Cesium+. Il se peut qu'elle n'utilise pas l'extension Cesium+, et <b>ne consultera donc pas votre message</b>.<br/><br/>Êtes-vous sûr de vouloir <b>continuer</b> malgré tout ?"
    },
    "INFO": {
      "MESSAGE_REMOVED": "Message supprimé",
      "All_MESSAGE_REMOVED": "Tous les messages ont été supprimés",
      "MESSAGE_SENT": "Message envoyé"
    },
    "ERROR": {
      "SEND_MSG_FAILED": "Erreur lors de l'envoi du message.",
      "LOAD_MESSAGES_FAILED": "Erreur lors de la récupération des messages.",
      "LOAD_MESSAGE_FAILED": "Erreur lors de la récupération du message.",
      "MESSAGE_NOT_READABLE": "Lecture du message impossible.",
      "USER_NOT_RECIPIENT": "Vous n'êtes pas le destinataire de ce message : déchiffrement impossible.",
      "NOT_AUTHENTICATED_MESSAGE": "L'authenticité du message est douteuse ou son contenu est corrompu.",
      "REMOVE_MESSAGE_FAILED": "Erreur de suppression du message",
      "MESSAGE_CONTENT_TOO_LONG": "Valeur trop longue ({{maxLength}} caractères max).",
      "MARK_AS_READ_FAILED": "Impossible de marquer le message comme 'lu'.",
      "LOAD_NOTIFICATIONS_FAILED": "Erreur lors de la récupération des notifications de messages.",
      "REMOVE_All_MESSAGES_FAILED": "Erreur lors de la suppression de tous les messages.",
      "MARK_ALL_AS_READ_FAILED": "Erreur lors du marquage des messages comme lus.",
      "RECIPIENT_IS_MANDATORY": "Le destinataire est obligatoire."
    }
  },
  "BLOCKCHAIN": {
    "LOOKUP": {
      "SEARCH_HELP": "Numéro de bloc, hash, clé publique, etc.",
      "POPOVER_FILTER_TITLE": "Filtres",
      "HEADER_MEDIAN_TIME": "Date / Heure",
      "HEADER_BLOCK": "Bloc #",
      "HEADER_ISSUER": "Noeud émetteur",
      "BTN_LAST": "Derniers blocs",
      "BTN_TX": "Transactions",
      "DISPLAY_QUERY": "Afficher la requête",
      "HIDE_QUERY": "Masquer la requête",
      "TX_SEARCH_FILTER": {
        "MEMBER_FLOWS": "<b class=\"ion-person\"></b> Entrées/sorties de membres",
        "EXISTING_TRANSACTION": "<b class=\"ion-card\"></b> Avec transactions",
        "PERIOD": "<b class=\"ion-clock\"></b> Entre <b class=\"gray\">{{params[1]|medianDateShort}}</b> ({{params[1]|medianTime}}) et <b class=\"gray\">{{params[2]|medianDateShort}}</b> ({{params[2]|medianTime}})",
        "ISSUER": "<b class=\"ion-android-desktop\"></b> Calculé par <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}",
        "TX_PUBKEY": "<b class=\"ion-card\"></b> Transactions concernant <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}"
      }
    },
    "ERROR": {
      "SEARCH_BLOCKS_FAILED": "Erreur de la recherche des blocs."
    }
  },
  "GROUP": {
    "GENERAL_DIVIDER": "Informations générales",
    "LOCATION_DIVIDER": "Adresse",
    "SOCIAL_NETWORKS_DIVIDER": "Réseaux sociaux et site web",
    "TECHNICAL_DIVIDER": "Informations techniques",
    "CREATED_TIME": "Créé {{creationTime|formatFromNow}}",
    "NOTIFICATIONS": {
      "TITLE": "Invitations"
    },
    "LOOKUP": {
      "TITLE": "Groupes",
      "SEARCH_HELP": "Nom de groupe, mots, lieu, etc.",
      "LAST_RESULTS_LIST": "Nouveaux groupes :",
      "OPEN_RESULTS_LIST": "Groupes ouverts :",
      "MANAGED_RESULTS_LIST": "Groupes fermés :",
      "BTN_LAST": "Nouveaux groupes",
      "BTN_NEW": "J'ajoute un groupe"
    },
    "TYPE": {
      "TITLE": "Nouveau groupe",
      "SELECT_TYPE": "Type de groupe :",
      "OPEN_GROUP": "Groupe ouvert",
      "OPEN_GROUP_HELP": "Un groupe ouvert est accessible par n'importe quel membre de la monnaie.",
      "MANAGED_GROUP": "Groupe administré",
      "MANAGED_GROUP_HELP": "un groupe administré est géré par des administrateurs et des modérateurs, qui peuvent accepter, refuser ou exclure un membre en son sein.",
      "ENUM": {
        "OPEN": "Groupe ouvert",
        "MANAGED": "Groupe administré"
      }
    },
    "VIEW": {
      "POPOVER_SHARE_TITLE": "{{title}}",
      "MENU_TITLE": "Options",
      "REMOVE_CONFIRMATION" : "Êtes-vous sûr de vouloir supprimer ce groupe ?<br/><br/>Cette opération est irréversible."
    },
    "EDIT": {
      "TITLE": "Groupe",
      "TITLE_NEW": "Nouveau groupe",
      "RECORD_TITLE": "Titre",
      "RECORD_TITLE_HELP": "Titre",
      "RECORD_DESCRIPTION": "Description",
      "RECORD_DESCRIPTION_HELP": "Description"
    },
    "ERROR": {
      "SEARCH_GROUPS_FAILED": "Échec de la recherche de groupes",
      "REMOVE_RECORD_FAILED": "Erreur de la suppression du groupe"
    },
    "INFO": {
      "RECORD_REMOVED" : "Groupe supprimé"
    }
  },
  "REGISTRY": {
    "CATEGORY": "Activité principale",
    "GENERAL_DIVIDER": "Informations générales",
    "LOCATION_DIVIDER": "Adresse",
    "SOCIAL_NETWORKS_DIVIDER": "Réseaux sociaux et site web",
    "TECHNICAL_DIVIDER": "Informations techniques",
    "BTN_SHOW_WOT": "Personnes",
    "BTN_SHOW_WOT_HELP": "Rechercher des personnes",
    "BTN_SHOW_PAGES": "Pages",
    "BTN_SHOW_PAGES_HELP": "Rechercher des pages",
    "BTN_NEW": "Créer une page",
    "MY_PAGES": "Mes pages",
    "NO_PAGE": "Aucune page",
    "SEARCH": {
      "TITLE": "Pages",
      "SEARCH_HELP": "Quoi, Qui : restaurant, Chez Marcel, ...",
      "BTN_ADD": "Nouveau",
      "BTN_LAST_RECORDS": "Pages récentes",
      "BTN_ADVANCED_SEARCH": "Recherche avancée",
      "BTN_OPTIONS": "Recherche avancée",
      "TYPE": "Type de page",
      "LOCATION_HELP": "Où : Code postal, Ville",
      "RESULTS": "Résultats",
      "RESULT_COUNT_LOCATION": "{{count}} résultat{{count>0?'s':''}}, près de {{location}}",
      "RESULT_COUNT": "{{count}} résultat{{count>0?'s':''}}",
      "LAST_RECORDS": "Pages récentes",
      "LAST_RECORD_COUNT_LOCATION": "{{count}} page{{count>0?'s':''}} récente{{count>0?'s':''}}, près de {{location}}",
      "LAST_RECORD_COUNT": "{{count}} page{{count>0?'s':''}} récente{{count>0?'s':''}}",
      "POPOVER_FILTERS": {
        "BTN_ADVANCED_SEARCH": "Options avancées ?"
      }
    },
    "VIEW": {
      "TITLE": "Annuaire",
      "CATEGORY": "Activité principale :",
      "LOCATION": "Adresse :",
      "MENU_TITLE": "Options",
      "POPOVER_SHARE_TITLE": "{{title}}",
      "REMOVE_CONFIRMATION" : "Êtes-vous sûr de vouloir supprimer cette page ?<br/><br/>Cette opération est irréversible."
    },
    "TYPE": {
      "TITLE": "Types",
      "SELECT_TYPE": "Type de page :",
      "ENUM": {
        "SHOP": "Commerce local",
        "COMPANY": "Entreprise",
        "ASSOCIATION": "Association",
        "INSTITUTION": "Institution"
      }
    },
    "EDIT": {
      "TITLE": "Edition",
      "TITLE_NEW": "Nouvelle page",
      "RECORD_TYPE":"Type de page",
      "RECORD_TITLE": "Nom",
      "RECORD_TITLE_HELP": "Nom",
      "RECORD_DESCRIPTION": "Description",
      "RECORD_DESCRIPTION_HELP": "Description de l'activité",
      "RECORD_ADDRESS": "Rue",
      "RECORD_ADDRESS_HELP": "Rue, bâtiment...",
      "RECORD_CITY": "Ville",
      "RECORD_CITY_HELP": "Ville",
      "RECORD_SOCIAL_NETWORKS": "Réseaux sociaux et site web",
      "RECORD_PUBKEY": "Clé publique",
      "RECORD_PUBKEY_HELP": "Clé publique de réception des paiements"
    },
    "WALLET": {
      "PAGE_DIVIDER": "Pages",
      "PAGE_DIVIDER_HELP": "Les pages référencent des activités acceptant la monnaie ou la favorisant : commerces, entreprises, associations, institutions. Elles sont stockées en dehors du réseau de la monnaie, dans <a ui-sref=\"app.es_network\">le réseau des nœuds Cesium+</a>."
    },
    "ERROR": {
      "LOAD_CATEGORY_FAILED": "Erreur de chargement de la liste des activités",
      "LOAD_RECORD_FAILED": "Erreur lors du chargement de la page",
      "LOOKUP_RECORDS_FAILED": "Erreur lors de l'exécution de la recherche",
      "REMOVE_RECORD_FAILED": "Erreur lors de la suppression de la page",
      "SAVE_RECORD_FAILED": "Erreur lors de la sauvegarde",
      "RECORD_NOT_EXISTS": "Page inexistante",
      "GEO_LOCATION_NOT_FOUND": "Ville ou code postal non trouvé"
    },
    "INFO": {
      "RECORD_REMOVED" : "Page supprimée",
      "RECORD_SAVED": "Page sauvegardée"
    }
  },
  "PROFILE": {
    "PROFILE_DIVIDER": "Profil Cesium+",
    "PROFILE_DIVIDER_HELP": "Il s'agit de données annexes, optionnelles. Elles sont stockées en dehors du réseau de la monnaie, dans <a ui-sref=\"app.es_network\">le réseau Cesium+</a>.",
    "NO_PROFILE_DEFINED": "Aucun profil saisi",
    "BTN_ADD": "Saisir mon profil",
    "BTN_EDIT": "Editer mon profil",
    "BTN_DELETE": "Supprimer mon profil",
    "BTN_REORDER": "Réordonner",
    "UID": "Pseudonyme",
    "TITLE": "Nom, Prénom",
    "TITLE_HELP": "Nom, Prénom",
    "DESCRIPTION": "A propos de moi",
    "DESCRIPTION_HELP": "A propos de moi...",
    "SOCIAL_HELP": "http://...",
    "GENERAL_DIVIDER": "Informations générales",
    "SOCIAL_NETWORKS_DIVIDER": "Réseaux sociaux, sites web",
    "TECHNICAL_DIVIDER": "Informations techniques",
    "MODAL_AVATAR": {
      "TITLE": "Photo de profil",
      "SELECT_FILE_HELP": "Veuillez <b>choisir le fichier image</b> :",
      "BTN_SELECT_FILE": "Choisir une photo",
      "RESIZE_HELP": "<b>Recadrez l'image</b>, si besoin. Un clic maintenu sur l'image permet de la déplacer. Cliquez sur la zone en bas à gauche pour zoomer.",
      "RESULT_HELP": "<b>Voici le résultat</b> tel que visible sur votre profil :"
    },
    "CONFIRM": {
      "DELETE": "Êtes-vous sûr de vouloir <b>supprimer votre profil Cesium+ ?</b><br/><br/>Cette opération est irréversible.",
      "DELETE_BY_MODERATOR": "Êtes-vous sûr de vouloir <b>supprimer ce profil Cesium+ ?</b><br/><br/>Cette opération est irréversible."
    },
    "ERROR": {
      "REMOVE_PROFILE_FAILED": "Erreur de suppression du profil",
      "LOAD_PROFILE_FAILED": "Erreur de chargement du profil",
      "SAVE_PROFILE_FAILED": "Erreur lors de la sauvegarde",
      "DELETE_PROFILE_FAILED": "Erreur lors de la suppression du profil",
      "INVALID_SOCIAL_NETWORK_FORMAT": "Format non pris en compte : veuillez indiquer une adresse valide.<br/><br/>Exemples :<ul><li>- Une page Facebook (https://www.facebook.com/user)</li><li>- Une page web (http://www.monsite.fr)</li><li>- Une adresse email (joe@dalton.com)</li></ul>",
      "IMAGE_RESIZE_FAILED": "Erreur lors du redimensionnement de l'image"
    },
    "INFO": {
      "PROFILE_REMOVED": "Profil supprimé",
      "PROFILE_SAVED": "Profil sauvegardé"
    },
    "HELP": {
      "WARNING_PUBLIC_DATA": "Les informations renseignées dans votre profil <b>sont publiques</b> : visibles y compris par des personnes <b>non connectées</b>.<br/>{{'PROFILE.PROFILE_DIVIDER_HELP'|translate}}"
    }
  },
  "LOCATION": {
    "BTN_GEOLOC_ADDRESS": "Trouver mon adresse sur la carte",
    "USE_GEO_POINT": "Apparaître sur les cartes {{'COMMON.APP_NAME'|translate}} ?",
    "LOADING_LOCATION": "Recherche de l'adresse...",
    "LOCATION_DIVIDER": "Adresse",
    "ADDRESS": "Rue",
    "ADDRESS_HELP": "Rue, complément d'adresse...",
    "CITY": "Ville",
    "CITY_HELP": "Code postal, Ville, Pays",
    "DISTANCE": "Distance maximale autour de la ville",
    "DISTANCE_UNIT": "km",
    "DISTANCE_OPTION": "{{value}} {{'LOCATION.DISTANCE_UNIT'|translate}}",
    "SEARCH_HELP": "Code postal, Ville",
    "PROFILE_POSITION": "Position du profil",
    "MODAL": {
      "TITLE": "Recherche de l'adresse",
      "SEARCH_HELP": "Ville, Code postal, Pays",
      "ALTERNATIVE_RESULT_DIVIDER": "Résultats alternatifs pour <b>{{address}}</b> :",
      "POSITION": "Lat/Lon : {{lat}}/{{lon}}"
    },
    "ERROR": {
      "CITY_REQUIRED_IF_STREET": "Champ obligatoire (car une rue est saisie)",
      "REQUIRED_FOR_LOCATION": "Champ obligatoire pour apparaître sur la carte",
      "INVALID_FOR_LOCATION": "Adresse inconnue",
      "GEO_LOCATION_FAILED": "Impossible de récupérer votre position. Veuillez utiliser le bouton de recherche.",
      "ADDRESS_LOCATION_FAILED": "Impossible de récupérer la position à partir de l'adresse"
    }
  },
  "SUBSCRIPTION": {
    "SUBSCRIPTION_DIVIDER": "Services en ligne",
    "SUBSCRIPTION_DIVIDER_HELP": "Les services en ligne offrent des services supplémentaires et optionnels, délégués à un perstataire de votre choix. Par exemple, pour recevoir les notifications de paiement par email.",
    "BTN_ADD": "Ajouter un service",
    "BTN_EDIT": "Gérer mes services",
    "NO_SUBSCRIPTION": "Aucun service utilisé",
    "SUBSCRIPTION_COUNT": "Services / Abonnements",
    "EDIT": {
      "TITLE": "Services en ligne",
      "HELP_TEXT": "Gérez ici vos abonnements et autres services en ligne",
      "PROVIDER": "Prestataire :"
    },
    "TYPE": {
      "ENUM": {
        "EMAIL": "Recevoir les notifications par email"
      }
    },
    "CONFIRM": {
      "DELETE_SUBSCRIPTION": "Êtes-vous sûr de vouloir <b>supprimer cet abonnement</b> ?"
    },
    "ERROR": {
      "LOAD_SUBSCRIPTIONS_FAILED": "Erreur lors du chargement des services en ligne",
      "ADD_SUBSCRIPTION_FAILED": "Erreur de l'envoi de l'abonnement",
      "UPDATE_SUBSCRIPTION_FAILED": "Erreur de la mise à jour de l'abonnement",
      "DELETE_SUBSCRIPTION_FAILED": "Erreur lors de la suppression de l'abonnement"
    },
    "MODAL_EMAIL": {
      "TITLE": "Notification par email",
      "HELP": "Remplissez ce formulaire pour <b>être notifié par email</b> des événements de votre compte.<br/>Votre adresse email sera chiffrée pour n'être visible que par le prestataire de service.",
      "EMAIL_LABEL": "Votre email :",
      "EMAIL_HELP": "jean.dupond@domaine.com",
      "FREQUENCY_LABEL": "Fréquence des notifications :",
      "FREQUENCY_DAILY": "Journalier",
      "FREQUENCY_WEEKLY": "Hebdomadaire",
      "PROVIDER": "Prestataire du service :"
    }
  },
  "DOCUMENT": {
    "HASH": "Hash : ",
    "LOOKUP": {
      "TITLE": "Recherche de documents",
      "BTN_ACTIONS": "Actions",
      "SEARCH_HELP": "issuer:AAA*, time:1508406169",
      "LAST_DOCUMENTS_DOTS": "Derniers documents :",
      "LAST_DOCUMENTS": "Derniers documents",
      "SHOW_QUERY": "Voir la requête",
      "HIDE_QUERY": "Masquer la requête",
      "HEADER_TIME": "Date/Heure",
      "HEADER_ISSUER": "Emetteur",
      "HEADER_RECIPIENT": "Destinataire",
      "HEADER_AMOUNT": "Montant",
      "READ": "Lu",
      "BTN_REMOVE": "Supprimer ce document",
      "BTN_COMPACT": "Compacter",
      "HAS_CREATE_OR_UPDATE_PROFILE": "a créé ou modifié son profil",
      "POPOVER_ACTIONS": {
        "TITLE": "Actions",
        "REMOVE_ALL": "Supprimer ces documents..."
      }
    },
    "INFO": {
      "REMOVED": "Document supprimé"
    },
    "CONFIRM": {
      "REMOVE": "Êtes-vous sûr de vouloir <b>supprimer ce document</b> ?",
      "REMOVE_ALL": "Êtes-vous sûr de vouloir <b>supprimer ces documents</b> ?"
    },
    "ERROR": {
      "LOAD_DOCUMENTS_FAILED": "Erreur lors de la recherche de documents",
      "REMOVE_FAILED": "Erreur lors de la suppression du document",
      "REMOVE_ALL_FAILED": "Erreur lors de la suppression des documents"
    }
  },
  "ES_SETTINGS": {
    "PLUGIN_NAME": "Cesium+",
    "PLUGIN_NAME_HELP": "Profils, notifications, messages privés",
    "ENABLE_TOGGLE": "Activer l'extension ?",
    "ENABLE_REMOTE_STORAGE": "Activer le stockage distant ?",
    "ENABLE_REMOTE_STORAGE_HELP": "Permet de stockage (chiffré) de vos paramètres sur les noeuds Cesium+",
    "ENABLE_MESSAGE_TOGGLE": "Activer les messages privés ?",
    "PEER": "Nœud de données Cesium+",
    "POPUP_PEER": {
      "TITLE" : "Nœud de données",
      "HELP" : "Saisissez l'adresse du nœud que vous voulez utiliser :",
      "PEER_HELP": "serveur.domaine.com:port"
    },
    "NOTIFICATIONS": {
      "DIVIDER": "Notifications",
      "HELP_TEXT": "Activez les types de notifications que vous souhaitez recevoir :",
      "ENABLE_TX_SENT": "Notifier les <b>paiements émis</b> ?",
      "ENABLE_TX_RECEIVED": "Notifier les <b>paiements reçus</b> ?",
      "ENABLE_CERT_SENT": "Notifier les <b>certifications émises</b> ?",
      "ENABLE_CERT_RECEIVED": "Notifier les <b>certifications reçues</b> ?",
      "ENABLE_HTML5_NOTIFICATION": "Avertir à chaque nouvelle notification ?",
      "ENABLE_HTML5_NOTIFICATION_HELP": "Ouvre une petite fenêtre à chaque nouvelle notification."
    },
    "CONFIRM": {
      "ASK_ENABLE_TITLE": "Fonctionnalités optionnelles",
      "ASK_ENABLE": "L'extension Cesium+ est <b>désactivée</b> dans vos paramètres, rendant inactives les fonctionnalités : <ul><li>&nbsp;&nbsp;<b><i class=\"icon ion-person\"></i> Profils Cesium+</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-android-notifications\"></i> Notifications</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-email\"></i> Messages privés</b>.<li>&nbsp;&nbsp;<b><i class=\"icon ion-location\"></i> Cartes, etc.</b>.</ul><br/><b>Souhaitez-vous ré-activer</b> l'extension ?"
    }
  },
  "ES_WALLET": {
    "ERROR": {
      "RECIPIENT_IS_MANDATORY": "Un destinataire est obligatoire pour le chiffrement."
    }
  },
  "ES_PEER": {
    "NAME": "Nom",
    "DOCUMENTS": "Documents",
    "SOFTWARE": "Logiciel",
    "DOCUMENT_COUNT": "Nombre de documents",
    "EMAIL_SUBSCRIPTION_COUNT": "{{emailSubscription}} abonné{{emailSubscription ? 's' : ''}} aux notifications par email"
  },
  "EVENT": {
    "NODE_STARTED": "Votre noeud ES API <b>{{params[0]}}</b> est démarré",
    "NODE_BMA_DOWN": "Le noeud <b>{{params[0]}}:{{params[1]}}</b> (utilisé par votre noeud ES API) est <b>injoignable</b>.",
    "NODE_BMA_UP": "Le noeud <b>{{params[0]}}:{{params[1]}}</b> est à nouveau accessible.",
    "MEMBER_JOIN": "Vous êtes maintenant <b>membre</b> de la monnaie <b>{{params[0]}}</b> !",
    "MEMBER_LEAVE": "Vous n'êtes <b>plus membre</b> de la monnaie <b>{{params[0]}}</b> !",
    "MEMBER_EXCLUDE": "Vous n'êtes <b>plus membre</b> de la monnaie <b>{{params[0]}}</b>, faute de non renouvellement ou par manque de certifications.",
    "MEMBER_REVOKE": "La révocation de votre compte a été effectuée. Il ne pourra plus être un compte membre de la monnaie <b>{{params[0]}}</b>.",
    "MEMBER_ACTIVE": "Votre renouvellement d'adhésion à la monnaie <b>{{params[0]}}</b> a été <b>pris en compte</b>.",
    "TX_SENT": "Votre <b>paiement</b> à <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> a été effectué.",
    "TX_SENT_MULTI": "Votre <b>paiement</b> à <b>{{params[1]}}</b> a été effectué.",
    "TX_RECEIVED": "Vous avez <b>reçu un paiement</b> de <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "TX_RECEIVED_MULTI": "Vous avez <b>reçu un paiement</b> de <b>{{params[1]}}</b>.",
    "CERT_SENT": "Votre <b>certification</b> à <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> a été effectuée.",
    "CERT_RECEIVED": "Vous avez <b>reçu une certification</b> de <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "USER": {
      "LIKE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> aime votre profil",
      "FOLLOW_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> suit votre activité",
      "STAR_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> vous a noté ({{params[3]}} <b class=\"ion-star\">)",
      "MODERATION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> vous demande une modération sur le profil : <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "DELETION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> demande la suppression d'un profil : <b>{{params[2]}}</b>",
      "ABUSE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> a signalé votre profil"
    },
    "PAGE": {
      "NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> a commenté votre page : <b>{{params[2]}}</b>",
      "UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> a modifié son commentaire sur votre page : <b>{{params[2]}}</b>",
      "NEW_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> a répondu à votre commentaire sur la page : <b>{{params[2]}}</b>",
      "UPDATE_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> a modifié sa réponse à votre commentaire sur la page : <b>{{params[2]}}</b>",
      "FOLLOW_NEW_COMMENT": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> a commenté la page : <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE_COMMENT": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> a modifié son commentaire sur la page : <b>{{params[2]}}</b>",
      "FOLLOW_NEW": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> a ajouté la page : <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> a modifié la page : <b>{{params[2]}}</b>",
      "MODERATION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> vous demande une modération sur la page : <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "DELETION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> a signalé une page à supprimer : <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
      "ABUSE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> a signalé votre page : <b>{{params[2]}}</b>"
    }
  },
  "LIKE": {
    "ERROR": {
      "FAILED_TOGGLE_LIKE": "Impossible d'exécuter cette action."
    }
  },
  "CONFIRM": {
    "ES_USE_FALLBACK_NODE": "Nœud de données <b>{{old}}</b> injoignable ou adresse invalide.<br/><br/>Voulez-vous temporairement utiliser le nœud de données <b>{{new}}</b> ?"
  },
  "ERROR": {
    "ES_CONNECTION_ERROR": "Nœud de données <b>{{server}}</b> injoignable ou adresse invalide.<br/><br/>Cesium va continuer à fonctionner, <b>sans l'extension Cesium+</b> (profils utilisateur, messages privés, cartes et graphiques)<br/><br/>Vérifiez votre connexion Internet, ou changez de nœud de données dans les <a class=\"positive\" ng-click=\"doQuickFix('settings')\">paramètres de l'extension</a>.",
    "ES_MAX_UPLOAD_BODY_SIZE": "Le volume des données à envoyer dépasse la limite fixée par le serveur.<br/>Veuillez ré-essayer après avoir, par exemple, supprimer des photos."
  },
  "ADMIN": {
    "MESSAGE": {
      "BTN_SEND_EMAIL": "Envoyer un email groupé"
    },
    "LOG": {
      "BTN_SHOW_LOG": "Logs des IP bloquées",
      "VIEW": {
        "TITLE": "Logs",
        "LOG_REQUEST_DIVIDER": "Liste des requêtes bloquées"
      }
    },
    "ERROR": {
      "NO_SSL_ACCESS": "Impossible d'accéder à cause des restriction SSL.",
      "LOAD_LOG_FAILED": "Erreur de lecture des logs impossible.",
      "AUTH_ERROR": "Erreur d'authentification au pod"
    }
  }
}
);

$translateProvider.translations("it-IT", {
   "COMMON": {
     "CATEGORY": "Categoria",
     "CATEGORIES": "Categorie",
     "CATEGORY_SEARCH_HELP": "Ricerca",
     "LAST_MODIFICATION_DATE": "Aggiornato il ",
     "SUBMIT_BY": "Inviato da",
     "BTN_PUBLISH": "Pubblicare",
     "BTN_PICTURE_DELETE": "Eliminare",
     "BTN_PICTURE_FAVORISE": "Predefinito",
     "BTN_PICTURE_ROTATE": "Girare",
     "BTN_ADD_PICTURE": "Aggiungere foto",
     "NOTIFICATIONS": {
       "TITLE": "Notifiche",
       "MARK_ALL_AS_READ": "Segna tutte come lette",
       "NO_RESULT": "Nessuna notifica",
       "SHOW_ALL": "Mostrare tutte",
       "LOAD_NOTIFICATIONS_FAILED": "Impossibile caricare le notifiche"
     }
   },
   "MENU": {
     "REGISTRY": "Pagine",
     "USER_PROFILE": "Mio profilo",
     "MESSAGES": "Messaggi",
     "NOTIFICATIONS": "Notifiche",
     "INVITATIONS": "Inviti"
   },
   "ACCOUNT": {
     "NEW": {
       "ORGANIZATION_ACCOUNT": "Conto per enti",
       "ORGANIZATION_ACCOUNT_HELP": "Se rappresenta un'azienda, un'associazione, etc.<br/>Questo conto non potrà creare il Dividendo Universale."
     },
     "EVENT": {
       "MEMBER_WITHOUT_PROFILE": "Per ottenere sue certificazioni più velocemente, riempire le informazioni<a ui-sref=\"app.edit_profile\">del suo profilo</a>. I membri della rete si fidano più volontieri di profili verificabili."
     },
     "ERROR": {
       "WS_CONNECTION_FAILED": "Cesium non può ricevere notifiche a causa di un problema tecnico (di conessione al data node Cesium+).<br/><br/>Se il problema persiste, le chiediamo di <b>scegliere un'altro data node</b> nelle impostazioni di Cesium+."
     }
   },
   "WOT": {
     "BTN_SUGGEST_CERTIFICATIONS_DOTS": "Suggerire delle identità da certificare..",
     "BTN_ASK_CERTIFICATIONS_DOTS": "Chiedere una certificazione ai membri...",
     "BTN_ASK_CERTIFICATION": "Chiedere una certificazione",
     "SUGGEST_CERTIFICATIONS_MODAL": {
       "TITLE": "Suggerire delle certificazioni",
       "HELP": "Scegliere i suoi suggerimenti, aiuti"
     },
     "ASK_CERTIFICATIONS_MODAL": {
       "TITLE": "Chiedere delle certificazioni",
       "HELP": "Selezionare un ricevente"
     },
     "SEARCH": {
      "DIVIDER_PROFILE": "Conti",
      "DIVIDER_PAGE": "Pagine",
      "DIVIDER_GROUP": "Gruppi"
    },
     "CONFIRM": {
       "SUGGEST_CERTIFICATIONS": "E’ sicuro(a) di voler <b>suggerire queste certificazioni</b> ?",
       "ASK_CERTIFICATION": " E’ sicuro(a) di voler  <b>chiedere una certificazione</b> ?",
       "ASK_CERTIFICATIONS": " E’ sicuro(a) di voler <b>chiedere una certificazione</b> a questi membri ?"
     }
   },
  "INVITATION": {
    "TITLE": "Invitazioni",
    "NO_RESULT": "Nessun invito ricevuto",
    "BTN_DELETE_ALL": "Cancellare tutti gli inviti",
    "BTN_DELETE": "Cancellare l'invito",
    "BTN_NEW_INVITATION": "Nuovo invito",
    "ASK_CERTIFICATION": "<a href=\"#/app/wot/{{pubkey}}/{{::uid}}\">{{::name||uid}}</a> ti ha chiesto una certificazione",
    "SUGGESTION_CERTIFICATION": "<a href=\"#/app/wot/{{::pubkey}}/{{::uid}}\">{{::name||uid}}</a> è proposto per ricevere una certificazione",
    "SUGGESTED_BY": "Suggerimento inviato da by <a class=\"positive\" href=\"#/app/wot/{{::issuer.pubkey}}/{{::issuer.uid}}\">{{::issuer.name||issuer.uid}}</a>",
    "NOTIFICATIONS": {
      "TITLE": "Inviti"
    },
    "LIST": {
      "TITLE": "Inviti"
    },
    "NEW": {
      "TITLE": "Nuovo invito",
      "RECIPIENTS": "A",
      "RECIPIENTS_HELP": "Riceventi dell'invito",
      "RECIPIENTS_MODAL_TITLE": "Riceventi",
      "RECIPIENTS_MODAL_HELP": "Scegliere riceventi:",
      "SUGGESTION_IDENTITIES": "Suggerimenti di identità da certificare",
      "SUGGESTION_IDENTITIES_HELP": "Suggerimenti di certificazioni",
      "SUGGESTION_IDENTITIES_MODAL_TITLE": "Suggerimenti",
      "SUGGESTION_IDENTITIES_MODAL_HELP": "Scegli tuoi sugerimenti:"
    },
    "CONFIRM": {
      "DELETE_ALL_CONFIRMATION": "Cancellare degli inviti è <b>una operazione irreversibile</b>.<br/><br/><b>Sei sicuro/a</b> di voler proseguire",
      "SEND_INVITATIONS_TO_CERTIFY": "<b>Sei sicuro/a</b> di voler <b>inviare questo suggerimento di certificazione</b> ?"
    },
    "INFO": {
      "INVITATION_SENT": "Invito inviato"
    },
    "ERROR": {
      "LOAD_INVITATIONS_FAILED": "Errore nel caricare gli inviti",
      "REMOVE_INVITATION_FAILED": "Errore nel cancellare gli inviti",
      "REMOVE_ALL_INVITATIONS_FAILED": "Errore nel cancellare inviti",
      "SEND_INVITATION_FAILED": "Errore nel invio degli inviti",
      "BAD_INVITATION_FORMAT": "<span class=\"assertive\"><i class=\"ion-close-circled\"></i> Invito illegibile (formatto sconosciuto)</span> - inviato da <a ui-sref=\"app.wot_identity({pubkey: '{{::pubkey}}', uid: '{{::uid}}' })\">{{::name||uid}}</a>"
    }
  },
   "COMMENTS": {
     "DIVIDER": "Commenti",
     "SHOW_MORE_COMMENTS": "Mostrare commenti precedenti",
     "COMMENT_HELP": "Suo commento o domanda...",
     "COMMENT_HELP_REPLY_TO": "Sua risposta...",
     "BTN_SEND": "Inviare",
     "POPOVER_SHARE_TITLE": "Messaggio #{{number}}",
     "REPLY": "Rispondere",
     "REPLY_TO": "Rispondere a:",
     "REPLY_TO_LINK": "In risposta a",
     "REPLY_TO_DELETED_COMMENT": "In risposta ad un commento cancellato",
     "REPLY_COUNT": "{{replyCount}} risposte",
     "DELETED_COMMENT": "Commento cancellato",
     "ERROR": {
       "FAILED_SAVE_COMMENT": "Salvare il commento cancellato",
       "FAILED_REMOVE_COMMENT": "Cancellazione del commento fallita"
     }
   },
   "MESSAGE": {
     "REPLY_TITLE_PREFIX": "Re: ",
     "FORWARD_TITLE_PREFIX": "Fw: ",
     "BTN_REPLY": "Rispondere",
     "BTN_COMPOSE": "Nuovo messaggio",
     "BTN_WRITE": "Scrivere",
     "NO_MESSAGE_INBOX": "Nessun messaggio ricevuto",
     "NO_MESSAGE_OUTBOX": "Nessun messaggio inviato",
     "NOTIFICATIONS": {
       "TITLE": "Messaggi",
       "MESSAGE_RECEIVED": "Hai <b>ricevuto un messaggio/b><br/>da"
     },
     "LIST": {
       "INBOX": "In entrata",
       "OUTBOX": "In uscita",
       "TITLE": "Messaggi privati",
       "POPOVER_ACTIONS": {
         "TITLE": "Opzioni",
         "DELETE_ALL": "Eliminare tutti i messaggi"
       }
     },
     "COMPOSE": {
       "TITLE": "Nuovo messaggio",
       "TITLE_REPLY": "Rispondere",
       "SUB_TITLE": "Nuovo messaggio",
       "TO": "A",
       "OBJECT": "Oggetto",
       "OBJECT_HELP": "Oggetto",
       "ENCRYPTED_HELP": "La informiamo che questo messaggio verrà criptato prima della sua spedizione in modo che solo il destinatario lo possa leggere e essere sicuro che ne sia Lei l'autore.",
       "MESSAGE": "Messaggio",
       "MESSAGE_HELP": "Contenuto del messaggio",
       "CONTENT_CONFIRMATION": "Nessun contenuto. <br/><br/>E’ sicura di voler inviare questo messaggio?"
     },
     "VIEW": {
       "TITLE": "Messaggio",
       "SENDER": "Inviato da",
       "RECIPIENT": "Inviato a",
       "NO_CONTENT": "Messaggio vuoto",
       "DELETE": "Cancellare il messaggio"
     },
     "CONFIRM": {
       "REMOVE": "E’ sicuro/a di voler <b>eliminare il messaggio</b>?<br/><br/> Questa operazione è irreversibile.",
       "REMOVE_ALL": "E’ sicuro/a di voler <b>eliminare tutti i messaggi</b>?<br/><br/> Questa operazione è irreversibile.",
       "MARK_ALL_AS_READ": "E’ sicuro/a di voler <b>segnare tutti i messaggi come letti/b>?",
       "USER_HAS_NO_PROFILE": "Questa identità non ha un profilo Cesium+. Pertanto non può <b>leggere il suo messaggio</b>.<br/><br/>E’ sicuro/a di voler <b>continuare</b>?"
     },
     "INFO": {
       "MESSAGE_REMOVED": "Messaggio eliminato correttamente",
       "All_MESSAGE_REMOVED": "Messaggi eliminati correttamente",
       "MESSAGE_SENT": "Messaggio inviato"
     },
     "ERROR": {
       "SEND_MSG_FAILED": "Errore nella spedizione del messaggio.",
       "LOAD_MESSAGES_FAILED": "Errore durante il caricamento dei messaggi.",
       "LOAD_MESSAGE_FAILED": "Errore durante il caricamento del messaggio.",
       "MESSAGE_NOT_READABLE": "Impossibile leggere il messaggio.",
       "USER_NOT_RECIPIENT": "Lei non è il destinatario del messaggio: impossibile leggerlo.",
       "NOT_AUTHENTICATED_MESSAGE": "Impossibile verificare l'autenticità del messaggio o contenuto corrotto.",
       "REMOVE_MESSAGE_FAILED": "Errore avvenuto durante l'eliminazione del messaggio",
       "MESSAGE_CONTENT_TOO_LONG": "Il contenuto supera il limite ({{maxLength}} caratteri ammessi).",
       "MARK_AS_READ_FAILED": "Impossibile segnare il messaggio come 'letto'.",
       "LOAD_NOTIFICATIONS_FAILED": "Errore nel caricare le notifiche.",
       "REMOVE_All_MESSAGES_FAILED": "Errore avvenuto durante l'eliminazione dei messaggi.",
       "MARK_ALL_AS_READ_FAILED": "Errore avvenuto nel segnare i messaggi come 'letti",
       "RECIPIENT_IS_MANDATORY": "Destinatario obbligatorio"
     }
   },
   "BLOCKCHAIN": {
    "LOOKUP": {
      "SEARCH_HELP": "Numero di blocco, hash...",
      "POPOVER_FILTER_TITLE": "Filtro",
      "HEADER_MEDIAN_TIME": "Data / Ora",
      "HEADER_BLOCK": "Blocco #",
      "HEADER_ISSUER": "Peer proprietario",
      "BTN_LAST": "Ultimi blocchi",
      "DISPLAY_QUERY": "Visualizzare query",
      "HIDE_QUERY": "Nascondere query",
      "TX_SEARCH_FILTER": {
        "MEMBER_FLOWS": "<b class=\"ion-person\"></b> Input/output Membri",
        "EXISTING_TRANSACTION": "<b class=\"ion-card\"></b> hanno transazioni",
        "PERIOD": "<b class=\"ion-clock\"></b> Tra <b class=\"gray\">{{params[1]|medianDateShort}}</b> ({{params[1]|medianTime}}) e <b class=\"gray\">{{params[2]|medianDateShort}}</b> ({{params[2]|medianTime}})",
        "ISSUER": "<b class=\"ion-android-desktop\"></b> Calcolato da {{params[1]|formatPubkey}}",
        "TX_PUBKEY": "<b class=\"ion-card\"></b> Transazioni legate a  <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}"
      }
    },
    "ERROR": {
      "SEARCH_BLOCKS_FAILED": "Errore durante la ricerca dei blocchi."
    }
  },
  "GROUP": {
    "GENERAL_DIVIDER": "Informazioni generali",
    "LOCATION_DIVIDER": "Indirizzo",
    "SOCIAL_NETWORKS_DIVIDER": "Presenza sui social e/o sito web",
    "TECHNICAL_DIVIDER": "Dati tecnici",
    "CREATED_TIME": "Creato {{creationTime|formatFromNow}}",
    "NOTIFICATIONS": {
      "TITLE": "Inviti"
    },
    "LOOKUP": {
      "TITLE": "Gruppi",
      "SEARCH_HELP": "Nome del grupo, parole chiavi, luogo, etc.",
      "LAST_RESULTS_LIST": "Nuovi gruppi :",
      "OPEN_RESULTS_LIST": "Gruppi aperti :",
      "MANAGED_RESULTS_LIST": "Gruppi chiusi :",
      "BTN_LAST": "Nuovi gruppi",
      "BTN_NEW": "Aggiungo un grupo!"
    },
    "TYPE": {
      "TITLE": "Nuovo gruppo",
      "SELECT_TYPE": "Tipo di gruppo :",
      "OPEN_GROUP": "Gruppo aperto",
      "OPEN_GROUP_HELP": "Un gruppo aperto è accessibile da qualsiasi membro della moneta.",
      "MANAGED_GROUP": "Gruppo amministrato",
      "MANAGED_GROUP_HELP": "un gruppo amministrato è gestito da amministratori e moderatori, che possono accetare, rifiutare o escludere membri del gruppo.",
      "ENUM": {
        "OPEN": "Gruppo aperto",
        "MANAGED": "Gruppo amministrato"
      }
    },
    "VIEW": {
      "POPOVER_SHARE_TITLE": "{{title}}",
      "MENU_TITLE": "Opzioni",
      "REMOVE_CONFIRMATION" : "Sei sicuro/a di voler chiudere questo gruppo ?<br/><br/>Questa operazione è irreversibile."
    },
    "EDIT": {
      "TITLE": "Gruppo",
      "TITLE_NEW": "Nuovo gruppo",
      "RECORD_TITLE": "Titolo",
      "RECORD_TITLE_HELP": "Titolo",
      "RECORD_DESCRIPTION": "Descrizione",
      "RECORD_DESCRIPTION_HELP": "Descrizione"
    },
    "ERROR": {
      "SEARCH_GROUPS_FAILED": "Errore nella ricerca di gruppi",
      "REMOVE_RECORD_FAILED": "Errore nel chiudere il gruppo"
    },
    "INFO": {
      "RECORD_REMOVED" : "Gruppo eliminato"
    }
  },
   "REGISTRY": {
     "CATEGORY": "Attività principale",
     "GENERAL_DIVIDER": "Informazioni di base",
     "LOCATION_DIVIDER": "Indirizzo",
     "SOCIAL_NETWORKS_DIVIDER": "Presenza sui social, sito web",
     "TECHNICAL_DIVIDER": "Dati tecnici",
     "BTN_SHOW_WOT": "Persone",
     "BTN_SHOW_WOT_HELP": "Cercare persone",
     "BTN_SHOW_PAGES": "Pagine",
     "BTN_SHOW_PAGES_HELP": "Cercare pagine",
     "BTN_NEW": "Aggiungere",
     "MY_PAGES": "Mie pagine",
     "NO_PAGE": "Nessuna pagina",
     "SEARCH": {
         "TITLE": "Pagine",
         "SEARCH_HELP": "Che, chi: parrucchiere, Pizza Efisio, ...",
         "BTN_ADD": "Nuovo",
         "BTN_LAST_RECORDS": "Pagine recenti",
         "BTN_ADVANCED_SEARCH": "Ricerca avanzata",
         "BTN_OPTIONS": "Ricerca avanzata",
         "TYPE": "Tipo di ente",
         "LOCATION_HELP": "Città",
         "RESULTS": "Risultati",
         "RESULT_COUNT_LOCATION": "{{count}} risultato{{count>0?'i':''}}, vicino a {{location}}",
         "RESULT_COUNT": "{{count}} risultato{{count>0?'i':''}}",
         "LAST_RECORDS": "Pagine recenti:",
         "LAST_RECORD_COUNT_LOCATION": "{{count}} pagina{{count>0?'e':''}} recente{{count>0?'i':''}}, vicino a{{location}}",
         "LAST_RECORD_COUNT": "{{count}} pagina{{count>0?'e':''}} recente{{count>0?'i':''}}",
         "POPOVER_FILTERS": {
           "BTN_ADVANCED_SEARCH": "Opzioni avanzate ?"
         }
     },
     "VIEW": {
       "TITLE": "Annuario",
       "CATEGORY": "Attività principale:",
       "LOCATION": "Indirizzo:",
       "MENU_TITLE": "Impostazioni",
       "POPOVER_SHARE_TITLE": "{{title}}",
       "REMOVE_CONFIRMATION" : "E’ sicuro/a di voler eliminare questo riferimento?<br/><br/>Questa operazione è irreversibile."
     },
     "TYPE": {
       "TITLE": "Nuovo riferimento",
       "SELECT_TYPE": "Tipo di ente/organismo:",
       "ENUM": {
         "SHOP": "Negozi locali",
         "COMPANY": "Azienda",
         "ASSOCIATION": "Associazione",
         "INSTITUTION": "Istituto"
       }
     },
     "EDIT": {
       "TITLE": "Modificare",
       "TITLE_NEW": "Nuovo riferimento",
       "RECORD_TYPE": "Tipo di ente",
       "RECORD_TITLE": "Nome",
       "RECORD_TITLE_HELP": "Nome",
       "RECORD_DESCRIPTION": "Descrizione",
       "RECORD_DESCRIPTION_HELP": "Descrivere l'attività",
       "RECORD_ADDRESS": "Indirizzo",
       "RECORD_ADDRESS_HELP": "Indirizzo: Strada, numero civico...",
       "RECORD_CITY": "Città",
       "RECORD_CITY_HELP": "Città, Paese",
       "RECORD_SOCIAL_NETWORKS": "Presenza sui social e/o sito web",
       "RECORD_PUBKEY": "Chiave pubblica",
       "RECORD_PUBKEY_HELP": "Chiave pubblica per ricevere pagamenti"
     },
     "WALLET": {
      "PAGE_DIVIDER": "Pagine",
      "PAGE_DIVIDER_HELP": "Le pagine sono un elenco dei profesionisti che accettano o favoriscono: negozi, aziende, associazioni, istituti..."
    },
     "ERROR": {
       "LOAD_CATEGORY_FAILED": "Errore nel caricamento delle attività principali",
       "LOAD_RECORD_FAILED": "Caricamento fallito",
       "LOOKUP_RECORDS_FAILED": "Errore nel caricare i dati",
       "REMOVE_RECORD_FAILED": "Errore nella cancellazione",
       "SAVE_RECORD_FAILED": "Impossibile salvare",
       "RECORD_NOT_EXISTS": "Inesistente",
       "GEO_LOCATION_NOT_FOUND": "Città o CAP inesistente"
     },
     "INFO": {
       "RECORD_REMOVED" : "Pagina eliminata con successo.",
       "RECORD_SAVED": "Pagina salvata"
     }
   },
   "PROFILE": {
     "PROFILE_DIVIDER": "Profilo Cesium+",
     "PROFILE_DIVIDER_HELP": "Si tratta qui di data esterni, salvati fuori della rete della moneta.",
     "NO_PROFILE_DEFINED": "Nessun profilo Cesium+",
     "BTN_ADD": "Creare mio profilo",
     "BTN_EDIT": "Modificare mio profilo",
     "UID": "Pseudonimo",
     "TITLE": "Cognome, Nome",
     "TITLE_HELP": "Nome",
     "DESCRIPTION": "A proposito di me",
     "DESCRIPTION_HELP": "A proposito di me...",
     "SOCIAL_HELP": "http://...",
     "GENERAL_DIVIDER": "Informazioni generali",
     "SOCIAL_NETWORKS_DIVIDER": "Presenza sui social e sito/i web",
     "TECHNICAL_DIVIDER": "Dati tecnici",
     "MODAL_AVATAR": {
       "TITLE": "Avatar",
       "SELECT_FILE_HELP": "<b>Scegliere un'immagine</b>:",
       "BTN_SELECT_FILE": "Scegliere un'immagine",
       "RESIZE_HELP": "<b>Ritagliare l'immagine</b> se necessario. Cliccare sull'immagine permette di spostarla. Cliccando nella zona a basso sinistra permette di fare uno zoom-in.",
       "RESULT_HELP": "<b>Questo è il risultato</b> come visibile sul suo profilo:"
     },
     "ERROR": {
       "LOAD_PROFILE_FAILED": "Impossibile caricare il profilo dell’utente.",
       "SAVE_PROFILE_FAILED": "Impossibile salvare il profilo",
       "INVALID_SOCIAL_NETWORK_FORMAT": "Formatto scorretto: URL sbagliato.<br/><br/>Esempi: :<ul><li>- Una pagina Facebook (https://www.facebook.com/user)</li><li>- Un sito: (http://www.domain.com)</li><li>- Un indirizzo mail: (joe@dalton.com)</li></ul>",
       "IMAGE_RESIZE_FAILED": "Errore nel ritagliare l'immagine"
     },
     "INFO": {
       "PROFILE_SAVED": "Profilo salvato"
     },
     "HELP": {
       "WARNING_PUBLIC_DATA": "La informiamo che le informazioni qui pubblicate <b>sono pubbliche</b>: sono anche visibili <b>da gente non registrata/b>."
     }
   },
   "LOCATION": {
     "BTN_GEOLOC_ADDRESS": "Trovare mio indirizzo sulla mappa",
     "USE_GEO_POINT": "Geo-localizzare (raccomandato)?",
     "LOADING_LOCATION": "Cercando indirizzo...",
     "LOCATION_DIVIDER": "Posizione",
     "ADDRESS": "Indirizzo",
     "ADDRESS_HELP": "Indirizzo (opzionale)",
     "CITY": "Città",
     "CITY_HELP": "Città, Paese",
     "DISTANCE": "Distanza massimale intorno alla città",
     "DISTANCE_UNIT": "km",
     "DISTANCE_OPTION": "{{value}} {{'LOCATION.DISTANCE_UNIT'|translate}}",
     "SEARCH_HELP": "Città, CAP",
     "MODAL": {
       "TITLE": "Cercare indirizzo",
       "SEARCH_HELP": "Città, Paese",
       "ALTERNATIVE_RESULT_DIVIDER": "Risultati alternativi <b>{{address}}</b>:",
       "POSITION": "lat/lon : {{lat}} {{lon}}"
     },
     "ERROR": {
       "REQUIRED_FOR_LOCATION": "Campo obbligatorio per apparire sulla mappa",
       "INVALID_FOR_LOCATION": "Indirizzo sconosciuto",
       "GEO_LOCATION_FAILED": "Impossibile trovare sua posizione. Utilizzi il bottone di ricerca.",
       "ADDRESS_LOCATION_FAILED": "Indirizzo non trovato"
     }
   },
   "SUBSCRIPTION": {
    "SUBSCRIPTION_DIVIDER": "Servizi online",
    "SUBSCRIPTION_DIVIDER_HELP": "I servizi online offrono servizi addizionali opzionali, delegati ad terzi.",
    "BTN_ADD": "Aggiungere un servizio",
    "BTN_EDIT": "Gestire miei servizi",
    "NO_SUBSCRIPTION": "Nessun servizio utilizzato",
    "SUBSCRIPTION_COUNT": "Servizi/ Abbonamenti",
    "EDIT": {
      "TITLE": "Servizi online",
      "HELP_TEXT": "Qui si possono gestire gli abbonamenti e/o altri servizi online",
      "PROVIDER": "Prestatore :"
    },
    "TYPE": {
      "ENUM": {
        "EMAIL": "Ricevere notifiche per posta elettronica"
      }
    },
    "CONFIRM": {
      "DELETE_SUBSCRIPTION": "Sei sicuro/a di voler <b>cancellare questo abbonamento</b> ?"
    },
    "ERROR": {
      "LOAD_SUBSCRIPTIONS_FAILED": "Errore nel caricamento dei servizi online",
      "ADD_SUBSCRIPTION_FAILED": "Errore nel invio dell' abbonamento",
      "UPDATE_SUBSCRIPTION_FAILED": "Errore nel aggiornamento dell' abbonamento",
      "DELETE_SUBSCRIPTION_FAILED": "Errore nella cancellazine dell' abbonamento"
    },
    "MODAL_EMAIL": {
      "TITLE" : "Notifiche per posta elettronica",
      "HELP" : "Riempi questo formulario per <b>essere notificato/a per e-mail</b> degli eventi che avvengono sul tuo conto.<br/>Tuo indizzo mail sarà cifrato e solo il prestatore del servizio lo potrà vedere.",
      "EMAIL_LABEL" : "Tuo indirizzo mail :",
      "EMAIL_HELP": "cristiana.leonardi@dominio.com",
      "FREQUENCY_LABEL": "Frequenza delle notifiche :",
      "FREQUENCY_DAILY": "Quotidiano",
      "FREQUENCY_WEEKLY": "Settimanale",
      "PROVIDER": "Prestatore del servizio :"
    }
  },
  "DOCUMENT": {
    "HASH": "Hash: ",
    "LOOKUP": {
      "TITLE": "Ricerca di documenti",
      "BTN_ACTIONS": "Azioni",
      "SEARCH_HELP": "issuer:AAA*, time:1508406169",
      "LAST_DOCUMENTS": "Ultimi documenti",
      "SHOW_QUERY": "Visualizzare la richiesta",
      "HIDE_QUERY": "Nacondere la richiesta",
      "HEADER_TIME": "Data/Ora",
      "HEADER_ISSUER": "Emittente",
      "HEADER_RECIPIENT": "Destinatario",
      "READ": "Letto",
      "BTN_REMOVE": "Eliminare questo documento",
      "POPOVER_ACTIONS": {
        "TITLE": "Azioni",
        "REMOVE_ALL": "Eliminare questi documenti..."
      }
    },
    "INFO": {
      "REMOVED": "Documento eliminato"
    },
    "CONFIRM": {
      "REMOVE": "Sei sicuro/a di voler <b>eliminare questo documento</b> ?",
      "REMOVE_ALL": "Si sicuro/a di voler <b>eliminare questi documenti</b> ?"
    },
    "ERROR": {
      "LOAD_DOCUMENTS_FAILED": "Errore nella ricerca dei documenti",
      "REMOVE_FAILED": "Errore nell'eliminazione del documento",
      "REMOVE_ALL_FAILED": "Errore nell'eliminazione dei documenti"
    }
  },
   "ES_SETTINGS": {
     "PLUGIN_NAME": "Cesium+",
     "PLUGIN_NAME_HELP": "Profili di utenti, notifiche, messaggi privati",
     "ENABLE_TOGGLE": "Abilitare l'estensione ?",
     "ENABLE_MESSAGE_TOGGLE": "Abilitare i messaggi? privati",
     "ENABLE_SETTINGS_TOGGLE": "Abilitare stoccaggio su dispositivi esterni per le impostazioni?",
     "PEER": "Indirizzo di data peers",
     "POPUP_PEER": {
       "TITLE" : "Data peer",
       "HELP" : "Definire l'indirizzo da usare per il peer:",
       "PEER_HELP": "server.domain.com:port"
     },
     "NOTIFICATIONS": {
       "DIVIDER": "Notifiche",
       "HELP_TEXT": "Scegliere le notifiche che accetta ricevere:",
       "ENABLE_TX_SENT": "Notificarmi di <b>pagamenti inviati</b> con successo?",
       "ENABLE_TX_RECEIVED": "Notificarmi di <b>pagamenti in entrata</b>?",
       "ENABLE_CERT_SENT": "Notificarmi delle <b>certificazioni inviate</b>?",
       "ENABLE_CERT_RECEIVED": "Notificarmi di <b>certificazioni ricevute</b>?"
     },
     "CONFIRM": {
       "ASK_ENABLE_TITLE": "Nuove funzionalità",
       "ASK_ENABLE": "Sono disponibili nuove funzionalità: <ul><li>&nbsp;&nbsp;<b><i class=\"icon ion-person\"></i>Profili di utenti</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-android-notifications\"></i> Notifications</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-email\"></i> Messaggi privati/b>.</ul><br/>Sono state <b>disabilitate</b> nelle sue impostazioni.<br/><br/><b>Desidera abilitare</b> queste funzionalità?"
     }
   },
   "ES_WALLET": {
     "ERROR": {
       "RECIPIENT_IS_MANDATORY": "Un destinatario è necessario per il criptaggio."
     }
   },
   "EVENT": {
     "NODE_STARTED": "Suo nodo ES API <b>{{params[0]}}</b> è UP",
     "NODE_BMA_DOWN": "Nodo<b>{{params[0]}}:{{params[1]}}</b> (utilizzato dal suo ES API) è <b>indisponibile</b>.",
     "NODE_BMA_UP": "Nodo <b>{{params[0]}}:{{params[1]}}</b> è di nuovo attivo.",
     "MEMBER_JOIN": "E diventato <b>membro/a</b> della WoT della moneta <b>{{params[0]}}</b>!",
     "MEMBER_LEAVE": "Non <b>è più membro/a</b> della WoT della moneta <b>{{params[0]}}</b>!",
     "MEMBER_EXCLUDE": "Non è <b>più membro/a</b> della WoT della moneta<b>{{params[0]}}</b>, a causa di certificazioni non rinnovate o soglia di certificazioni non raggiunta.",
     "MEMBER_REVOKE": "Il suo conto è stato revocato. Da ora in poi non farà più parte della WoT. <b>{{params[0]}}</b>.",
     "MEMBER_ACTIVE": "La sua presenza nella WoT <b>{{params[0]}}</b> è stata <b>rinnovata correttamente</b>.",
     "TX_SENT": "Il suo pagamento <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> è stato eseguito.",
     "TX_SENT_MULTI": "Il suo pagamento <b>{{params[1]}}</b> è stato eseguito.",
     "TX_RECEIVED": "Ha ricevuto un pagamento da <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
     "TX_RECEIVED_MULTI": "Ha ricevuto un pagamento da <b>{{params[1]}}</b>.",
     "CERT_SENT": "Sua <b>certificazione</b> a favore di <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> è stata eseguita.",
     "CERT_RECEIVED": "Ha ricevuto <b>una certificazione</b> da parte di <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
     "PAGE": {
       "NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha scritto un commento sul suo riferimento: <b>{{params[2]}}</b>",
       "UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha modificato il suo commento sul suo riferimento: <b>{{params[2]}}</b>",
       "NEW_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha risposto al suo commento sul riferimento: <b>{{params[2]}}</b>",
       "UPDATE_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> ha modificato sua risposta sul suo commento a proposito del riferimento: <b>{{params[2]}}</b>"
     }
   },
   "CONFIRM": {
    "ES_USE_FALLBACK_NODE": "Nodo<b>{{old}}</b> irraggiungibile o indirizzo sbagliato.<br/><br/>Vuoi utilizzare temporaneamente il nodo di dati <b>{{new}}</b> ?"
  },
   "ERROR": {
    "ES_CONNECTION_ERROR": "Nodo di dati<b>{{server}}</b>  irraggiungibile o indirizzo sbagliato.<br/><br/>Cesium continuerà a funzionare <b>senza l'estensione Cesium+</b> (profili utenti, messaggi privati, mappe e grafici)<br/><br/>Verifica tua connessione o cambia nodo <a class=\"positive\" ng-click=\"doQuickFix('settings')\">impostazioni dell'estensione</a>.",
     "ES_MAX_UPLOAD_BODY_SIZE": "Il volume di data da inviare supera il limite imposto dal server.<br/><br/>Suggeriamo di riprovare dopo aver eliminato delle foto, per esempio."
   }
 }
);

$translateProvider.translations("nl-NL", {
  "COMMON": {
    "CATEGORY": "Categorie",
    "CATEGORIES": "Categorieën",
    "CATEGORY_SEARCH_HELP": "Zoeken",
    "LAST_MODIFICATION_DATE": "Vernieuwd op ",
    "SUBMIT_BY": "Ingediend door",
    "BTN_PUBLISH": "Publiceren",
    "BTN_PICTURE_DELETE": "Wissen",
    "BTN_PICTURE_FAVORISE": "Default",
    "BTN_ADD_PICTURE": "Afbeelding toevoegen",
    "NOTIFICATIONS": {
      "TITLE": "Notificaties",
      "MARK_ALL_AS_READ": "Markeer alles als gelezen",
      "NO_RESULT": "Geen berichten",
      "SHOW_ALL": "Toon alles",
      "LOAD_NOTIFICATIONS_FAILED": "Kan berichten niet laden"
    }
  },
  "MENU": {
    "REGISTRY": "Pagina's",
    "USER_PROFILE": "Mijn profiel",
    "MESSAGES": "Berichten"
  },
  "ACCOUNT": {
    "NEW": {
      "ORGANIZATION_ACCOUNT": "Ondernemingsrekening",
      "ORGANIZATION_ACCOUNT_HELP": "Als je een onderneming, vereniging etc. vertegenwoordigt.<br/>Deze rekening zal geen dividend créeren."
    },
    "EVENT": {
      "MEMBER_WITHOUT_PROFILE": "Vul <a ui-sref=\"app.edit_profile\"je gebruikersprofiel</a> in om sneller een certificering te verkrijgen. Leden zullen een verfifieerbare identiteit eerder vertrouwen."
    }
  },
  "COMMENTS": {
    "DIVIDER": "Commentaren",
    "SHOW_MORE_COMMENTS": "Toon eerdere commentaren",
    "COMMENT_HELP": "Jouw commentaar, vraag...",
    "COMMENT_HELP_REPLY_TO": "Jouw antwoord...",
    "BTN_SEND": "Verzenden",
    "POPOVER_SHARE_TITLE": "Bericht #{{number}}",
    "REPLY": "Antwoord",
    "REPLY_TO": "Antwoorden op:",
    "REPLY_TO_LINK": "In antwoord op ",
    "REPLY_TO_DELETED_COMMENT": "In antwoord op een gewist bericht",
    "REPLY_COUNT": "{{replyCount}} antwoorden",
    "DELETED_COMMENT": "Bericht gewist"
  },
  "MESSAGE": {
    "REPLY_TITLE_PREFIX": "Re: ",
    "FORWARD_TITLE_PREFIX": "Fw: ",
    "BTN_REPLY": "Antwoord",
    "BTN_COMPOSE": "Nieuw bericht",
    "BTN_WRITE": "Schrijven",
    "NO_MESSAGE_INBOX": "Geen bericht ontvangen",
    "NO_MESSAGE_OUTBOX": "Geen bericht verzonden",
    "NOTIFICATIONS": {
      "TITLE": "Berichten",
      "MESSAGE_RECEIVED": "Je hebt een <b>bericht ontvangen</b><br/>van"
    },
    "LIST": {
      "INBOX": "Inbox",
      "OUTBOX": "Verzonden",
      "TITLE": "Privé",
      "POPOVER_ACTIONS": {
        "TITLE": "Opties",
        "DELETE_ALL": "Alle berichten wissen"
      }
    },
    "COMPOSE": {
      "TITLE": "Nieuw bericht",
      "TITLE_REPLY": "Antwoord",
      "SUB_TITLE": "Nieuw bericht",
      "TO": "Aan",
      "OBJECT": "Onderwerp",
      "OBJECT_HELP": "Onderwerp",
      "ENCRYPTED_HELP": "Please note this message will by encrypt before sending zodat alleen de ontvanger het kan lezen en zeker kan zijn dat jij de auteur bent.",
      "MESSAGE": "Bericht",
      "MESSAGE_HELP": "Berichtinhoud",
      "CONTENT_CONFIRMATION": "Geen berichtinhoud.<br/><br/>Weet je zeker dat je dit bericht wil verzenden?"
    },
    "VIEW": {
      "TITLE": "Bericht",
      "SENDER": "Verzonden door",
      "RECIPIENT": "Verzonden aan",
      "NO_CONTENT": "Leeg bericht"
    },
    "CONFIRM": {
      "REMOVE": "Weet je zeker dat je <b>dit bericht wil wissen</b>?<br/><br/>Dit kan niet ongedaan gemaakt worden.",
      "REMOVE_ALL": "Weet je zeker dat je <b>alle berichten wil wissen</b>?<br/><br/>Dit kan niet ongedaan gemaakt worden.",
      "MARK_ALL_AS_READ": "Weet je zeker dat je <b>alle berichten als gelezen wil markeren</b>?"
    },
    "INFO": {
      "MESSAGE_REMOVED": "Bericht succesvol gewist",
      "All_MESSAGE_REMOVED": "Berichten succesvol gewist",
      "MESSAGE_SENT": "Bericht verzonden"
    },
    "ERROR": {
      "SEND_MSG_FAILED": "Fout tijdens verzending.",
      "LOAD_MESSAGES_FAILED": "Kan berichten niet laden.",
      "LOAD_MESSAGE_FAILED": "Kan bericht niet laden.",
      "MESSAGE_NOT_READABLE": "Kan bericht niet lezen.",
      "USER_NOT_RECIPIENT": "Je bent niet de geadresseerde van dit bericht: het kan niet gelezen worden.",
      "NOT_AUTHENTICATED_MESSAGE": "De authenticiteit van het bericht is onduidelijk of de inhoud is gecorrumpeerd.",
      "REMOVE_MESSAGE_FAILED": "Kan bericht niet wissen.",
      "MESSAGE_CONTENT_TOO_LONG": "Waarde te lang (max {{maxLength}} characters).",
      "MARK_AS_READ_FAILED": "Kan bericht niet als gelezen markeren.",
      "LOAD_NOTIFICATIONS_FAILED": "Kan niet alle berichtnotificaties laden.",
      "REMOVE_All_MESSAGES_FAILED": "Kan niet alle berichten wissen.",
      "MARK_ALL_AS_READ_FAILED": "Kan berichten niet als gelezen markeren."
    }
  },
  "REGISTRY": {
    "CATEGORY": "Hoofdactiviteit",
    "GENERAL_DIVIDER": "Basisinformatie",
    "LOCATION_DIVIDER": "Adres",
    "SOCIAL_NETWORKS_DIVIDER": "Sociale media en website",
    "TECHNICAL_DIVIDER": "Technische informatie",
    "BTN_NEW": "Toevoegen",
    "SEARCH": {
      "TITLE": "Bedrijfsregister",
      "TITLE_SMALL_DEVICE": "Bedrijfsregister",
      "SEARCH_HELP": "Wie, Wat: kapper, Lili's restaurant, ...",
      "BTN_ADD": "Nieuw",
      "BTN_OPTIONS": "Geavanceerd zoeken",
      "TYPE": "Soort organisatie",
      "LOCATION": "Locatie",
      "LOCATION_HELP": "Plaats",
      "LAST_RECORDS": "Nieuwste referenties:",
      "RESULTS": "Resultaten:"
    },
    "VIEW": {
      "TITLE": "Register",
      "CATEGORY": "Hoofdactiviteit:",
      "LOCATION": "Adres:",
      "MENU_TITLE": "Opties",
      "POPOVER_SHARE_TITLE": "{{title}}",
      "REMOVE_CONFIRMATION" : "Weet je zeker dat je deze referentie wil verwijderen?<br/><br/>Dit kan niet ongedaan worden gemaakt."
    },
    "TYPE": {
      "TITLE": "Nieuwe referentie",
      "SELECT_TYPE": "Soort organizatie:",
      "ENUM": {
        "SHOP": "Locale winkel",
        "COMPANY": "Onderneming",
        "ASSOCIATION": "Stichting",
        "INSTITUTION": "Instituut"
      }
    },
    "EDIT": {
      "TITLE": "Bewerk",
      "TITLE_NEW": "Nieuwe referentie",
      "RECORD_TYPE":"Soort organizatie",
      "RECORD_TITLE": "Naam",
      "RECORD_TITLE_HELP": "Naam",
      "RECORD_DESCRIPTION": "Beschrijving",
      "RECORD_DESCRIPTION_HELP": "Omschrijf activiteit",
      "RECORD_ADDRESS": "Straat",
      "RECORD_ADDRESS_HELP": "Straat, gebouw...",
      "RECORD_CITY": "Plaats",
      "RECORD_CITY_HELP": "Plaats",
      "RECORD_SOCIAL_NETWORKS": "Sociale media en website",
      "RECORD_PUBKEY": "Publieke sleutel",
      "RECORD_PUBKEY_HELP": "Publieke sleutel om betalingen te ontvangen"
    },
    "ERROR": {
      "LOAD_CATEGORY_FAILED": "Laden hoofdactiveiten mislukt",
      "LOAD_RECORD_FAILED": "Laden datasheet mislukt",
      "LOOKUP_RECORDS_FAILED": "Opzoeken datasheets is mislukt.",
      "REMOVE_RECORD_FAILED": "Verwijderen datasheet mislukt",
      "SAVE_RECORD_FAILED": "Opslaan datasheet mislukt",
      "RECORD_NOT_EXISTS": "Datasheet niet gevonden"
    },
    "INFO": {
      "RECORD_REMOVED" : "Datasheet succesvol verwijderd"
    }
  },
  "PROFILE": {
    "UID": "Pseudoniem",
    "TITLE": "Naam",
    "TITLE_HELP": "Naam",
    "DESCRIPTION": "Over mij",
    "DESCRIPTION_HELP": "Over mij...",
    "ADDRESS": "Adres",
    "ADDRESS_HELP": "Adres (optioneel)",
    "CITY": "Plaats",
    "CITY_HELP": "Plaats (optioneel)",
    "SOCIAL_HELP": "http://...",
    "GENERAL_DIVIDER": "Algemene informatie",
    "LOCATION_DIVIDER": "Localisatie",
    "SOCIAL_NETWORKS_DIVIDER": "Sociale media en website",
    "TECHNICAL_DIVIDER": "Technische informatie",
    "ERROR": {
      "LOAD_PROFILE_FAILED": "Kon gebruikersprofiel niet laden.",
      "SAVE_PROFILE_FAILED": "Opslaan profiel mislukt",
      "INVALID_SOCIAL_NETWORK_FORMAT": "Ongeldig formaat: vul een geldig internetadres in.<br/><br/>Voorbeelden:<ul><li>- Een Facebookpagina (https://www.facebook.com/user)</li><li>- Een webpagina (http://www.domain.com)</li><li>- Een emailadres (joe@dalton.com)</li></ul>",
      "IMAGE_RESIZE_FAILED": "Fout tijdens afbeelding schalen"
    },
    "INFO": {
      "PROFILE_SAVED": "Profiel opgeslagen"
    },
    "HELP": {
      "WARNING_PUBLIC_DATA": "Let op, de informatie die hier is vastgelegd <b>is publiek</b>: zichtbaar ook voor <b>niet ingelogde gebruikers</b>."
    }
  },
  "ES_SETTINGS": {
    "PLUGIN_NAME": "Cesium+",
    "ENABLE_TOGGLE": "Uitbreiding inschakelen?",
    "ENABLE_MESSAGE_TOGGLE": "Berichten inschakelen?",
    "ENABLE_SETTINGS_TOGGLE": "Globale opslag voor instellingen inschakelen?",
    "PEER": "Adres dataknooppunt",
    "POPUP_PEER": {
      "TITLE" : "Dataknoop",
      "HELP" : "Stel het te gebruiken adres in:",
      "PEER_HELP": "server.domein.com:poort"
    },
    "NOTIFICATIONS": {
      "DIVIDER": "Notificaties",
      "HELP_TEXT": "Schakel het type notificatie dat je wil ontvangen in:",
      "ENABLE_TX_SENT": "Bericht bij validatie van <b>verzonden betalingen</b>?",
      "ENABLE_TX_RECEIVED": "Bericht bij validatie van <b>ontvangen betalingen</b>?",
      "ENABLE_CERT_SENT": "Bericht bij validatie van <b>verzonden certificaties</b>?",
      "ENABLE_CERT_RECEIVED": "Bericht bij validatie van <b>ontvangen certificaties</b>?"
    },
    "CONFIRM": {
      "ASK_ENABLE_TITLE": "Optionele functies",
      "ASK_ENABLE": "Cesium+ is <b>uitgeschakeld</b> waardoor deze functies niet beschikbaar zijn: <ul><li>&nbsp;&nbsp;<b><i class=\"icon ion-person\"></i> Gebruikersprofielen</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-android-notifcaitions\"></i> Notificaties</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-email\"></i> Privéberichten</b>.</ul><br/><br/>Wil je deze functies <b>inschakelen</b>?"
    }
  },
  "EVENT": {
    "NODE_STARTED": "Je knoop ES API <b>{{params[0]}}</b> is UP",
    "NODE_BMA_DOWN": "Knooppunt <b>{{params[0]}}:{{params[1]}}</b> (gebruikt door je ES API) is <b>onbereikbaar</b>.",
    "NODE_BMA_UP": "Knooppunt <b>{{p0}}:{{params[1]}}</b> is weer bereikbaar.",
    "MEMBER_JOIN": "Je bent nu <b>lid</b> van valuta <b>{{params[0]}}</b>!",
    "MEMBER_LEAVE": "Je bent <b>geen lid meer</b> van valuta <b>{{params[0]}}</b>!",
    "MEMBER_ACTIVE": "Je lidmaatschap bij <b>{{params[0]}}</b> is met <b>succes verlengd</b>.",
    "TX_SENT": "Je <b>betaling</b> aan <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> is uitgevoerd.",
    "TX_SENT_MULTI": "Je <b>betaling</b> aan <b>{{params[1]}}</b> is uitgevoerd.",
    "TX_RECEIVED": "Je hebt een <b>betaling ontvangen</b> van <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "TX_RECEIVED_MULTI": "Je hebt een <b>betaling ontvangen</b> van <b>{{params[1]}}</b>.",
    "CERT_SENT": "Je <b>certificatie</b> van <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> is uitgevoerd.",
    "CERT_RECEIVED": "Je hebt een <b>certificatie ontvangen</b> van <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "PAGE": {
      "NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> heeft gereageerd op jouw referentie: <b>{{params[2]}}</b>",
      "UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> heeft zijn/aar reactie op jouw referentie bewerkt: <b>{{params[2]}}</b>",
      "NEW_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> hheeft gereageerd op jouw commentaar op referentie: <b>{{params[2]}}</b>",
      "UPDATE_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> heeft zijn/haar reactie op jouw commentaar bewerkt, op referentie: <b>{{params[2]}}</b>"
    }
  },
  "CONFIRM": {
    "ES_USE_FALLBACK_NODE": "Knooppunt <b>{{old}}</b> onbereikbaar of ongeldig adres.<br/><br/>Tijdelijk knooppunt <b>{{new}}</b> gebruiken?"
  },
  "ERROR": {
    "ES_CONNECTION_ERROR": "Knooppunt <b>{{server}}</b> onbereikbaar of ongeldig adres.<br/><br/>Cesium zal verder weken, <b>zonder de Cesium+ uitbreiding</b> (gebruikersprofielens, privéberichten, kaarten en grafieken).<br/><br/>Controleer je internetverbinding, of verander je knooppunt in <a class=\"positive\" ng-click=\"doQuickFix('settings')\">instellingen</a>.",
    "ES_MAX_UPLOAD_BODY_SIZE": "De hoveelheid te verzenden gegevens is overschrijdt de serverlimiet.<br/><br/>Probeer het nogmaals na, bijvoorbeeld, het wissen foto's."
  }
}
);

$translateProvider.translations("pt-PT", {
  "COMMON": {
    "ABUSES_TEXT": "{{total}} pessoa{{total > 1 ? 's' : ''}} {{total > 1 ? 'sinalizaram' : 'sinalizoua'}}  um problema",
    "BTN_LIKE": "Gosta de mim",
    "BTN_LIKE_REMOVE": "Deixou de gostar de mim",
    "BTN_REMOVE_REPORTED_ABUSE": "Cancelar reportar",
    "BTN_REPORT_ABUSE_DOTS": "Reportar um problema ou um abuso...",
    "COMMENT_HELP": "Comentário",
    "LIKES_TEXT": "A {{total}} pessoa{{total > 1 ? 's' : ''}} {{total > 1 ? 'gostaram' : 'gostou'}} desta página",
    "NOTIFICATION": {
        "HAS_UNREAD": "Tem {{count}} notifica{{count>0?'ções':'ção'}} não lida{{count>0?'s':''}}",
        "TITLE": "Nova notificação | {{'COMMON.APP_NAME'|translate}}"
    },
    "REPORT_ABUSE": {
      "ASK_DELETE": "Pedir a eliminação?",
      "CONFIRM": {
          "SENT": "Reporte enviado. Grato!"
      },
      "REASON_HELP": "Eu explico o problema...",
      "SUB_TITLE": "Por favor explique brevemente o problema :",
      "TITLE": "Assinalar um problema"
    },

    "CATEGORY": "Categoria",
    "CATEGORIES": "Categorias",
    "CATEGORY_SEARCH_HELP": "Procurar",
    "LAST_MODIFICATION_DATE": "Atualização em",
    "SUBMIT_BY": "Enviado por",
    "BTN_PUBLISH": "Publicar",
    "BTN_PICTURE_DELETE": "Apagar",
    "BTN_PICTURE_FAVORISE": "Principal",
    "BTN_PICTURE_ROTATE": "Rodar",
    "BTN_ADD_PICTURE": "Adicionar uma foto",
    "NOTIFICATIONS": {
      "TITLE": "Notificações",
      "MARK_ALL_AS_READ": "Marcar tudo como lido",
      "NO_RESULT": "Nenhuma notificação",
      "SHOW_ALL": "Ver tudo",
      "LOAD_NOTIFICATIONS_FAILED": "Falha ao carregar as notificações"
    }
  },
  "DOCUMENT": {
    "HASH": "Hash: ",
    "LOOKUP": {
      "BTN_COMPACT": "Compactar",
      "HAS_CREATE_OR_UPDATE_PROFILE": "Perfil criado ou modificado",
      "LAST_DOCUMENTS_DOTS": "Últimos documentos :",
      "TITLE": "Procura de documentos",
      "BTN_ACTIONS": "Ações",
      "SEARCH_HELP": "issuer:AAA*, time:1508406169",
      "LAST_DOCUMENTS": "Últimos documentos",
      "SHOW_QUERY": "Ver a procura",
      "HIDE_QUERY": "Esconder a procura",
      "HEADER_TIME": "Data/Hora",
      "HEADER_ISSUER": "Emissor",
      "HEADER_RECIPIENT": "Destinatário",
      "HEADER_AMOUNT": "Quantia",
      "READ": "Lido",
      "BTN_REMOVE": "Apagar este documento",
      "POPOVER_ACTIONS": {
        "TITLE": "Acções",
        "REMOVE_ALL": "Apagar estes documentos..."
      }
    },
    "INFO": {
      "REMOVED": "Documento apagado"
    },
    "CONFIRM": {
      "REMOVE": "Deseja <b>apagar este documento</b>?",
      "REMOVE_ALL": "Deseja <b>apagar estes documentos</b>?"
    },
    "ERROR": {
      "LOAD_DOCUMENTS_FAILED": "Falha ao procurar os documentos",
      "REMOVE_FAILED": "Falha ao apagar o documento",
      "REMOVE_ALL_FAILED": "Falha ao apagar os documentos"
    }
  },
  "MENU": {
    "REGISTRY": "Páginas",
    "USER_PROFILE": "O meu perfil",
    "MESSAGES": "Mensagens",
    "NOTIFICATIONS": "Notificações",
    "INVITATIONS": "Convites"
  },
  "ACCOUNT": {
    "NEW": {
      "ORGANIZATION_ACCOUNT": "Conta para uma organização",
      "ORGANIZATION_ACCOUNT_HELP": "Se representa uma empresa, uma associação, etc.<br/>Nenhum dividendo universal será criado por esta conta."
    },
    "EVENT": {
      "MEMBER_WITHOUT_PROFILE": "Para obter as suas certificações mais rapidamente, complete <a ui-sref=\"app.edit_profile\">o seu perfil de usuário</a>. Os membros concederão mais facilmente a sua confiança a uma identidade verificável."
    },
    "ERROR": {
      "WS_CONNECTION_FAILED": "Cesium não pode receber as notificações, por causa de uma falha técnica (conexão ao nó de dados Cesium+).<br/><br/>Se o problema persistir, por favor <b>escolha outro nó de dados</b> nas definições de Cesium+."
    }
  },
  "WOT": {
    "BTN_SUGGEST_CERTIFICATIONS_DOTS": "Sugerir identidades a certificar…",
    "BTN_ASK_CERTIFICATIONS_DOTS": "Pedir a membros que o certifiquem…",
    "BTN_ASK_CERTIFICATION": "Pedir uma certificação",
    "SUGGEST_CERTIFICATIONS_MODAL": {
      "TITLE": "Sugerir certificações",
      "HELP": "Selecionar as suas sugestões"
    },
    "ASK_CERTIFICATIONS_MODAL": {
      "TITLE": "Solicitar certificações",
      "HELP": "Selecionar os destinatários"
    },
    "SEARCH": {
      "DIVIDER_PROFILE": "Contas",
      "DIVIDER_PAGE": "Páginas",
      "DIVIDER_GROUP": "Grupos"
    },
    "CONFIRM": {
      "SUGGEST_CERTIFICATIONS": "Deseja <b>enviar estas sugestões de certificação</b> ?",
      "ASK_CERTIFICATION": "Deseja <b>enviar um pedido de certificação</b> ?",
      "ASK_CERTIFICATIONS": "Deseja <b>enviar um pedido de certificação</b> a estas pessoas ?"
    }
  },
  "INVITATION": {
    "TITLE": "Convites",
    "NO_RESULT": "Nenhum convite em espera",
    "BTN_DELETE_ALL": "Apagar todos os convites",
    "BTN_DELETE": "Apagar o convite",
    "BTN_NEW_INVITATION": "Novo convite",
    "ASK_CERTIFICATION": "<a href=\"#/app/wot/{{pubkey}}/{{::uid}}\">{{::name||uid}}</a> solicita a sua certificação",
    "SUGGESTION_CERTIFICATION": "<a href=\"#/app/wot/{{::pubkey}}/{{::uid}}\">{{::name||uid}}</a> foi sugerido/a para certificação",
    "SUGGESTED_BY": "Sugestão enviada por <a class=\"positive\" href=\"#/app/wot/{{::issuer.pubkey}}/{{::issuer.uid}}\">{{::issuer.name||issuer.uid}}</a>",
    "NOTIFICATIONS": {
      "TITLE": "Convites"
    },
    "LIST": {
      "TITLE": "Convites"
    },
    "NEW": {
      "TITLE": "Novo convite",
      "RECIPIENTS": "Para",
      "RECIPIENTS_HELP": "Destinatários do convite",
      "RECIPIENTS_MODAL_TITLE": "Destinatários",
      "RECIPIENTS_MODAL_HELP": "Por favor, escolha os destinatários :",
      "SUGGESTION_IDENTITIES": "Sugestão de certificação",
      "SUGGESTION_IDENTITIES_HELP": "Certificações a sugerir",
      "SUGGESTION_IDENTITIES_MODAL_TITLE": "Sugestões",
      "SUGGESTION_IDENTITIES_MODAL_HELP": "Por favor, escolha as suas sugestões :"
    },
    "CONFIRM": {
      "DELETE_ALL_CONFIRMATION": "A eliminação dos convites é uma <b>operação irreversível</b>.<br/><br/>Deseja continuar ?",
      "SEND_INVITATIONS_TO_CERTIFY": "Deseja <b>enviar este convite a certificar</b> ?"
    },
    "INFO": {
      "INVITATION_SENT": "Convite enviado"
    },
    "ERROR": {
      "LOAD_INVITATIONS_FAILED": "Falha ao carregar os convites",
      "REMOVE_INVITATION_FAILED": "Falha ao eliminar convite",
      "REMOVE_ALL_INVITATIONS_FAILED": "Falha ao eliminar os convites",
      "SEND_INVITATION_FAILED": "Falha ao enviar convite",
      "BAD_INVITATION_FORMAT": "<span class=\"assertive\"><i class=\"ion-close-circled\"></i> convite ilegível (formato desconhecido)</span> - enviado por <a ui-sref=\"app.wot_identity({pubkey: '{{::pubkey}}', uid: '{{::uid}}' })\">{{::name||uid}}</a>"
    }
  },
  "COMMENTS": {
    "DIVIDER": "Comentários",
    "SHOW_MORE_COMMENTS": "Visualizar os comentários anteriores",
    "COMMENT_HELP": "O seu comentário, perguntas, etc.",
    "COMMENT_HELP_REPLY_TO": "A sua reposta…",
    "BTN_SEND": "Enviar",
    "POPOVER_SHARE_TITLE": "Mensagem #{{number}}",
    "MODIFIED_ON": "modificado em {{time|formatDate}}",
    "MODIFIED_PARENTHESIS": "(modificado então)",
    "REPLY": "Responder",
    "REPLY_TO": "Reposta a :",
    "REPLY_TO_LINK": "Em reposta a ",
    "REPLY_TO_DELETED_COMMENT": "Em reposta a um comentário apagado",
    "REPLY_COUNT": "{{replyCount}} repostas",
    "DELETED_COMMENT": "Comentário apagado",
    "ERROR": {
      "FAILED_SAVE_COMMENT": "Falha ao gravar o comentário",
      "FAILED_REMOVE_COMMENT": "Falha ao eliminar comentário"
    }
  },
  "MESSAGE": {
    "REPLY_TITLE_PREFIX": "Rep: ",
    "FORWARD_TITLE_PREFIX": "Tr: ",
    "BTN_REPLY": "Responder",
    "BTN_COMPOSE": "Nova mensagem",
    "BTN_WRITE": "Escrever",
    "NO_MESSAGE_INBOX": "Nenhuma mensagem recebida",
    "NO_MESSAGE_OUTBOX": "Nenhuma mensagem enviada",
    "NOTIFICATIONS": {
      "TITLE": "Mensajes",
      "MESSAGE_RECEIVED": "<b>Mensagem recebida</b><br/>de"
    },
    "LIST": {
      "INBOX": "Caixa de entrada",
      "OUTBOX": "Mensagens enviadas",
      "LAST_INBOX": "Novas mensagens",
      "LAST_OUTBOX": "Mensagens enviadas",
      "BTN_LAST_MESSAGES": "Mensagens recentes",
      "TITLE": "Mensagens",
      "SEARCH_HELP": "Procurar em mensagens",
      "POPOVER_ACTIONS": {
        "TITLE": "Opções",
        "DELETE_ALL": "Apagar todas as mensagens"
      }
    },
    "COMPOSE": {
      "TITLE": "Nova mensagem",
      "TITLE_REPLY": "Responder",
      "SUB_TITLE": "Nova mensagem",
      "TO": "Para",
      "OBJECT": "Objeto",
      "OBJECT_HELP": "Objeto",
      "ENCRYPTED_HELP": "Tenha em conta que esta mensagem será cifrada antes do envio, com o fim de que só o destinatário possa lê la, e que se tenha a segurança de que é da sua autoria.",
      "MESSAGE": "Mensagem",
      "MESSAGE_HELP": "Conteúdo da mensagem",
      "CONTENT_CONFIRMATION": "O conteúdo da mensagem está vazio.<br/><br/>deseja enviar a mensagem assim?"
    },
    "VIEW": {
      "TITLE": "Mensagem",
      "SENDER": "Enviado por",
      "RECIPIENT": "Enviado para",
      "NO_CONTENT": "Mensagem vazia",
      "DELETE": "Eliminar a mensagem"
    },
    "CONFIRM": {
      "REMOVE": "Deseja <b>apagar esta mensagem</b> ?<br/><br/>Esta operação é irreversível.",
      "REMOVE_ALL" : "Deseja <b>apagar todas as mensagens</b> ?<br/><br/>Esta operação é irreversível.",
      "MARK_ALL_AS_READ": "Deseja <b>marcar todas as mensagens como lidas</b> ?",
      "USER_HAS_NO_PROFILE": "Esta identidade não tem nenhum perfil Cesium+. Pode não estar habilitada a extensão Cesium+, y <b>não poderá ver a sua mensagem</b>.<br/><br/>Deseja <b>continuar</b> de qualquer forma?"
    },
    "INFO": {
      "MESSAGE_REMOVED": "Mensagem apagada",
      "All_MESSAGE_REMOVED": "Toda as mensagens foram apagadas",      "MESSAGE_SENT": "Mensagem enviada"
    },
    "ERROR": {
      "SEND_MSG_FAILED": "Falha no envio da mensagem.",
      "LOAD_MESSAGES_FAILED": "Falha ao recuperar as mensagens.",
      "LOAD_MESSAGE_FAILED": "Falha ao recuperar a mensagem.",
      "MESSAGE_NOT_READABLE": "Leitura da mensagem impossível.",
      "USER_NOT_RECIPIENT": "Não há destinatário nesta mensagem : impossível decifrar.",
      "NOT_AUTHENTICATED_MESSAGE": "A autenticidade da mensagem é duvidosa ou o seu conteúdo está corrompido.",
      "REMOVE_MESSAGE_FAILED": "Falha ao eliminar mensagem",
      "MESSAGE_CONTENT_TOO_LONG": "Valor demasiado longo ({{maxLength}} caracteres max).",
      "MARK_AS_READ_FAILED": "Impossível marcar a mensagem como 'lida'.",
      "LOAD_NOTIFICATIONS_FAILED": "Falha ao recuperar as notificações de mensagens.",
      "REMOVE_All_MESSAGES_FAILED": "Falha ao eliminar todas as mensagens.",
      "MARK_ALL_AS_READ_FAILED": "Falha ao marcar as mensagens como lidas.",
      "RECIPIENT_IS_MANDATORY": "O destinatário é obrigatório."
    }
  },
  "BLOCKCHAIN": {
    "LOOKUP": {
      "SEARCH_HELP": "Número de bloco, hash, chave pública, etc.",
      "POPOVER_FILTER_TITLE": "Filtros",
      "HEADER_MEDIAN_TIME": "Data / Hora",
      "HEADER_BLOCK": "Bloco #",
      "HEADER_ISSUER": "Nó emissor",
      "BTN_LAST": "Últimos blocos",
      "DISPLAY_QUERY": "Mostrar a consulta",
      "HIDE_QUERY": "Ocultar a consulta",
      "TX_SEARCH_FILTER": {
        "MEMBER_FLOWS": "Entradas/saídas de membros",
        "EXISTING_TRANSACTION": "transações existentes",
        "PERIOD": "<b class=\"ion-clock\"></b> Entre o <b class=\"gray\">{{params[1]|medianDateShort}}</b> ({{params[1]|medianTime}}) e o <b class=\"gray\">{{params[2]|medianDateShort}}</b> ({{params[2]|medianTime}})",
        "ISSUER": "<b class=\"ion-android-desktop\"></b> Calculado por <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}",
        "TX_PUBKEY": "<b class=\"ion-card\"></b> Transações que implicam <b class=\"ion-key\"></b> {{params[1]|formatPubkey}}"
      }
    },
    "ERROR": {
      "SEARCH_BLOCKS_FAILED": "Falha na procura dos blocos."
    }
  },
  "GROUP": {
    "GENERAL_DIVIDER": "Informações gerais",
    "LOCATION_DIVIDER": "Localização",
    "SOCIAL_NETWORKS_DIVIDER": "Redes sociais e site",
    "TECHNICAL_DIVIDER": "Informações técnicas",
    "CREATED_TIME": "Criada {{creationTime|formatFromNow}}",
    "NOTIFICATIONS": {
      "TITLE": "Convites"
    },
    "LOOKUP": {
      "TITLE": "Grupos",
      "SEARCH_HELP": "Nome de grupo, palavras , lugar, etc.",
      "LAST_RESULTS_LIST": "Novos grupos :",
      "OPEN_RESULTS_LIST": "Grupos abertos :",
      "MANAGED_RESULTS_LIST": "Grupos fechados :",
      "BTN_LAST": "Novos grupos",
      "BTN_NEW": "Adiciono um grupo"
    },
    "TYPE": {
      "TITLE": "Novo grupo",
      "SELECT_TYPE": "Tipo de grupo :",
      "OPEN_GROUP": "Grupo aberto",
      "OPEN_GROUP_HELP": "Um grupo aberto é acessível por qualquer membro da moeda.",
      "MANAGED_GROUP": "Grupo administrado",
      "MANAGED_GROUP_HELP": "Um grupo administrado é gerido por administradores e moderadores, que podem aceitar, recusar ou excluir um membro do seu seio.",
      "ENUM": {
        "OPEN": "Grupo aberto",
        "MANAGED": "Grupo administrado"
      }
    },
    "VIEW": {
      "POPOVER_SHARE_TITLE": "{{title}}",
      "MENU_TITLE": "Opções",
      "REMOVE_CONFIRMATION" : "Deseja eliminar este grupo?<br/><br/>Esta operação é irreversível."
    },
    "EDIT": {
      "TITLE": "Grupo",
      "TITLE_NEW": "Novo grupo",
      "RECORD_TITLE": "Título",
      "RECORD_TITLE_HELP": "Título",
      "RECORD_DESCRIPTION": "Descrição",
      "RECORD_DESCRIPTION_HELP": "Descrição"
    },
    "ERROR": {
      "SEARCH_GROUPS_FAILED": "Falha na procura de grupos",
      "REMOVE_RECORD_FAILED": "Falha ao eliminar o grupo"
    },
    "INFO": {
      "RECORD_REMOVED" : "Grupo eliminado"
    }
  },
  "REGISTRY": {
    "CATEGORY": "Atividade principal",
    "GENERAL_DIVIDER": "Informações gerais",
    "LOCATION_DIVIDER": "Localização",
    "SOCIAL_NETWORKS_DIVIDER": "Redes sociais e site",
    "TECHNICAL_DIVIDER": "Informações técnicas",
    "BTN_SHOW_WOT": "Pessoas",
    "BTN_SHOW_WOT_HELP": "Procurar pessoas",
    "BTN_SHOW_PAGES": "Páginas",
    "BTN_SHOW_PAGES_HELP": "Procura de páginas",
    "BTN_NEW": "Criar uma página",
    "MY_PAGES": "As minhas páginas",
    "NO_PAGE": "Sem páginas",
    "SEARCH": {
      "TITLE": "Páginas",
      "SEARCH_HELP": "O quê, Quem, ex: cabeleireiro, restaurante Sol.",
      "BTN_ADD": "Novo",
      "BTN_LAST_RECORDS": "Páginas recentes",
      "BTN_ADVANCED_SEARCH": "Procura avançada",
      "BTN_OPTIONS": "Procura avançada",
      "TYPE": "Tipo de página",
      "LOCATION_HELP": "Cidade",
      "RESULTS": "Resultados",
      "RESULT_COUNT_LOCATION": "{{count}} Resultado{{count>0?'s':''}}, próximo de {{location}}",
      "RESULT_COUNT": "{{count}} resultado{{count>0?'s':''}}",
      "LAST_RECORDS": "Páginas recentes",
      "LAST_RECORD_COUNT_LOCATION": "{{count}} página{{count>0?'s':''}} reciente{{count>0?'s':''}}, próximo de {{location}}",
      "LAST_RECORD_COUNT": "{{count}} página{{count>0?'s':''}} reciente{{count>0?'s':''}}",
      "POPOVER_FILTERS": {
        "BTN_ADVANCED_SEARCH": "Opções avançadas"
      }
    },
    "VIEW": {
      "TITLE": "Anuário",
      "CATEGORY": "Atividade principal :",
      "LOCATION": "Localização :",
      "MENU_TITLE": "Opções",
      "POPOVER_SHARE_TITLE": "{{title}}",
      "REMOVE_CONFIRMATION" : "Deseja apagar esta página ?<br/><br/>Esta operação é irreversível."
    },
    "TYPE": {
      "TITLE": "Nueva página",
      "SELECT_TYPE": "Tipo de página :",
      "ENUM": {
        "SHOP": "Comercio local",
        "COMPANY": "Empresa",
        "ASSOCIATION": "Associação",
        "INSTITUTION": "Instituição"
      }
    },
    "EDIT": {
      "TITLE": "Edição",
      "TITLE_NEW": "Nova página",
      "RECORD_TYPE":"Tipo de página",
      "RECORD_TITLE": "Nome",
      "RECORD_TITLE_HELP": "Nome",
      "RECORD_DESCRIPTION": "Descrição",
      "RECORD_DESCRIPTION_HELP": "Descrição da atividade",
      "RECORD_ADDRESS": "Rua",
      "RECORD_ADDRESS_HELP": "Rua, edifício…",
      "RECORD_CITY": "Cidade",
      "RECORD_CITY_HELP": "Cidade",
      "RECORD_SOCIAL_NETWORKS": "Redes sociais e site",
      "RECORD_PUBKEY": "Chave pública",
      "RECORD_PUBKEY_HELP": "Chave pública para receber pagamentos"
    },
    "WALLET": {
      "PAGE_DIVIDER": "Páginas",
      "PAGE_DIVIDER_HELP": "As páginas referem-se a coletivos que aceitam moeda ou a promovem: lojas, empresas, negócios, associações, instituições. Armazenam-se fora da rede da moeda, em <a ui-sref=\"app.es_network\">a rede Cesium+</a>."
    },
    "ERROR": {
      "LOAD_CATEGORY_FAILED": "Falha no carregamento da lista de atividades",
      "LOAD_RECORD_FAILED": "Falha durante o carregamento da página",
      "LOOKUP_RECORDS_FAILED": "Falha durante a execução de procura.",
      "REMOVE_RECORD_FAILED": "Falha na eliminação da página",
      "SAVE_RECORD_FAILED": "Falha durante a gravação",
      "RECORD_NOT_EXISTS": "Página inexistente",
      "GEO_LOCATION_NOT_FOUND": "Cidade ou código postal não encontrado"
    },
    "INFO": {
      "RECORD_REMOVED" : "Página apagada",
      "RECORD_SAVED": "Página guardada"
    }
  },
  "PROFILE": {
    "PROFILE_DIVIDER": "Perfil Cesium+",
    "PROFILE_DIVIDER_HELP": "Estes são dados auxiliares, armazenados fora da rede monetária",
    "NO_PROFILE_DEFINED": "Nenhum perfil Cesium+",
    "BTN_ADD": "Entra no meu perfil",
    "BTN_EDIT": "Editar o meu perfil",
    "BTN_DELETE": "Eliminar o meu perfil",
    "BTN_REORDER": "Reordenar",
    "UID": "Pseudónimo",
    "TITLE": "Nome, Apelidos",
    "TITLE_HELP": "Nome, Apelidos",
    "DESCRIPTION": "Sobre mim",
    "DESCRIPTION_HELP": "Sobre mim…",
    "SOCIAL_HELP": "http://...",
    "GENERAL_DIVIDER": "Informações gerais",
    "SOCIAL_NETWORKS_DIVIDER": "Redes sociais, sites",
    "TECHNICAL_DIVIDER": "Informações técnicas",
    "MODAL_AVATAR": {
      "TITLE": "Foto de perfil",
      "SELECT_FILE_HELP": "Por favor, <b>escolha uma imagem</b>:",
      "BTN_SELECT_FILE": "Escolher uma imagem",
      "RESIZE_HELP": "<b>Enquadre a imagem</b>, se necessário. Um clique pressionado sobre a imagem permite deslocá-la. faça clique na zona inferior esquerda para fazer zoom.",
      "RESULT_HELP": "<b>Aqui está o resultado</b> tal como se verá no seu perfil :"
    },
    "CONFIRM": {
      "DELETE": "Deseja <b>eliminar o seu perfil Cesium+?</b><br/><br/>Esta operação é irreversível.",
      "DELETE_BY_MODERATOR": "Deseja <b>eliminar este perfil Cesium+?</b><br/><br/>Esta operação é irreversível ."
    },
    "ERROR": {
      "DELETE_PROFILE_FAILED": "falhar durante a eliminação do perfil",
      "REMOVE_PROFILE_FAILED": "Falha de eliminação do perfil",
      "LOAD_PROFILE_FAILED": "Falha no carregamento do perfil de usuário.",
      "SAVE_PROFILE_FAILED": "Falha durante a gravação",
      "INVALID_SOCIAL_NETWORK_FORMAT": "Formato inválido: por favor, indique uma direção válida.<br/><br/>Exemplos :<ul><li>- Uma página Facebook (https://www.facebook.com/user)</li><li>- Uma página web (http://www.meusitio.pt)</li><li>- Uma direção de email (jose@dalton.com)</li></ul>",
      "IMAGE_RESIZE_FAILED": "Falhou o redimensionamento da imagem"
    },
    "INFO": {
      "PROFILE_REMOVED": "Perfil eliminado",
      "PROFILE_SAVED": "Perfil guardado"
    },
    "HELP": {
      "WARNING_PUBLIC_DATA": "A informação do seu perfil <b>é pública</b>: visível também por pessoas <b>sem conta</b>.<br/>{{'PROFILE.PROFILE_DIVIDER_HELP'|translate}}"
    }
  },
  "LIKE": {
    "ERROR": {
        "FAILED_TOGGLE_LIKE": "Impossível executar esta ação."
    }
  },
  "LOCATION": {
    "BTN_GEOLOC_ADDRESS": "Atualizar a partir do endereço",
    "USE_GEO_POINT": "Aparecer no mapa {{'COMMON.APP_NAME'|translate}}",
    "LOADING_LOCATION": "Encontrar a direção…",
    "LOCATION_DIVIDER": "Localização",
    "ADDRESS": "Rua",
    "ADDRESS_HELP": "Rua, número, etc…",
    "CITY": "Cidade",
    "CITY_HELP": "Cidade, País",
    "DISTANCE": "Distancia máxima dos arredores da cidade",
    "DISTANCE_UNIT": "km",
    "DISTANCE_OPTION": "{{value}} {{'LOCATION.DISTANCE_UNIT'|translate}}",
    "SEARCH_HELP": "Cidade, País",
    "PROFILE_POSITION": "Posição do perfil",
    "MODAL": {
      "TITLE": "Procurar por direção",
      "SEARCH_HELP": "Cidade, País",
      "ALTERNATIVE_RESULT_DIVIDER": "Resultados alternativos para <b>{{address}}</b> :",
      "POSITION": "Latitud/Longitud : {{lat}} / {{lon}}"
    },
    "ERROR": {
      "CITY_REQUIRED_IF_STREET": "Requerido sei uma rua foi inserida",
      "REQUIRED_FOR_LOCATION": "Campo obrigatório para aparecer no mapa",
      "INVALID_FOR_LOCATION": "Localização desconhecida",
      "GEO_LOCATION_FAILED": "Não se pode recuperar a sua localização Por favor use o botão de procura",
      "ADDRESS_LOCATION_FAILED": "Não se pode recuperar a posição da direção."
    }
  },
  "SUBSCRIPTION": {
    "SUBSCRIPTION_DIVIDER": "Serviços on line",
    "SUBSCRIPTION_DIVIDER_HELP": "Os serviços on line oferecem serviços adicionais, proporcionados por um terceiro.",
    "BTN_ADD": "Agregar um serviço",
    "BTN_EDIT": "Administrar os meus serviços",
    "NO_SUBSCRIPTION": "Nenhum serviço definido",
    "SUBSCRIPTION_COUNT": "Serviços / Subscrição",
    "EDIT": {
      "TITLE": "Serviços on line",
      "HELP_TEXT": "Faça a gestão das suas subscrições e outros serviços on line aqui",
      "PROVIDER": "Provedor:"
    },
    "TYPE": {
      "ENUM": {
        "EMAIL": "Receber notificações por correio eletrónico"
      }
    },
    "CONFIRM": {
      "DELETE_SUBSCRIPTION": "Deseja <b>eliminar</b> esta subscrição ?"
    },
    "ERROR": {
      "LOAD_SUBSCRIPTIONS_FAILED": "Falha ao carregar serviços em linha",
      "ADD_SUBSCRIPTION_FAILED": "Falha ao carregar subscrição",
      "UPDATE_SUBSCRIPTION_FAILED": "Falha durante a atualização da subscrição",
      "DELETE_SUBSCRIPTION_FAILED": "Falha ao eliminar a subscrição"
    },
    "MODAL_EMAIL": {
      "TITLE" : "Notificação por correio eletrónico",
      "HELP" : "Preencha este formulário para <b>ser notificado por correio eletrónico</b> dos eventos da sua conta. <br/> A sua direção de correi eletrónico será cifrada e visível unicamente para o provedor de serviços.",
      "EMAIL_LABEL" : "O seu correio eletrónico :",
      "EMAIL_HELP": "maria@dominio.com",
      "FREQUENCY_LABEL": "Frequência das notificações :",
      "FREQUENCY_DAILY": "Diária",
      "FREQUENCY_WEEKLY": "Semanal",
      "PROVIDER": "Provedor de serviço :"
    }
  },
  "ES_PEER": {
    "DOCUMENT_COUNT": "Número de documentos",
    "DOCUMENTS": "Documentos",
    "EMAIL_SUBSCRIPTION_COUNT": "{{emailSubscription}} suscrito/a{{emailSubscription ? 's' : ''}} para notificações por correio",
    "NAME": "Nome",
    "SOFTWARE": "Software"
  },
  "ES_SETTINGS": {
    "PLUGIN_NAME": "Cesium+",
    "PLUGIN_NAME_HELP": "Perfis, notificações, mensagens privadas",
    "ENABLE_TOGGLE": "Ativar a extensão",
    "ENABLE_MESSAGE_TOGGLE": "Ativar as mensagens privadas",
    "ENABLE_REMOTE_STORAGE": "Ativar o armazenamento remoto",
    "ENABLE_REMOTE_STORAGE_HELP": "Permite armazenar (cifrado) as suas definições nos nós Cesium+",
    "PEER": "Localização do nó de dados",
    "POPUP_PEER": {
      "TITLE" : "Nodo de dados",
      "HELP" : "Ingresse a direção do nó que quer utilizar:",
      "PEER_HELP": "servidor.domínio.com:porta"
    },
    "NOTIFICATIONS": {
      "DIVIDER": "Notificações",
      "HELP_TEXT": "Ative os tipos de notificações que deseja receber:",
      "ENABLE_TX_SENT": "Notificar a validação dos <b>pagamentos emitidos</b>",
      "ENABLE_TX_RECEIVED": "Notificar a validação dos <b>pagamentos recebidos</b>",
      "ENABLE_CERT_SENT": "Notificar a validação das <b>certificações emitidas</b>",
      "ENABLE_CERT_RECEIVED": "Notificar a validação das <b>certificações recebidas</b>",
      "ENABLE_HTML5_NOTIFICATION": "Alertar a cada nova notificação",
      "ENABLE_HTML5_NOTIFICATION_HELP": "Abre uma pequena janela pop-up a cada nova notificação."
    },
    "CONFIRM": {
      "ASK_ENABLE_TITLE": "Outras funcionalidades",
      "ASK_ENABLE": "A extensão de Cesium+ está desabilitada nas definições, desativando certas funcionalidades: <ul><li>&nbsp;&nbsp;<b><i class=\"icon ion-person\"></i> Perfis de usuário/a</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-android-notification\"></i> Notificações</b>;<li>&nbsp;&nbsp;<b><i class=\"icon ion-email\"></i> Mensagens privados</b>.</ul><br/><br/><b>Deseja reativar</b> a extensão?"
    }
  },
  "ES_WALLET": {
    "ERROR": {
      "RECIPIENT_IS_MANDATORY": "Um destinatário é obrigatório para cifrar."
    }
  },
  "EVENT": {
    "NODE_STARTED": "O Seu nó PT API <b>{{params[0]}}</b> foi iniciado",
    "NODE_BMA_DOWN": "O nó <b>{{params[0]}}:{{params[1]}}</b> (utilizado pelo seu nó PT API) <b>não é localizável</b>.",
    "NODE_BMA_UP": "O nó <b>{{params[0]}}:{{params[1]}}</b> está novamente acessível.",
    "MEMBER_JOIN": "Agora é <b>membro</b> da moeda <b>{{params[0]}}</b> !",
    "MEMBER_LEAVE": "Não é <b>membro</b> da moeda <b>{{params[0]}}</b>!",
    "MEMBER_EXCLUDE": "Você já não é membro da moeda <b>{{params[0]}}</b>, por falta de renovação ou certificações.",
    "MEMBER_REVOKE": "O seu estado de membro foi revogado. já não é membro da moeda <b>{{params[0]}}</b>.",
    "MEMBER_ACTIVE": "O seu estado de membro <b>{{params[0]}}</b> foi <b>renovado com sucesso</b>.",
    "TX_SENT": "O seu <b>pagamento</b> para <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> fue efectuado.",
    "TX_SENT_MULTI": "O seu <b>pagamento</b> para <b>{{params[1]}}</b> foi efetuado.",
    "TX_RECEIVED": "<b>Recebeu um pagamento</b> de <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "TX_RECEIVED_MULTI": "<b>Recebeu um pagamento</b> de <b>{{params[1]}}</b>.",
    "CERT_SENT": "A sua <b>certificação</b> a <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\" ><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> foi efetuada.",
    "CERT_RECEIVED": "<b>Recebeu uma certificação</b> de <span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span>.",
    "USER": {
        "ABUSE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> reportou o seu perfil",
        "DELETION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> reportou um perfil para apagar : <b>{{params[2]}}</b>",
        "FOLLOW_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> veja a atividade do seu perfil",
        "LIKE_RECEIVED": "A <span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> gostou do seu perfil</b>",
        "MODERATION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> pede moderação sobre o perfil : <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",
        "STAR_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> pontuou com ({{params[3]}} <b class=\"ion-star\">)"
    },
    "PAGE": {
      "ABUSE_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> reportou a sua página : <b>{{params[2]}}</b>",
      "DELETION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> reportou uma página para apagar : <b>{{params[2]}}</b>",
      "FOLLOW_CLOSE": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> fechou a página : <b>{{params[2]}}</b>",
      "FOLLOW_NEW": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> adicionou a página : <b>{{params[2]}}</b>",
      "FOLLOW_NEW_COMMENT": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> comentou a página : <b>{{params[2]}}</b>",
      "FOLLOW_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> segue a sua página : <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> modificou a página : <b>{{params[2]}}</b>",
      "FOLLOW_UPDATE_COMMENT": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> modificou o seu comentário na página : <b>{{params[2]}}</b>",
      "MODERATION_RECEIVED": "<span class=\"positive\"><i class=\"icon ion-person\"></i>&thinsp;{{name||params[1]}}</span> pede-vos moderação sobre a página : <b>{{params[2]}}</b><br/><b class=\"dark ion-quote\"> </b><span class=\"text-italic\">{{params[3]}}</span>",

      "NEW_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> comentou a sua referencia : <b>{{params[2]}}</b>",
      "UPDATE_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> modificou o seu comentário sobre a sua referencia : <b>{{params[2]}}</b>",
      "NEW_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid}\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> contestou o seu comentário sobre a referencia : <b>{{params[2]}}</b>",
      "UPDATE_REPLY_COMMENT": "<span ng-class=\"{'gray': !notification.uid, 'positive':notification.uid }\"><i class=\"icon\" ng-class=\"{'ion-person': notification.uid, 'ion-key': !notification.uid}\"></i>&thinsp;{{name||uid||params[1]}}</span> modificou a reposta ao seu comentário sobre a referencia : <b>{{params[2]}}</b>"
    }
  },
  "CONFIRM": {
    "ES_USE_FALLBACK_NODE": "Nó de dados <b>{{old}}</b> indisponível ou endereço inválido.<br/><br/>Deseja utilizar temporariamente o nó de dados <b>{{new}}</b>?"
  },
  "ERROR": {
    "ES_CONNECTION_ERROR": "Nó de dados <b>{{server}}</b> indisponível ou endereço inválido.<br/><br/>Cesium continuará funcionando, <b>sem a extensão Cesium+</b> (perfis de usuário, mensagens privadas), mapas e gráficos).<br/><br/>Verifique a sua ligação à Internet, o altere o nó de dados em <a class=\"positive\" ng-click=\"doQuickFix('settings')\"> nas definições da extensão </a>.",
    "ES_MAX_UPLOAD_BODY_SIZE": "O volume de dados a enviar excede o limite estabelecido pelo servidor.<br/><br/>Por favor, tente novamente depois, por exemplo, apagando fotos."
  }
}
);

$translateProvider.translations("de-DE", {
  "MAP": {
    "COMMON": {
      "SEARCH_DOTS": "Suche...",
      "BTN_LOCALIZE_ME": "Lokalisiere mich"
    },
    "NETWORK": {
      "LOOKUP": {
        "BTN_MAP": "Karte der Knoten",
        "BTN_MAP_HELP": "Karte der Knoten öffnen"
      },
      "VIEW": {
        "TITLE": "Karte der Knoten",
        "LAYER": {
          "MEMBER": "Mitgliedsknoten",
          "MIRROR": "Spiegelknoten",
          "OFFLINE": "Offline-Knoten"
        }
      }
    },
    "WOT": {
      "LOOKUP": {
        "BTN_MAP": "Karte",
        "BTN_MAP_HELP": "Mitgliederkarte öffnen"
      },
      "VIEW": {
        "TITLE": "Mitgliederkarte",
        "LAYER": {
          "MEMBER": "<i class=\"icon ion-person positive\"></i> Mitglieder",
          "PENDING": "<i class=\"icon ion-clock energized\"></i> Ausstehende Registrierungen",
          "WALLET": "<i class=\"icon ion-card grey\"></i> Einfache Brieftaschen"
        }
      },
      "ERROR": {
        "LOAD_POSITION_FAILED": "Die anzuzeigenden Positionen konnten nicht abgerufen werden."
      }
    },
    "REGISTRY": {
      "LOOKUP": {
        "BTN_MAP": "Karte",
        "BTN_MAP_HELP": "Karte der Seiten öffnen"
      },
      "VIEW": {
        "TITLE": "Karte der Seiten",
        "LAYER": {
          "SHOP": "<i class=\"icon ion-page-shop positive\"></i> Lokale Geschäfte",
          "COMPANY": "<i class=\"icon ion-page-company positive\"></i> Unternehmen",
          "ASSOCIATION": "<i class=\"icon ion-page-association energized\"></i> Verbände",
          "INSTITUTION": "<i class=\"icon ion-page-institution grey\"></i> Institutionen"
        }
      }
    },
    "PROFILE": {
      "MARKER_HELP": "<b>Ziehe</b> diese Markierung, um <b>deinen Standort</b><br/>auf der Karte zu <b>aktualisieren</b>, oder verwende die Schaltflächen<br/>über der Karte."
    },
    "ERROR": {
      "LOCALIZE_ME_FAILED": "Ihr aktueller Standort kann nicht abgerufen werden"
    },
    "SETTINGS": {
      "MAP_DIVIDER": "Karten",
      "ENABLE_GOOGLE_API": "Google API-Dienste aktivieren?",
      "ENABLE_GOOGLE_API_HELP": "Ermöglicht die Anzeige von Konten mit einer Adresse, aber ohne GPS-Ortung in der <b>Mitgliederkarte</b>.",
      "GOOGLE_API_KEY": "Google-API-Schlüssel",
      "BTN_GOOGLE_API": "Einen Google-API-Schlüssel erhalten",
      "BTN_GOOGLE_API_WARNING": "Erfordert ein Google-Konto",
      "GOOGLE_API_KEY_PLACEHOLDER": "Beispiel: AIzaqyAgszvWm0tM81x1sMK_ipDHBI7EowLqR7I"
    },
    "HELP": {
      "TIP": {
        "WOT": "Auf dieser Karte werden <b>Profile mit einer Position</b> angezeigt, unabhängig davon, ob sie Mitgliedskonten entsprechen oder nicht.<br/><br/>Nur Profile mit der Option &quot;Auf der Karte anzeigen&quot; sind aktiviert.",
        "WOT_BTN_LAYERS": "<b>Filtern Sie die hier angezeigten Daten</b>: Mitglieder, ausstehende Registrierungen, einfache Brieftaschen usw.",
        "WOT_BTN_SEARCH": "Sie können nach Name, öffentlichem Schlüssel oder Pseudonym des Mitglieds <b>suchen</b>."
      }
    }
  }
}
);

$translateProvider.translations("en-GB", {
  "MAP": {
    "COMMON": {
      "SEARCH_DOTS": "Search...",
      "BTN_LOCALIZE_ME": "Localize me"
    },
    "NETWORK": {
      "LOOKUP": {
        "BTN_MAP": "Peers map",
        "BTN_MAP_HELP": "Open peers map"
      },
      "VIEW": {
        "TITLE": "Peers map",
        "LAYER": {
          "MEMBER": "Member peers",
          "MIRROR": "Mirror peers",
          "OFFLINE": "Offline peers"
        }
      }
    },
    "WOT": {
      "LOOKUP": {
        "BTN_MAP": "Map",
        "BTN_MAP_HELP": "Open members map"
      },
      "VIEW": {
        "TITLE": "Members map",
        "LAYER": {
          "MEMBER": "<i class=\"icon ion-person positive\"></i> Members",
          "PENDING": "<i class=\"icon ion-clock energized\"></i> Pending registrations",
          "WALLET": "<i class=\"icon ion-card gray\"></i> Simple wallets"
        }
      },
      "ERROR": {
        "LOAD_POSITION_FAILED": "Can not retrieve the positions to display."
      }
    },
    "REGISTRY": {
      "LOOKUP": {
        "BTN_MAP": "Map",
        "BTN_MAP_HELP": "Open the map of the pages"
      },
      "VIEW": {
        "TITLE": "Map of pages",
        "LAYER": {
          "SHOP": "<i class=\"icon ion-page-shop positive\"></i> Local shops",
          "COMPANY": "<i class=\"icon ion-page-company positive\"></i> Companies",
          "ASSOCIATION": "<i class=\"icon ion-page-association energized\"></i> Associations",
          "INSTITUTION": "<i class=\"icon ion-page-institution gray\"></i> Institutions"
        }
      }
    },
    "PROFILE": {
      "MARKER_HELP": "<b>Drag and drop</b> this marker to <b>update<br/>your position</b>, or use the buttons<br/>on top of the map."
    },
    "ERROR": {
      "LOCALIZE_ME_FAILED": "Unable to retrieve your current position"
    },
    "SETTINGS": {
      "MAP_DIVIDER": "Maps",
      "ENABLE_GOOGLE_API": "Enable Google API services?",
      "ENABLE_GOOGLE_API_HELP": "In the <b>members map</b>, allows you to display accounts with an address but without geolocation.",
      "GOOGLE_API_KEY": "Google API key",
      "BTN_GOOGLE_API": "Getting a key",
      "BTN_GOOGLE_API_WARNING": "requires a Google account",
      "GOOGLE_API_KEY_PLACEHOLDER": "eg: AIzaqyAgszvWm0tM81x1sMK_ipDHBI7EowLqR7I"
    },
    "HELP": {
      "TIP": {
        "WOT": "This map displays <b>profiles with a position</b>, whether they are member accounts or not.<br/><br/>Only profiles with the option &quot;appear on the map&quot; has been activated.",
        "WOT_BTN_LAYERS": "<b>Filter the displayed data</b>: members, pending registrations, simple portfolios, etc.",
        "WOT_BTN_SEARCH": "You can <b>search</b> by name, public key, or member pseudonym."
      }
    }
  }
}
);

$translateProvider.translations("en", {
  "MAP": {
    "COMMON": {
      "SEARCH_DOTS": "Search...",
      "BTN_LOCALIZE_ME": "Localize me"
    },
    "NETWORK": {
      "LOOKUP": {
        "BTN_MAP": "Peers map",
        "BTN_MAP_HELP": "Open peers map"
      },
      "VIEW": {
        "TITLE": "Peers map",
        "LAYER": {
          "MEMBER": "Member peers",
          "MIRROR": "Mirror peers",
          "OFFLINE": "Offline peers"
        }
      }
    },
    "WOT": {
      "LOOKUP": {
        "BTN_MAP": "Map",
        "BTN_MAP_HELP": "Open members map"
      },
      "VIEW": {
        "TITLE": "Members map",
        "LAYER": {
          "MEMBER": "<i class=\"icon ion-person positive\"></i> Members",
          "PENDING": "<i class=\"icon ion-clock energized\"></i> Pending registrations",
          "WALLET": "<i class=\"icon ion-card gray\"></i> Simple wallets"
        }
      },
      "ERROR": {
        "LOAD_POSITION_FAILED": "Can not retrieve the positions to display."
      }
    },
    "REGISTRY": {
      "LOOKUP": {
        "BTN_MAP": "Map",
        "BTN_MAP_HELP": "Open the map of the pages"
      },
      "VIEW": {
        "TITLE": "Map of pages",
        "LAYER": {
          "SHOP": "<i class=\"icon ion-page-shop positive\"></i> Local shops",
          "COMPANY": "<i class=\"icon ion-page-company positive\"></i> Companies",
          "ASSOCIATION": "<i class=\"icon ion-page-association energized\"></i> Associations",
          "INSTITUTION": "<i class=\"icon ion-page-institution gray\"></i> Institutions"
        }
      }
    },
    "PROFILE": {
      "MARKER_HELP": "<b>Drag and drop</b> this marker to <b>update<br/>your position</b>, or use the buttons<br/>on top of the map."
    },
    "ERROR": {
      "LOCALIZE_ME_FAILED": "Unable to retrieve your current position"
    },
    "SETTINGS": {
      "MAP_DIVIDER": "Maps",
      "ENABLE_GOOGLE_API": "Enable Google API services?",
      "ENABLE_GOOGLE_API_HELP": "In the <b>members map</b>, allows you to display accounts with an address but without geolocation.",
      "GOOGLE_API_KEY": "Google API key",
      "BTN_GOOGLE_API": "Getting a key",
      "BTN_GOOGLE_API_WARNING": "requires a Google account",
      "GOOGLE_API_KEY_PLACEHOLDER": "eg: AIzaqyAgszvWm0tM81x1sMK_ipDHBI7EowLqR7I"
    },
    "HELP": {
      "TIP": {
        "WOT": "This map displays <b>profiles with a position</b>, whether they are member accounts or not.<br/><br/>Only profiles with the option &quot;appear on the map&quot; has been activated.",
        "WOT_BTN_LAYERS": "<b>Filter the displayed data</b>: members, pending registrations, simple portfolios, etc.",
        "WOT_BTN_SEARCH": "You can <b>search</b> by name, public key, or member pseudonym."
      }
    }
  }
}
);

$translateProvider.translations("eo-EO", {
  "MAP": {
    "COMMON": {
      "SEARCH_DOTS": "Traserĉi...",
      "BTN_LOCALIZE_ME": "Lokalizi min"
    },
    "NETWORK": {
      "LOOKUP": {
        "BTN_MAP": "Mapo",
        "BTN_MAP_HELP": "Malfermi la mapon pri nodoj"
      },
      "VIEW": {
        "TITLE": "Mapo pri nodoj",
        "LAYER": {
          "MEMBER": "Membro-nodoj",
          "MIRROR": "Spegul-nodoj",
          "OFFLINE": "Nekonektitaj nodoj"
        }
      }
    },
    "WOT": {
      "LOOKUP": {
        "BTN_MAP": "Mapo",
        "BTN_MAP_HELP": "Malfermi la mapon pri membroj"
      },
      "VIEW": {
        "TITLE": "Mapo pri membroj",
        "LAYER": {
          "MEMBER": "<i class=\"icon ion-person positive\"></i> Membroj",
          "PENDING": "<i class=\"icon ion-clock energized\"></i> Aliĝoj atendantaj",
          "WALLET": "<i class=\"icon ion-card gray\"></i> Simplaj monujoj"
        }
      },
      "ERROR": {
        "LOAD_POSITION_FAILED": "Neeblas ricevi la lokojn afiŝotajn."
      }
    },
    "REGISTRY": {
      "LOOKUP": {
        "BTN_MAP": "Mapo",
        "BTN_MAP_HELP": "Malfermi la mapon pri paĝoj"
      },
      "VIEW": {
        "TITLE": "Mapo pri paĝoj",
        "LAYER": {
          "SHOP": "<i class=\"icon ion-page-shop positive\"></i> Lokaj komercoj",
          "COMPANY": "<i class=\"icon ion-page-company positive\"></i> Entreprenoj",
          "ASSOCIATION": "<i class=\"icon ion-page-association energized\"></i> Asocioj",
          "INSTITUTION": "<i class=\"icon ion-page-institution gray\"></i> Institucioj"
        }
      }
    },
    "PROFILE": {
      "MARKER_HELP": "<b>Ŝovu-demetu</b> tiun ĉi markilon por <b>aktualigi<br/> vian lokon</b> sur la mapo, aŭ uzu la serĉo-butonon<br/>super la mapo."
    },
    "ERROR": {
      "LOCALIZE_ME_FAILED": "Neeblas ricevi vian nunan lokon"
    },
    "SETTINGS": {
      "MAP_DIVIDER": "Mapoj",
      "ENABLE_GOOGLE_API": "Aktivigi la Google-API-servojn?",
      "ENABLE_GOOGLE_API_HELP": "Ebligas afiŝi sur la <b>mapo pri membroj</b> kontojn kun adreso sed sen iu GPS-lokalizo.",
      "GOOGLE_API_KEY": "Google-API-ŝlosilo",
      "BTN_GOOGLE_API": "Ekhavi ŝlosilon",
      "BTN_GOOGLE_API_WARNING": "Necesigas havi Google-konton",
      "GOOGLE_API_KEY_PLACEHOLDER": "Ekzemple: AIzaqyAgszvWm0tM81x1sMK_ipDHBI7EowLqR7I"
    },
    "HELP": {
      "TIP": {
        "WOT": "Tiu ĉi mapo afiŝas <b>la profilojn kun loko</b>, ĉu ili rilatas al membro-kontoj aŭ ne.<br/><br/>Nur videblas la profiloj, kies kromaĵo &quot;aperi sur la mapo&quot; estis aktivigita.",
        "WOT_BTN_LAYERS": "<b>Filtru ĉi tie la afiŝitajn datenojn</b>: membroj, aliĝoj atendantaj, simplaj monujoj, ktp.",
        "WOT_BTN_SEARCH": "Vi povas <b>efektivigi serĉadon</b> laŭ nomo, publika ŝlosilo aŭ membro-pseŭdonimo."
      }
    }
  }
}
);

$translateProvider.translations("fr-FR", {
  "MAP": {
    "COMMON": {
      "SEARCH_DOTS": "Rechercher...",
      "BTN_LOCALIZE_ME": "Me localiser"
    },
    "NETWORK": {
      "LOOKUP": {
        "BTN_MAP": "Carte",
        "BTN_MAP_HELP": "Ouvrir la carte des noeuds"
      },
      "VIEW": {
        "TITLE": "Carte des noeuds",
        "LAYER": {
          "MEMBER": "Nœuds membre",
          "MIRROR": "Nœuds miroir",
          "OFFLINE": "Nœuds hors ligne"
        }
      }
    },
    "WOT": {
      "LOOKUP": {
        "BTN_MAP": "Carte",
        "BTN_MAP_HELP": "Ouvrir la carte des membres"
      },
      "VIEW": {
        "TITLE": "Carte des membres",
        "LAYER": {
          "MEMBER": "<i class=\"icon ion-person positive\"></i> Membres",
          "PENDING": "<i class=\"icon ion-clock energized\"></i> Inscriptions en attente",
          "WALLET": "<i class=\"icon ion-card gray\"></i> Simples portefeuilles"
        }
      },
      "ERROR": {
        "LOAD_POSITION_FAILED": "Impossible de récupérer les positions à afficher."
      }
    },
    "REGISTRY": {
      "LOOKUP": {
        "BTN_MAP": "Carte",
        "BTN_MAP_HELP": "Ouvrir la carte des pages"
      },
      "VIEW": {
        "TITLE": "Carte des pages",
        "LAYER": {
          "SHOP": "<i class=\"icon ion-page-shop positive\"></i> Commerces locaux",
          "COMPANY": "<i class=\"icon ion-page-company positive\"></i> Entreprises",
          "ASSOCIATION": "<i class=\"icon ion-page-association energized\"></i> Associations",
          "INSTITUTION": "<i class=\"icon ion-page-institution gray\"></i> Institutions"
        }
      }
    },
    "PROFILE": {
      "MARKER_HELP": "<b>Glissez-déposez</b> ce marqueur pour <b>mettre<br/>à jour votre position</b> sur la carte, ou utilisez le bouton<br/>de recherche au-dessus de la carte."
    },
    "ERROR": {
      "LOCALIZE_ME_FAILED": "Impossible de récupérer votre position actuelle"
    },
    "SETTINGS": {
      "MAP_DIVIDER": "Cartes",
      "ENABLE_GOOGLE_API": "Activer les services Google API ?",
      "ENABLE_GOOGLE_API_HELP": "Permet l'affichage dans la <b>carte des membres</b> des comptes ayant une adresse mais aucun positionnement GPS.",
      "GOOGLE_API_KEY": "Clé d'API Google",
      "BTN_GOOGLE_API": "Obtenir une clé",
      "BTN_GOOGLE_API_WARNING": "Nécessite d'avoir un compte Google",
      "GOOGLE_API_KEY_PLACEHOLDER": "Exemple : AIzaqyAgszvWm0tM81x1sMK_ipDHBI7EowLqR7I"
    },
    "HELP": {
      "TIP": {
        "WOT": "Cette carte affiche <b>les profils ayant une position</b>, qu'ils correspondent à des comptes membre ou non.<br/><br/>Seuls sont visibles les profils dont l'option &quot;apparaître sur la carte&quot; a été activée.",
        "WOT_BTN_LAYERS": "<b>Filtrez ici les données affichées</b> : membres, inscriptions en attente, simples portefeuilles, etc.",
        "WOT_BTN_SEARCH": "Vous pouvez <b>effectuer une recherche</b> par nom, clef publique ou pseudonyme de membre."
      }
    }
  }
}
);

$translateProvider.translations("it-IT", {
   "MAP": {
     "COMMON": {
       "SEARCH_DOTS": "Ricerca...",
       "BTN_LOCALIZE_ME": "Trovami"
     },
     "NETWORK": {
       "LOOKUP": {
         "BTN_MAP": "Mappa dei peers",
         "BTN_MAP_HELP": "Aprire mappa dei peers"
       },
       "VIEW": {
         "TITLE": "Mappa dei peers",
         "LAYER": {
           "MEMBER": "Peers membri",
           "MIRROR": "Peers specchi",
           "OFFLINE": "Peers offline"
         }
       }
     },
     "WOT": {
       "LOOKUP": {
         "BTN_MAP": "Mappa dei membri",
         "BTN_MAP_HELP": "Aprire mappa dei membri"
       },
       "VIEW": {
         "TITLE": "Mappa membri",
         "LAYER": {
           "MEMBER": "<i class=\"icon ion-person positive\"></i> Membri",
           "PENDING": "<i class=\"icon ion-clock energized\"></i> Registrazione pendenti",
           "WALLET": "<i class=\"icon ion-card gray\"></i> Portafogli osservatori"
         }
       },
       "ERROR": {
         "LOAD_POSITION_FAILED": "Errore nel caricamento delle posizioni."
       }
     },
     "REGISTRY": {
       "LOOKUP": {
         "BTN_MAP": "Mappa",
         "BTN_MAP_HELP": "Aprire la mappa delle pagine pro"
       },
       "VIEW": {
         "TITLE": "Mappa delle pagine pro",
         "LAYER": {
           "SHOP": "<i class=\"icon ion-page-shop positive\"></i> Negozi locali",
           "COMPANY": "<i class=\"icon ion-page-company positive\"></i> Aziende",
           "ASSOCIATION": "<i class=\"icon ion-page-association energized\"></i> Associazioni",
           "INSTITUTION": "<i class=\"icon ion-page-institution gray\"></i> Instituti"
         }
       }
     },
     "PROFILE": {
       "MARKER_HELP": "<b>Drag and drop</b> questo indicatore per <b>aggiornare<br/>tua posizione</b>, o usa i bottoni <br/>sopra la mappa."
     },
     "ERROR": {
       "LOCALIZE_ME_FAILED": "Impossible trovare tua posizione attuale"
     },
     "SETTINGS": {
       "MAP_DIVIDER": "Mappe",
       "ENABLE_GOOGLE_API": "Abilitare i servizi Google API ?",
       "ENABLE_GOOGLE_API_HELP": "Sulla <b>mappa dei membri</b>, ti permette di visualizzare i conti con un indirizzo ma senza geolocalizzazione.",
       "GOOGLE_API_KEY": "Chiave Google API",
       "BTN_GOOGLE_API": "Ottenere una chiave",
       "BTN_GOOGLE_API_WARNING": "necessita un conto Google",
       "GOOGLE_API_KEY_PLACEHOLDER": "per es. : AIzaqyAgszvWm0tM81x1sMK_ipDHBI7EowLqR7I"
     },
     "HELP": {
       "TIP": {
         "WOT": "Questa mappa mostra <b>profili che hanno fornito una posizione</b>, che siano conti membri o no.<br/><br/>Solo i profili che hanno attivato l'opzione &quot;apparire sulla mappa&quot; sono stati attivati.",
         "WOT_BTN_LAYERS": "<b>Filtrare i dati visibili</b>: membri, registrazioni pendenti, portafogli osservatori, etc.",
         "WOT_BTN_SEARCH": "Puoi <b>cercare</b> per nome, chiave pubblica, o pseudonimo del membro."
       }
     }
   }
 }
 );

$translateProvider.translations("fr-FR", {
  "RML9": {
    "BTN_EXPORT": "Télécharger",
    "BTN_OPEN": "Ouvrir la page RML9",
    "BTN_SWOW_TX": "Voir les transactions",
    "FILE_NAME": "relevé du compte {{pubkey|formatPubkey}} au {{currentTime|formatDateForFile}}.csv",
    "HEADERS": {
      "TIME": "Date",
      "AMOUNT": "Montant",
      "COMMENT": "Commentaire"
    },
    "VIEW": {
      "TITLE": "RML9",
      "DIVIDER": "Dernières transactions :",
      "BALANCE": "Solde du compte"
    },
    "CHART": {
      "INPUT_CHART_TITLE": "Somme des flux entrants, par émetteur :",
      "OUTPUT_CHART_TITLE": "Somme des flux sortants, par destinaire :"
    },
    "SETTINGS": {
      "ENABLE_TOGGLE": "Activer le plugin RML9 ?"
    }
  }
}
);
}]);
